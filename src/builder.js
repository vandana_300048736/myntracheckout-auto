import puppeteer from 'puppeteer'

export default class Launcher {
  static async build(viewport) {
    const launchOptions = {
      headless: false,
      defaultViewport: null,
      slowMo: 0,
      keepCookies: true,
       ignoreDefaultArgs: ['--disable-extensions'],
      //userDataDir: "./user_data",
      //slowmo -- to slow the execution
      //ignoreDefaultArgs: ['--enable-automation'],
      args: [
        '--start-fullscreen',
        '--disable-infobars'
      ],
    }

    const browser = await puppeteer.launch(launchOptions)
    const context = await browser.createIncognitoBrowserContext();

    //const page = await browser.newPage()
    const page = await context.newPage()
    const extendedPage = new Launcher(page)
    page.setDefaultTimeout(70000)

    switch (viewport) {
      case 'pwa':
        
        const mobileViewport = puppeteer.devices['iPhone 8']
        await page.emulate(mobileViewport)
        console.log("PWA USER AGENT IN VIEWPORT+++++")
        break

      case 'desktop':
       await page.setViewport({ width: 1440, height: 768 })
       console.log("DESKTOP USER AGENT IN VIEWPORT+++++")
        break
  
      case 'android':
          console.log("VIEWPORT USER AGENT IS Android App+++++++++++++")
          await page.setViewport({ width: 400, height: 700 })
          await page.setUserAgent('MyntraRetailAndroid/3.25.1 (Phone, 320dpi) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19')
          break

      case 'ios':
          console.log("VIEWPORT USER AGENT IS iOS App+++++++++++++")
          await page.setViewport({ width: 400, height: 700 })
          await page.setUserAgent('MyntraRetailiPhone/3.9.0-fox (Phone, 320dpi) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19')
          break
        default:
          throw new Error('Only PWA and desktop are supported')
    }

    return new Proxy(extendedPage, {
      get: function (_target, property) {
        return extendedPage[property] || browser[property] || page[property]
      },
    })
  }

  constructor(page) {
    this.page = page
  }

  async waitAndClick(selector) {
    await this.page.waitForSelector(selector)
    await this.page.click(selector)
  }

  
  async waitAndType(selector, text) {
    await this.page.waitForSelector(selector)
    await this.page.type(selector, text)
  }
  async getText(selector) {
    await this.page.waitForSelector(selector)
    return this.page.$eval(selector, e => e.innerHTML)
  }
  async getTextXpath(xpath)
  {
    const [getXpath] = await this.page.$x(xpath);
    //get the text using innerText from that webelement
  
    const getMsg = await this.page.evaluate(name => name.innerText, getXpath);
    return getMsg
  }
 
async getCurrentURL()
{
 
  await this.page.evaluate(_ => {
    var URL= window.location.href;
    return URL
  });
 
   
}

  async getCount(selector) {  
    await this.page.waitForSelector(selector)
    return this.page.$$eval(selector, items => items.length)
  }

  
  async waitForXPathAndClick(xpath) {
    await this.page.waitForXPath(xpath)
    const elements = await this.page.$x(xpath)
    if (elements.length > 1) {
      console.warn('more than one result')
    }
  
    await elements[0].click()
  }
  async waitForXpathAndType(xpath,text){
    await this.page.waitForXPath(xpath)
    const [ele] = await this.page.$x(xpath)
    await  ele.type(text)
  }

  async isElementVisible(selector) {
    let visible = true
    await this.page
      .waitForSelector(selector, { visible: true, timeout: 3000 })
      .catch(() => {
        visible = false
      })
      console.log("element is visible", visible)
    return visible
  }

  async isXPathVisible(selector) {
    console.log(selector)
    let visible = true
    await this.page
      .waitForXPath(selector, { visible: true, timeout: 5000 })
      .catch(() => {
        visible = false
        
      })
    return visible
  }


async scrollPage(){
  await this.page.evaluate(_ => {
    window.scrollBy(0, 300);
  });
}

async autoScroll(page){
  await page.evaluate(async () => {
      await new Promise((resolve, reject) => {
          var totalHeight = 0;
          var distance = 100;
          var timer = setInterval(() => {
            console.log("Entered autoscroll")
              var scrollHeight = document.body.scrollHeight
              window.scrollBy(0, distance)
              totalHeight += distance;

              if(totalHeight >= scrollHeight){
                  clearInterval(timer)
                  resolve()
              }
          }, 100)
      })
  })
}

async writingCookies() {
  const cookieArray = require(C.cookieFile); //C.cookieFile can be replaced by ('./filename.json')
  await page.setCookie(...cookieArray);
  await page.cookies(C.feedUrl); //C.url can be ('https://example.com')
  }

  async getCookies() {
    const cookiesObject = await page.cookies();
    jsonfile.writeFile('linkedinCookies.json', cookiesObject, { spaces: 2 },
      function (err) {
        if (err) {
          console.log('The Cookie file could not be written.', err);
        }
        console.log("Cookie file has been successfully saved in current working Directory : '" + process.cwd() + "'");
      })
    }

async cookieSet()
{
  console.log("ENTERED cookieSet Func++++++++++++++++++")
  const fs = require(fs);
const cookiesFilePath = 'cookies.json';
// Save Session Cookies
const cookiesObject = await page.cookies()
// Write cookies to temp file to be used in other profile pages
fs.writeFile(cookiesFilePath, JSON.stringify(cookiesObject),
 function(err) { 
  if (err) {
  console.log('The file could not be written.', err)
  }
  console.log('Session has been successfully saved')
})
  
}

async cookiePreviousload()
{
  const previousSession = fs.existsSync(cookiesFilePath)
if (previousSession) {
  // If file exist load the cookies
  const cookiesString = fs.readFileSync(cookiesFilePath);
  const parsedCookies = JSON.parse(cookiesString);
  if (parsedCookies.length !== 0) {
    for (let cookie of parsedCookies) {
      await page.setCookie(cookie)
    }
    console.log('Session has been loaded in the browser')
  }
}
}


}
