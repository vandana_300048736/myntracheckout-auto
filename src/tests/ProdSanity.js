import { expect, assert } from 'chai'
import { step } from 'mocha-steps'
import Page from '../builder'
import AddressPage from '../pages/AddressPage'
import LoginPage from '../pages/LoginPage'
import Homepage  from '../pages/HomePage'
import WishlistPage from '../pages/WishlistPage'
import PdpPage from '../pages/PdpPage'
import CartPage from '../pages/CartPage'
import ListPage from '../pages/ListPage'
import PaymentsPage from '../pages/PaymentsPage'
import GiftCardPage from '../pages/GiftCardPage'



describe('Prod Sanity Test', () => {
  let page
  let loginPage
  let homePage
  let wishlistPage
  let pdpPage
  let cartPage
  let listPage
  let addressPage
  let paymentsPage
  let giftCardPage
  
  
  beforeEach(async () => {

    page = await Page.build(process.env.platform)
    loginPage = await new LoginPage(page)
    homePage = await new Homepage(page)
    wishlistPage = await new WishlistPage(page)
    pdpPage = await new PdpPage(page)
    listPage = await new ListPage(page)
    cartPage = await new CartPage(page)
    addressPage=await new AddressPage(page)
    paymentsPage=await new PaymentsPage(page)
    giftCardPage=await new GiftCardPage(page)
    //await loginPage.login(page)
    // await cartPage.setCookies(page)
    //await wishlistPage.clearWishlist(page)
    // await cartPage.clearCart(page)
    await homePage.navigateToHomePage(page)

  })

  afterEach(async () => {
    await page.close()
  })

  
it('TC:001->Verify Remove addresses on address page', async() => 
{
  await loginPage.login(page)
  await homePage.searchFor(page, "10430798")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  assert.strictEqual(await addressPage.removeAllAddresses(page),true," Failed to remove all the address in address page")
  
})

it('TC:0002->Verify edit address on address page', async() =>
{
  await loginPage.login(page)
  await homePage.searchFor(page, "10430798")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  assert.strictEqual(await addressPage.removeAllAddresses(page),true," Failed to remove all the address in address page")
  await addressPage.addAddress(page)
  assert.strictEqual(await addressPage.editAddress(page),true," Failed to remove all the address in address page")
})

it('TC:003->Verify change address on address page', async() =>
{
  await loginPage.login(page)
  await homePage.searchFor(page, "10430798")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  assert.strictEqual(await addressPage.removeAllAddresses(page),true," Failed to remove all the address in address page")
  await addressPage.addAddress(page)
  await addressPage.addSecondAddress(page)
  assert.strictEqual(await addressPage.changeAddressOnAddressPage(page),true," Failed to change the address in address page")
  
})

it('TC:004->Verify user is able to add products to cart from pdp', async () => {
       
    await homePage.searchFor(page, "10206819")
    await pdpPage.selectSize(page)
    await pdpPage.addProductPdpToCart(page)
    const cartEmpty=await cartPage.isCartEmpty(page)
    assert.strictEqual(cartEmpty, false, "Cart is  empty")
    console.log("***** Item added to cart  Successfully  *****")
    
      
})

// verify move to bag functionality from wishlist page, and verify count is reduced and bag count is increased

it('TC:005->Verify user is able to move items from wishlist to cart page and verify the cart & wishlist count', async () => {
  
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  console.log("***** Removed all item ***")
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "10206819")
  await pdpPage.wishlistItem(page)
  await wishlistPage.navigateToWishlist(page)
  const wishCountBefore =await wishlistPage.getWishlistCount(page)
  const beforeMoving= await wishlistPage.getCartCount(page)
  console.log("** Before Moving**** :"+beforeMoving)
  await wishlistPage.moveItemfromWishlist(page)
  const afterMoving =await wishlistPage.getCartCount(page)
  console.log("*** After moving Count is:"+afterMoving)
  const wishCountAfter =await wishlistPage.getWishlistCount(page)
  console.log("*** Wishlist count after moving"+wishCountAfter)
  console.log("cart count above")
  assert.strictEqual(afterMoving, beforeMoving + 1, "item is not moved to cart")
  assert.strictEqual(wishCountAfter, wishCountBefore - 1, "wishlist count not decreased after moving to bag")
  await wishlistPage.navigateToCartPage(page)
 
})

it('TC:006->Verify user can edit quantity in the cart page ', async () => {

  await homePage.searchFor(page, "10206819")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  console.log("go to cart page")
  const qtyBeforeEdit=await cartPage.getQty(page)
  console.log("****** Qty Before edit :"+qtyBeforeEdit)
  await cartPage.editQty(page)
  console.log(" ***** user successfully edited the item ******")
  const qtyAfterEdit=await cartPage.getQty(page)
  console.log("****** Qty After edit :"+qtyAfterEdit)
  assert.notEqual(qtyBeforeEdit,qtyAfterEdit,"Qty has not changed ")

})

it('TC:007->Verify user can apply giftwrap', async () => {

  await homePage.searchFor(page, "10206819")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  console.log("go to cart page")
  await cartPage.addGiftWrap(page)
  console.log("****** Applied Gift wrap ******")
  
})

it('TC:008->Verify user can remove applied  giftwrap', async () => {

  await homePage.searchFor(page, "10430798")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  console.log("go to cart page")
  await cartPage.addGiftWrap(page)
  console.log("****** Applied Giftwrap ******")
  await cartPage.removeAppliedGiftwrap(page)
  console.log("****** Removed Applied Giftwrap ********")

})

it('TC:009->Verify empty cart by removing items added to cart ', async () => {
  await loginPage.login(page)
  await homePage.searchFor(page, "10430798")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  console.log("***** Removed all item ***")
})

it('TC:010->Verify user can add items from cart filler ', async () => {

  await homePage.searchFor(page, "10430798")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  console.log("go to cart page")
  const cartCount_Before_Adding_Cartfiller= await cartPage.getTotalCartItem(page)
  console.log("******** Cart count before adding cart filler "+cartCount_Before_Adding_Cartfiller)
  const isCartFillerDisplayed = await cartPage.verifyCartfillerSection(page)
  assert.strictEqual(isCartFillerDisplayed, true, "Cart filler section not displayed")
  await cartPage.addItemFromCartfiller(page)
  console.log("******** Cart count before adding cart filler ")
  const cartCount_After_Adding_Cartfiller=await cartPage.getTotalCartItem(page)
  console.log("******** Cart count after adding cart filler "+cartCount_After_Adding_Cartfiller)
  //assert.notStrictEqual(cartPrice_Before_Adding_Cartfiller,cartPrice_After_Adding_Cartfiller,"After adding item from cart filler price has not chnaged")
  console.log("****** Added Item from Cart filler *********")
  await cartPage.removeAllItemsFromCart(page)
  console.log("***** Removed all item ***")
  const cartEmpty=await cartPage.isCartEmpty(page)
  assert.strictEqual(cartEmpty, true, "Cart is not empty")
  console.log("***** Cart is Empty***")
  
})

it('TC:011->Verify the cart behaviour for non logged in user ', async () => {
  
  await homePage.searchFor(page, "10430798")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  console.log("go to cart page")
  const coupon_Login=await cartPage.verifyLoginButtonforNonLoggedInUserOnCouponSection(page)
  assert.strictEqual(coupon_Login,true,"Login button is not shown in Coupon section")
  await cartPage.clickCouponLogin(page)
  const login_PageDisplayed=await loginPage.verifyLoginPageDisplayed(page)
  assert.strictEqual(login_PageDisplayed,true,"Login page not displayed")
  console.log("***** Verified the behaviour for non logged in user *******")  
  
})

it('TC:012->Verify user is able to switch seller', async () => {
  
  await homePage.searchFor(page, "11369440")
  await pdpPage.selectSize(page)
  assert.strictEqual(await pdpPage.isMoreSellerDisplayed(page),true,"More seller option is not displayed")
  await pdpPage.addProductPdpToCart(page)
  console.log("go to cart page")
  const sellerNameBeforeEdit=await cartPage.getSellerName(page)
  const price=await cartPage.getItemPrice(page)
  await cartPage.clickOnSize(page)
  const sellerNameInEditSizeWindow=await cartPage.getSellerNameinEditSizeWindow(page)
  var verifySeller=sellerNameBeforeEdit.includes(sellerNameInEditSizeWindow)
  assert.strictEqual(verifySeller,true,"Seller names are different ")
  const priceInEditSizeWindow=await cartPage.getItemPriceInEditSizeWindow(page)
  assert.equal(priceInEditSizeWindow,price,"Item prices are different")
  await cartPage.clickSwitchSeller(page)
  await cartPage.isSellerSelected(page)
  await cartPage.switchSeller(page)
  await cartPage.clickOnSizeInPwa(page)
  const sellerNameInEditSizeWindowAfterSwitchingSeller=await cartPage.getSellerNameinEditSizeWindow(page)
 
  var verifySellerAfterSwitching=sellerNameInEditSizeWindow.includes(sellerNameInEditSizeWindowAfterSwitchingSeller)
  console.log("*** the value is :"+verifySellerAfterSwitching)
  assert.strictEqual(verifySellerAfterSwitching,false,"Seller name is same ")
  await cartPage.clickDoneOnSelectSellerWindow(page)
  const sellernameAfterSwitchingSeller=await cartPage.getSellerName(page)
  console.log("**** Seller name after switching seller is :"+sellernameAfterSwitchingSeller)
  const priceAfterSwitchingSeller=await cartPage.getItemPrice(page)
  assert.notEqual(price,priceAfterSwitchingSeller,"Price after switching seller is same")
  console.log("******** Price after switching seller ******"+priceAfterSwitchingSeller)
  console.log("**** Verified Switch seller ******")
})

it('TC:013->Verify the Address on Cart for non logged in user ', async () => {

  await homePage.searchFor(page, "10430798")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  console.log("go to cart page")
  const addressOnCart=await cartPage.verifyAddressOnCartSection(page)
  assert.strictEqual(addressOnCart,true,"Address on Cart section is not displayed")
  await cartPage.verifyAddressOnCartHeaderForNonLoggedInUser(page)
  await cartPage.clickOnAddressonCartHeader(page)
  await cartPage.verifyEnterPincodeForNonLoggedInUser(page)
  assert.strictEqual(await cartPage.verifyEnterPincodeSectionForNonLoggedInUser(page),true,"Enter pincode section is not displayed")
  await cartPage.clickOnCheckInEnterPincodeSection(page)
  assert.strictEqual(await cartPage.verifyErrormessageOnClickOfCheck(page),true,"Error message is not displayed on click of Check ")
  await cartPage.closeAddressOnCartWindow(page)

})

it('TC:014->Verify add address on cart page',async () =>
{
  await loginPage.login(page)
  await homePage.searchFor(page, "10430798")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  const addAddress=await cartPage.addAddressOnCartPage(page)
  assert.strictEqual(addAddress,true,"Address not added sucessfully")
  
})

it('TC:015->Verify user can edit size in cart page', async () => {
  await homePage.searchFor(page, "6920579")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  console.log("go to cart page")
  const sizeBeforeEdit=await cartPage.getSize(page)
  console.log("***** Size before edit is: "+sizeBeforeEdit)
  await cartPage.clickOnSize(page)
  await cartPage.editSizeInWindow(page)
  await cartPage.clickDoneOnSelectSellerWindow(page)
  await cartPage.clickDoneInPwa(page)
  const sizeAfterEdit=await cartPage.getSize(page)
  console.log("**** size after edit is :"+sizeAfterEdit)
  assert.notEqual(sizeBeforeEdit,sizeAfterEdit,"Both the sizes are same ")

})

it('TC:016->Verify Free delivery for orders greater than shipping fee threshold', async () => {
  await loginPage.login(page)
  await homePage.searchFor(page, "11369178")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  console.log("go to cart page")
  assert.strictEqual(await cartPage.verifyFreeDeliveryHeader(page),true,"Delivery charge is applicable")
  assert.strictEqual(await cartPage.verifyFreeDeliveryInPriceBlockSection(page),true,"Free delivery is not displayed")
  console.log("Delivery is free for orders greater than shipping fee threshold ")
})

it('TC:017->Verify change address on cart page', async() =>
{
  await loginPage.login(page)
  await homePage.searchFor(page, "10430798")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.changeAddressOnCartPage(page)
})

it('TC:018->verify the error message when user enters invalid coupon code ', async() =>
{
  await loginPage.login(page)
  await homePage.searchFor(page, "10430798")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  console.log("go to cart page")
  await cartPage.clickOnCoupon(page)
  assert.strictEqual(await cartPage.verifyApplyCouponHeader(page),true,"Apply Coupon Header is not displayed ")
  await cartPage.enterInvalidCouponCode(page)
  await cartPage.clickOnCheckInCouponWindow(page)
  assert.strictEqual(await cartPage.verifyCouponErrorMessage(page),true,"Error message  is not displayed ")
  await cartPage.clickOnApplyCouponInCouponwindow(page)

})

it('TC:019->Verify Offer section in cart Page', async() =>
{
  
  await homePage.searchFor(page, "10430798")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  console.log("go to cart page")
  assert.strictEqual(await cartPage.verfiyOfferSectionDisplayed(page),true," Offer section is not displayed ")
  await cartPage.clickOnShowMoreInOfferSection(page)
  assert.strictEqual(await cartPage.verifyOfferSectionHeader(page),true," Offer section header is not displayed ")

})

it('TC:020->Verify Try and buy section in Address Page', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "11369178")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  console.log("go to Address Page")
  assert.strictEqual( await addressPage.verifyTryAndBuySection(page),true," Try and buy  section is not displayed ")
  assert.strictEqual(await addressPage.verifyTryandBuyFree(page),true,"Try and buy is not free")
  await addressPage.clickOnTryAndBuyCheckoBox(page)
  await addressPage.verifyTryAndBuyFreeInPriceBreakUp(page)
  assert.strictEqual(await addressPage.verifyTryAndBuyFreeInPriceBreakUp(page),true,"Try and buy price is not free in price brakup")
  await addressPage.clickOnHowItWorks(page)
  assert.strictEqual( await addressPage.verifyTryAndBlockImage(page),true," Try and buy image is not displayed ")
  await addressPage.closeTryAndBuyBlockImage(page)
  
})

it('TC:021->Verify Try and buy is not apllicable for Cart value<1119', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "10206735")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  console.log("go to Address Page")
  assert.strictEqual(await addressPage.verifyTryAndBuySection(page),true," Try and buy  section is not displayed ")
 assert.strictEqual(await addressPage.verifyTryAndBuyHeaderForLesserCartValue(page),true,"Try and buy is allowed ")
 assert.strictEqual(await addressPage.verifyTryAndBuyIsNotAllowedForLesserCartValue(page),true,"Try and buy is allowed where cart<1119")
})

it('TC:022->Verify Try and buy is not apllicable for Cosmetic/Inner wears', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "55269")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  console.log("go to Address Page")
  assert.strictEqual(await addressPage.verifyTryAndBuySection(page),false," Try and buy  section is  displayed for cosmetics/Inner wear ")
 
})

it('TC:023->Verify delivery estimate section in address ', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  assert.strictEqual(await addressPage.verifyDeliveryEstimatedSection(page),true," Estimated deliery section is not displayed ")
  assert.strictEqual(await addressPage.verifyDeliveryDate(page),true,"Estimated delivery date is not displayed")
})

it('TC:024->Verify Offer section in payments page', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue")
  assert.strictEqual(await paymentsPage.verfiyOfferSectionDisplayed(page),true," Offer section is not displayed ")
  await paymentsPage.clickOnShowMoreInOfferSection(page)
  assert.strictEqual(await paymentsPage.verifyOfferSectionHeader(page),true," Offer section header is not displayed ")

})

it('TC:025->Verify price in cart /Address /Payments page are same ', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  const cartPrice=await cartPage.getTotalPrice(page)
  console.log("the total price in cart is :"+cartPrice)
  await cartPage.clickOnPlaceOrder(page)
  const addressPrice=await addressPage.getTotalPrice(page)
  console.log("the total price in address page is :"+addressPrice)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue,Navigated to payments page")
  const paymentsPrice= await paymentsPage.getTotalPrice(page)
  console.log("the total price in payemnts page is :"+paymentsPrice)
  assert.equal(cartPrice,addressPrice,"Thr price in cart and address is not same ")
  assert.equal(cartPrice,paymentsPrice,"Thr price in cart and payments is not same ")
  assert.equal(addressPrice,paymentsPrice,"Thr price in address and payments is not same ")

})

it('TC:026->Verify user can place order with already linked Gift card ', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue,Navigated to payments page")
  assert.strictEqual(await paymentsPage.verifyGiftCardPaymentOption(page),true,"Gift card section is not displayed")
  await paymentsPage.clickOnApplyGiftCard(page)
  assert.strictEqual(await paymentsPage.verifyApplyGiftcardHeader(page),true,"Gift card header is not displayed ")
  await paymentsPage.enterGiftCardDetails(page)
  assert.strictEqual(await paymentsPage.verifyErrorMessageOnAddingGiftCard(page),true,"Error message is not displayed")
})

it('TC:027->Verify Order placement flow with EMI payment Option ', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue,Navigated to payments page")
  assert.strictEqual(await paymentsPage.verifyAndClickEMIPaymentOption(page),true,"EMI payment option  is not displayed")
  assert.strictEqual(await paymentsPage.verifyEMIOptionHeader(page),true,"EMI header is not displayed ")
  await paymentsPage.selectEMIOption(page)
  await paymentsPage.clickEMIPaynow(page)
  await paymentsPage.waitForPaymentsPageLoader(page)
  await paymentsPage.verifyRedirectionForEMIOption(page)

})


it('TC:028->Verify Order placement flow with Credit/Debit Card Payment option ', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue,Navigated to payments page")
  assert.strictEqual(await paymentsPage.verifyAndClickCreditDebitCardPaymentOption(page),true,"Credit /Debit card payment option  is not displayed")
  assert.strictEqual(await paymentsPage.verifyCreditDebitCardOptionHeader(page),true,"Credit /Debit card header is not displayed ")
  assert.strictEqual(await paymentsPage.verifyOnClickOfSaveCardInfoIcon(page),true,"On click of Saved card info icon Modal has not opened")
  assert.strictEqual(await paymentsPage.verifyOnClickOfCVVInfoIcon(page),true,"On click of CVV  info icon Modal has not opened")
  await paymentsPage.enterCreditDebitCardDetails(page)
 
})

it('TC:029->Verify Order placement flow with Wallets Payment option ', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue,Navigated to payments page")
  assert.strictEqual(await paymentsPage.verifyAndClickWalletPaymentOption(page),true,"wallet payment option  is not displayed")
  assert.strictEqual(await paymentsPage.verifyWalletOptionHeader(page),true,"wallet header is not displayed ")
  await paymentsPage.selectWalletOption(page)
  var walletSelected=await paymentsPage.getSelectedWalletName(page)
  await paymentsPage.clickWalletPaynow(page)
  await paymentsPage.waitForPaymentsPageLoader(page)
  var url=await paymentsPage.verifyRedirectionForWalletOption(page)
  var isRedirectedToSelectedWalletOption=url.includes(walletSelected)
 assert.equal(isRedirectedToSelectedWalletOption,true,"The redirection to selected wallet not happened")

})

it('TC:030->Verify Order placement flow with Phone pe UPI Payment option ', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue,Navigated to payments page")
  assert.strictEqual(await paymentsPage.verifyAndClickUPIPaymentOption(page),true,"UPI payment option  is not displayed")
  assert.strictEqual(await paymentsPage.verifyUPIOptionHeader(page),true,"wallet header is not displayed ")
  await paymentsPage.selectPhonePeOption(page)
  await paymentsPage.clickPhonePeUPIPaynow(page)
  await paymentsPage.waitForPaymentsPageLoader(page)
  assert.equal(await paymentsPage.verifyRedirectionForPhonePe(page),true,"Failed to redirect to phone pe ")

})

it('TC:031->Verify Error message if user enters invalid UPI ID ', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue,Navigated to payments page")
  assert.strictEqual(await paymentsPage.verifyAndClickUPIPaymentOption(page),true,"UPI payment option  is not displayed")
  assert.strictEqual(await paymentsPage.verifyUPIOptionHeader(page),true,"UPI header is not displayed ")
  await paymentsPage.selectOtherUPIOption(page)
  await paymentsPage.enterInvalidUPIID(page)
  await paymentsPage.verifyOnClickOfSavedUPIInfoIcon(page)
  await paymentsPage.clickUPIIDPaynow(page)
  console.log("clicked PAynow")
  await paymentsPage.waitForPaymentsPageLoader(page)
  assert.strictEqual(await paymentsPage.verifyValidationForUPI(page),true,"Error message is not displayed ")

})

it('TC:032->Verify Order placement flow with Gpay payment option', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue,Navigated to payments page")
  assert.strictEqual(await paymentsPage.verifyAndClickUPIPaymentOption(page),true,"UPI payment option  is not displayed")
  assert.strictEqual(await paymentsPage.verifyUPIOptionHeader(page),true,"UPI header is not displayed ")
  await paymentsPage.selectGpayUPIOption(page)
  await paymentsPage.enterValidUPIID(page)
  await paymentsPage.verifyOnClickOfSavedUPIInfoIcon(page)
  await paymentsPage.clickGpayUPIIDPaynow(page)
  await paymentsPage.waitForPaymentsPageLoader(page)
  assert.equal(await paymentsPage.verifyRedirectionForGpay(page),true,"Failed to redirect to Gpay UPI")

})

it('TC:033->Verify Order placement flow with other Netbanking Payment option ', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue,Navigated to payments page")
  assert.strictEqual(await paymentsPage.verifyAndClickNetbankingPaymentOption(page),true,"Net banking payment option  is not displayed")
  assert.strictEqual(await paymentsPage.verifyNetbankingHeader(page),true,"Net banking header is not displayed ")
  await paymentsPage.selectNetBankingFromDropDown(page)
  var netbankingSelected=await paymentsPage.getSelectedDropdownNetbankingName(page)
  await paymentsPage.clickOthertNetbankingPaynow(page)
  await paymentsPage.waitForPaymentsPageLoader(page)
  var url=await paymentsPage.verifyRedirectionForNetbankingOption(page)
  var isRedirectedToSelectedNetbankingOption=url.includes(netbankingSelected)
  assert.equal(isRedirectedToSelectedNetbankingOption,true,"The redirection to selected netbanking not happened")

})

it('TC:034->Verify Order placement flow with default Netbanking Payment option ', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue,Navigated to payments page")
  assert.strictEqual(await paymentsPage.verifyAndClickNetbankingPaymentOption(page),true,"Net banking payment option  is not displayed")
  assert.strictEqual(await paymentsPage.verifyNetbankingHeader(page),true,"Net banking header is not displayed ")
  await paymentsPage.selectDefaultNetBankingOption(page)
 var netbankingSelected=await paymentsPage.getSelectedNetbankingName(page)
  await paymentsPage.getSelectedNetbankingName(page)
  await paymentsPage.clickNetBankingPaynow(page)
  await paymentsPage.waitForPaymentsPageLoader(page)
  var url=await paymentsPage.verifyRedirectionForNetbankingOption(page)
  var isRedirectedToSelectedNetbankingOption=url.includes(netbankingSelected)
  assert.equal(isRedirectedToSelectedNetbankingOption,true,"The redirection to selected netbanking not happened")

})

it('TC:035->Verify Order placement flow with COD', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue,Navigated to payments page")
  assert.strictEqual(await paymentsPage.verifyAndClickCODPaymentOption(page),true,"COD payment option  is not displayed")
  assert.strictEqual(await paymentsPage.verifyCODHeader(page),true,"COD header is not displayed ")
  assert.strictEqual(await paymentsPage.verifyCaptchaSectionDisplayed(page),true,"Captcha has not displayed for COD payment mode")
  assert.strictEqual(await paymentsPage.verifyCODInfoMessage(page),true,"COD info message is not displayed")
  await paymentsPage.clickOnPlaceOrder(page)
  assert.strictEqual(await paymentsPage.verifyErrorMessageForCOD(page),true,"Error message is not displayed for COD option ")
})

it('TC:036->Verify user is able to add new Address and Remove Added Address ', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickAddNewAddressButton(page)
  assert.strictEqual(await addressPage.verifyAddNewAddressHeader(page),true," Add new Address header is not displayed ")
  assert.equal(await addressPage.addAddress(page),true,"Address not added successfully")
  await addressPage.clickAddEditAddressButton(page)
  await addressPage.removeAddress(page)
  console.log("Removed added address successfully")
})

it('TC:037->Verify Address scoring for non NCR region ', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickAddNewAddressButton(page)
  assert.strictEqual(await addressPage.verifyAddNewAddressHeader(page),true," Add new Address header is not displayed ")
  await addressPage.addAddressforNonNCR(page)
  assert.strictEqual(await addressPage.verifyBadAddressScoringError(page),true," Bad address scoring Error message is not displayed")
  await addressPage.verifyBadAddressScoring(page)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue")

})

it('TC:038->Verify error message for non servicable address ', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickAddNewAddressButton(page)
  assert.strictEqual(await addressPage.verifyAddNewAddressHeader(page),true," Add new Address header is not displayed ")
  await addressPage.addNonServicableAddress(page)
  assert.strictEqual(await addressPage.verifyisAddressBlockErrorDisplayed(page),true,"Address block error is not displayed")
  assert.strictEqual(await addressPage.getAddressBlockError(page),true,"Appropriate  error message is not displayed")
  assert.strictEqual(await addressPage.verifyNonServicableHeaderMessage(page),true,"Non Servicable Error message is not displayed in header ")
  assert.strictEqual(await addressPage.verifyIsGoToBagDisplayed(page),true,"Go to bag button in error message is not displayed")
  await addressPage.clickGoToBag(page)
  assert.equal(await cartPage.verifyServicabilityErrorOnCartPage(page),true,"Non servicable error message is not displayed on cart page")
  assert.equal(await cartPage.verifyItemsNotServicableMessage(page),true,"Item not servicable error message is not displayed")
  await cartPage.clickOnPlaceOrder(page)
  assert.equal(await cartPage.verifyNonServicableErrorWindowOnClickOfPlaceOrder(page),true,"Non servicable message is not displayed on click of place order")
  await cartPage.clickOnCloseWindow(page)
  await cartPage.changeAddressOnCartPage(page)

})

it('TC:039-> Verify giftcard purchase flow', async() =>
{
    await loginPage.login(page)
    await giftCardPage.purchaseGiftCard(page)
    assert.strictEqual(await paymentsPage.verifyGiftCartContextOnPaymentPage(page),true," Failed to load payment page for giftcard purchase flow")
    assert.strictEqual(await paymentsPage.verifyAndClickNetbankingPaymentOption(page),true,"Net banking payment option  is not displayed")
    assert.strictEqual(await paymentsPage.verifyNetbankingHeader(page),true,"Net banking header is not displayed ")
    await paymentsPage.selectNetBankingFromDropDown(page)
    var netbankingSelected=await paymentsPage.getSelectedDropdownNetbankingName(page)
    await paymentsPage.clickOthertNetbankingPaynow(page)
    await paymentsPage.verifyLowSuccessRateMessageForNetbanking(page)
    await paymentsPage.waitForPaymentsPageLoader(page)
    var url=await paymentsPage.verifyRedirectionForNetbankingOption(page)
    var isRedirectedToSelectedNetbankingOption=url.includes(netbankingSelected)
    assert.equal(isRedirectedToSelectedNetbankingOption,true,"The redirection to selected netbanking not happened")
})

it('TC:040->Verify Order placement flow with Saved VPA Option ', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue,Navigated to payments page")
  assert.strictEqual(await paymentsPage.verifySavedUPIOptionDisplayed(page),true,"Saved UPI payment option  is not displayed")
  await paymentsPage.verifySavedUPIOption(page)
  await paymentsPage.waitForPaymentsPageLoader(page)
  assert.strictEqual(await paymentsPage.verifyRedirectionForGpay(page),true,"Failed to redirect to UPI Page")

})

it('TC:041->Verify Order placement flow with Saved Card Option ', async() =>
{
  await loginPage.login(page)
  await homePage.clickOnBagIcon(page)
  console.log("go to cart page")
  await cartPage.removeAllItemsFromCart(page)
  await cartPage.clickOnMyntraLogo(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue,Navigated to payments page")
  assert.strictEqual(await paymentsPage.verifySavedUPIOptionDisplayed(page),true,"Saved UPI payment option  is not displayed")
  await paymentsPage.verifySavedCardOption(page)
  
})

it('TC:042-> Verify giftcard purchase flow with new giftcard revamp changes', async() =>
{
    await loginPage.login(page)
    await giftCardPage.purchaseGiftCardWithNewGiftcardUiChanges(page)
    assert.strictEqual(await paymentsPage.verifyGiftCartContextOnPaymentPage(page),true," Failed to load payment page for giftcard purchase flow")
    assert.strictEqual(await paymentsPage.verifyAndClickNetbankingPaymentOption(page),true,"Net banking payment option  is not displayed")
    assert.strictEqual(await paymentsPage.verifyNetbankingHeader(page),true,"Net banking header is not displayed ")
    await paymentsPage.selectNetBankingFromDropDown(page)
    var netbankingSelected=await paymentsPage.getSelectedDropdownNetbankingName(page)
    await paymentsPage.clickOthertNetbankingPaynow(page)
    await paymentsPage.verifyLowSuccessRateMessageForNetbanking(page)
    await paymentsPage.waitForPaymentsPageLoader(page)
    var url=await paymentsPage.verifyRedirectionForNetbankingOption(page)
    var isRedirectedToSelectedNetbankingOption=url.includes(netbankingSelected)
    assert.equal(isRedirectedToSelectedNetbankingOption,true,"The redirection to selected netbanking not happened")
})

it('TC:043->Verify Order placement flow with Myntra Credit Option ', async() =>
{
  await loginPage.myntraCreditlogin(page)
  await homePage.searchFor(page, "13078274")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  await addressPage.clickContinue(page)
  console.log("Clicked Continue,Navigated to payments page")
  await paymentsPage.verifyMyntraCreditPaymentOption(page)
  await paymentsPage.verifyMyntraCreditPriceInPriceBreakupIsSameAsMcUsedBalance(page)
  assert.strictEqual(await paymentsPage.verifyAndClickUPIPaymentOption(page),true,"UPI payment option  is not displayed")
  await paymentsPage.selectPhonePeOption(page)
  await paymentsPage.clickPhonePeUPIPaynow(page)
  await paymentsPage.verify2FAWindow(page)
  var contactNumber=await paymentsPage.selectMobileNumberToGetOTP(page)
  console.log("the phone number selected is :"+contactNumber)
  await paymentsPage.clickPhoneNumberin2FAscreen(page)
  assert.strictEqual(await paymentsPage.verifyOTPScreen(page),true,"OTP screen is not displayed")
  var phoneNumber=await paymentsPage.getPhoneNumberInOTPScreen(page)
  console.log("the phone number displayed is :"+phoneNumber)
  var verifyPhoneNumber=phoneNumber.includes(contactNumber) 
  assert.strictEqual(verifyPhoneNumber,true,"Phone numbers are different")
  await paymentsPage.clickSubmitInOTPScreen(page)
  assert.strictEqual(await paymentsPage.verifyerrorMessageForInvalidOTP(page),true,"The error message is not displayed")
  
})

it('TC:044-> Verify giftcard purchase if there is no saved addresses', async() =>
{
    await loginPage.login(page)
    await homePage.searchFor(page, "10430798")
    await pdpPage.selectSize(page)
    await pdpPage.addProductPdpToCart(page)
    await cartPage.clickOnPlaceOrder(page)
    assert.strictEqual(await addressPage.removeAllAddresses(page),true," Failed to remove all the address in address page")
    await giftCardPage.purchaseGiftCardForNewUser(page)
    assert.strictEqual(await paymentsPage.verifyGiftCartContextOnPaymentPage(page),true," Failed to load payment page for giftcard purchase flow")
    assert.strictEqual(await paymentsPage.verifyAndClickNetbankingPaymentOption(page),true,"Net banking payment option  is not displayed")
    assert.strictEqual(await paymentsPage.verifyNetbankingHeader(page),true,"Net banking header is not displayed ")
    await paymentsPage.selectNetBankingFromDropDown(page)
    var netbankingSelected=await paymentsPage.getSelectedDropdownNetbankingName(page)
    await paymentsPage.clickOthertNetbankingPaynow(page)
    await paymentsPage.verifyLowSuccessRateMessageForNetbanking(page)
    await paymentsPage.waitForPaymentsPageLoader(page)
    var url=await paymentsPage.verifyRedirectionForNetbankingOption(page)
    var isRedirectedToSelectedNetbankingOption=url.includes(netbankingSelected)
    assert.equal(isRedirectedToSelectedNetbankingOption,true,"The redirection to selected netbanking not happened")
})

it('TC:045-> Verify giftcard purchase if there is no saved addresses with new giftcard revamp changes', async() =>
{
  await loginPage.login(page)
  await homePage.searchFor(page, "10430798")
  await pdpPage.selectSize(page)
  await pdpPage.addProductPdpToCart(page)
  await cartPage.clickOnPlaceOrder(page)
  assert.strictEqual(await addressPage.removeAllAddresses(page),true," Failed to remove all the address in address page")
  await giftCardPage.purchaseGiftCardWithNewGiftcardUiChanges(page)
  assert.strictEqual(await paymentsPage.verifyGiftCartContextOnPaymentPage(page),true," Failed to load payment page for giftcard purchase flow")
  assert.strictEqual(await paymentsPage.verifyAndClickNetbankingPaymentOption(page),true,"Net banking payment option  is not displayed")
  assert.strictEqual(await paymentsPage.verifyNetbankingHeader(page),true,"Net banking header is not displayed ")
  await paymentsPage.selectNetBankingFromDropDown(page)
  var netbankingSelected=await paymentsPage.getSelectedDropdownNetbankingName(page)
  await paymentsPage.clickOthertNetbankingPaynow(page)
  await paymentsPage.verifyLowSuccessRateMessageForNetbanking(page)
  await paymentsPage.waitForPaymentsPageLoader(page)
  var url=await paymentsPage.verifyRedirectionForNetbankingOption(page)
  var isRedirectedToSelectedNetbankingOption=url.includes(netbankingSelected)
  assert.equal(isRedirectedToSelectedNetbankingOption,true,"The redirection to selected netbanking not happened")
})

})




