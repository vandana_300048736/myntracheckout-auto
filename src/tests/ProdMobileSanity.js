import { expect, assert } from 'chai'
import { step } from 'mocha-steps'
import Page from '../builder'
import LoginPage from '../pages/LoginPage'
import Homepage  from '../pages/HomePage'
import WishlistPage from '../pages/WishlistPage'
import PdpPage from '../pages/PdpPage'
import CartPage from '../pages/CartPage'
import ListPage from '../pages/ListPage'
import pplocator from '../objectRepository/PdpPageLocators'


describe('Prod Mobile Sanity Test', () => {
  let page
  let loginPage
  let homePage
  let wishlistPage
  let pdpPage
  let cartPage
  let listPage

  
  
  beforeEach(async () => {

    page = await Page.build(process.env.platform)
    console.log("BEFORE EACH USER AGENT++++++++++++"+await page.userAgent())
    console.log("Platform Name is:____"+process.env.platform)

    loginPage = await new LoginPage(page)
    homePage = await new Homepage(page)
    wishlistPage = await new WishlistPage(page)
    pdpPage = await new PdpPage(page)
    listPage = await new ListPage(page)
    cartPage = await new CartPage(page)
  })

  afterEach(async () => {
   await page.close()
  })

  it('TC:00001->Verify user is able to verify insider points', async () => {
    
    await loginPage.login(page)
    await cartPage.setCookies()

    const fs = require('fs').promises;

    // ... Move cookie to cookie.json file code
    const cookies = await page.cookies();
    await fs.writeFile('./cookies.json', JSON.stringify(cookies, null, 2));   

    // ... Read cookie to cookie.json file code
    const cookiesString = await fs.readFile('./cookies.json');
    const cookiess = JSON.parse(cookiesString);

     await page.setCookie(...cookies);
     await homePage.navigateToCart(page)
     console.log(cookies)
     console.log(cookiess)
     await cartPage.clearCart(page)
     await homePage.navigateToPlp(page)
     await listPage.clickFirstProduct(page)
     const itemPriceInPDP = await pdpPage.getTextPicePDP(page)
     console.log("Price of the item is:********************"+itemPriceInPDP)
     const expectedinsiderpoints=await cartPage.getExpectedInsider(page,itemPriceInPDP)
     console.log("Expected Insider points is: "+expectedinsiderpoints)
     await pdpPage.addSingleSizeItemFromPDPToCart(page)
     await pdpPage.addProductPdpToCart(page)
     console.log("Returned from PDP to Cart___________")
     console.log("Trying Scrolling now+++++++++++++")
     await page.autoScroll(page)
     console.log("Scrolling DONE+++++++++++++++++")    
     await cartPage.verifyInsiderPoints(page,expectedinsiderpoints)     
  })
})
