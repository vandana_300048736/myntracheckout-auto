//import DataDrive from "../DataDrive/LoginCredentials.json"
export default class LoginPageLocators {
    constructor(page) {
      this.page = page
      this.env = process.env.platform
      this.loginDetails = {
       //emailId : "9743644000",
       //password: "Welcome@123",

        emailId : "9916444504",
       password: "Test123$",
       //emailId : "9986780772",
       //password: "123456",
       // emailId : "9620117709",
        //password: "Myntra@2020",
        mobiledummy: "9999999991",
        myntraCreditEmailId:"9731676232",
        myntraCreditPassword:"TCS@2015"

      }
      this.loginLocators = {

        user_id :{
          pwa:"input[id='mobileNumberPass']",
          desktop: "input[id='mobileNumberPass']",
          android: "#mobileNumberPass",
          ios: "#mobileNumberPass"
        },
        user_password :{
          pwa: '.form-control.has-feedback',
          desktop: ".form-control.has-feedback",
          android:"#reactPageContent > div > div > form > div > div:nth-child(2) > input",
          ios:"#reactPageContent > div > div > form > div > div:nth-child(2) > input"
      
        },
        login_button:{
          pwa: ".btn.primary.lg.block.submitButton",
          desktop: ".btn.primary.lg.block.submitButton",
          android: "#reactPageContent > div > div > form > div > div:nth-child(3) > button",
          ios: "#reactPageContent > div > div > form > div > div:nth-child(3) > button"
     
        },
        login_Section:
        {
          pwa:".signInContainer",
          desktop:".signInContainer"
        },
        user_mobile_login:
        {
          android: "#reactPageContent > div > div > div.signInContainer > div.mobileInputContainer > div.form-group > input",
          ios: "#reactPageContent > div > div > div.signInContainer > div.mobileInputContainer > div.form-group > input"
     
        },
        mob_continueBtn:
        {
          android: "#reactPageContent > div > div > div.signInContainer > div.mobileInputContainer > div.submitBottomOption",
          ios: "#reactPageContent > div > div > div.signInContainer > div.mobileInputContainer > div.submitBottomOption"
     
        },
        mob_login_with_password:
        {
          android: "#reactPageContent > div > div:nth-child(3) > span",
          ios: "#reactPageContent > div > div:nth-child(3) > span"
     
        },
        mob_username:
        {
          android: "#mobileNumberPass",
          ios: "#mobileNumberPass"
   
        },
        mob_password:
        {
          android: "#reactPageContent > div > div > form > div > div:nth-child(2) > input",
          ios: "#reactPageContent > div > div > form > div > div:nth-child(2) > input"    
        },
        mob_clickLoginBtn:
        {
          android: "#reactPageContent > div > div > form > div > div:nth-child(3) > button",
          ios: "#reactPageContent > div > div > form > div > div:nth-child(3) > button"  
        }

      }
    }
}