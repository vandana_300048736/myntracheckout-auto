export default class PlpPageLocators {
    constructor(page) {
      this.page = page
      this.env = process.env.platform
      this.validPincode = "560068"
      this.invalidPincode = "000000",
      
      this.PlpPageLocators = {

        addToBagButton :{
          pwa:".//div[contains(@class,'padding-sm pdp-action-buttons fixed visible')]//button[contains(@class,'flex center full-width')]",
          desktop: "div.application-base main.pdp-pdp-container div.pdp-details.common-clearfix div.pdp-description-container div:nth-child(3) div.pdp-action-container.pdp-fixed > div.pdp-add-to-bag.pdp-button.pdp-flex.pdp-center",
          android:""
        }

      
        
      }
    }
}