export default class WishlistPageLocators {
    constructor(page) {
      this.page = page
      this.env = process.env.platform
      this.wishlistLocators = {

        removeIconLength :{
          pwa: '//div[@class="remove-button"]',
          desktop: '//div[@class="itemcard-removeIcon"]'
        },
        removeIcon:{
            pwa:".remove-button",
            desktop: ".itemcard-removeIcon"
        },
        emptyWishlist:{
            pwa: '//div[text()="WISHLIST EMPTY"]',
            desktop: '//div[text()="YOUR WISHLIST IS EMPTY"]'
        },
        moveToBag:{
            pwa: ' .action-button.move-to-bag',
            desktop: '.itemcard-moveToBag.itemcard-boldFont'
        },
        selectSize:{
            pwa: '.btn.default.outline.sizeSelect-btn',
            desktop: '.sizeselect-sizeButton'
        },
        SelectSizeDone:{
            pwa: '.default.sizeSelect-doneButton.sizeSelect-doneButtonActive',
            desktop: '.sizeselect-done'
        },
        bagCount:{
            pwa:'.badge',
            desktop: ".desktop-badge.desktop-melon"
        },
        cartCount:{
            desktop:'//span[contains(@class,"desktop-badge")]',
            pwa:'//span[contains(@class,"badge")]'
        },
        wishlistCount:{
            desktop:'.index-count.index-heading',
            pwa:'.header-sub-heading'

        },
        loginButtonWishlist:{
            desktop:'.wishlistLogin-button',
            pwa:'.form-control.mobileNumberInput'
        },
        bagIcon:
        {
            desktop:"span[class='myntraweb-sprite desktop-iconBag sprites-headerBag']",
            pwa:"#header-cart-icon"
        }
            

        
        }
    }
}