export default class CartPageLocators {
    constructor(page) {
      this.page = page
      this.env = process.env.platform
      this.giftwrapMessage = "Myntra"
      this.invalidCouponCode="abc"

      this.addressDetails = {
        name : "Automation test",
        mobileNumber: "9620117709",
        pincode: "560022",
        streetAddress: "301 Mahaveer fair oaks  Narayanappa garden",
        locality: "Yeswanthpura",
      }

      this.CartPageLocators = {

        removeItems :{
          pwa:'//button[text()="Remove"]',
          desktop: '//button[text()="Remove"]',
          android:'//button[text()="Remove"]',
          ios:'//button[text()="Remove"]'
        },
        removeButton:{
            pwa:  '.inlinebuttonV2-base-actionButton.itemContainer-base-inlineButton.removeButton',
            desktop: '.inlinebuttonV2-base-actionButton.itemContainer-base-inlineButton.removeButton',
            android: '.inlinebuttonV2-base-actionButton.itemContainer-base-inlineButton.removeButton',
            ios: '.inlinebuttonV2-base-actionButton.itemContainer-base-inlineButton.removeButton'
          
        },
        nonServicableError:
        {
          pwa:".itemComponents-base-serviceabilityErrorMessage",
          desktop:".itemComponents-base-serviceabilityErrorMessage"
        },

        removeFromDialog:{
            pwa:"//button[@class='inlinebuttonV2-base-actionButton itemComponents-base-inlineButton' and text()='Remove']",
            desktop: "//button[@class='inlinebuttonV2-base-actionButton itemComponents-base-inlineButton' and text()='Remove']",
            android: "//button[@class='inlinebuttonV2-base-actionButton itemComponents-base-inlineButton' and text()='Remove']",
            ios: "//button[@class='inlinebuttonV2-base-actionButton itemComponents-base-inlineButton' and text()='Remove']"
        },
        movetoWishlist :{
          
        },
        emptyCart:{
            pwa: '.emptyCart-base-emptyText',
            desktop: ".emptyCart-base-emptyText",
            android: ".emptyCart-base-emptyText",
            ios: ".emptyCart-base-emptyText"
        },
        edityQty:{
          pwa: '.itemComponents-base-quantity',
          desktop: ".itemComponents-base-quantity"
        
        },
        editSizeButton:{
          pwa:".itemComponents-base-size",
          desktop:".itemComponents-base-size"
        },
        freeDeliveryText:
        {
          pwa:"//span[@class='shippingTip-base-tipBold']",
          desktop:"//span[@class='shippingTip-base-tipBold']"
        },
        freeDeliveryInPriceBlock:
        {
          pwa:"span[class='priceDetail-base-discount']",
          desktop:"span[class='priceDetail-base-discount']"
        },

        qtySelect:
              {
                pwa: "//div[@id='2']",
                desktop:"//div[@id='2']"
              }  ,
        sellerName:
        {
          pwa:"div[class='itemComponents-base-sellerData']",
          desktop:"div[class='itemComponents-base-sellerData']"
        },
        seller:{
          pwa:"span[class='sizeSelector-base-bold']",
          desktop:"span[class='sizeSelector-base-bold']"
        },
        itemPriceInEditSizeWindow:
        {
          pwa:"//span[@class='inlinePriceComponent-base-amount sizeSelector-base-amount']",
          desktop:"//span[@class='inlinePriceComponent-base-bold']"
        },
        itemPrice:
        {
          pwa:"//div[contains(@class,'itemComponents-base-price')]",
          desktop:"//div[contains(@class,'itemComponents-base-price')]"
          
        },
        totalPrice:
        {
          pwa:"//*[@class='priceDetail-base-total']/span[2]/span[2]",
          desktop:"//*[@class='priceDetail-base-total']/span[2]/span[2]"
        },
        editSize:
        {
          pwa:"//*[@class='sizeSelector-base-mobileList']/div[2]",
          desktop:"//div[contains(@class,'modal-base-modal dialogs-base-container')]/div/div[2]/div[2]/div[2]"
        },
        getSize:
        {
          pwa:"//*[@class='itemComponents-base-size']/span",
          desktop:"//*[@class='itemComponents-base-size']/span"
        },
        doneInEditsize:
        {
          pwa:"div[class='button-base-button sizeSelector-base-mobileButton ']"
        },
        switchSeller:
        {
          pwa:".dialogs-base-sellerCount",
          desktop:".dialogs-base-sellerCount"
        },
        sellerSelectedRadioButton:
        {
          pwa:".listSeller-base-checkedButton",
          desktop:"div.layout:nth-child(1) div.page div.desktop-base-cartLayout div.itemBlock-base-leftBlock div.itemContainer-base-itemMargin div.item-base-item div.itemContainer-base-item div.modal-base-container div.modal-base-modal.dialogs-base-container div:nth-child(2) div:nth-child(4) div.radioGroup-base-container.dialogs-base-ListSellerContainer div.listSeller-base-radioButtonSeller:nth-child(1) > svg.listSeller-base-radioIcon.listSeller-base-selectedRadio"
        },
        sellerUnselectedRadioButton:
        {
          pwa:"div.buttonV2-base-buttonContainer:nth-child(1) > div",
          desktop:"div.layout:nth-child(1) div.page div.desktop-base-cartLayout div.itemBlock-base-leftBlock div.itemContainer-base-itemMargin div.item-base-item div.itemContainer-base-item div.modal-base-container div.modal-base-modal.dialogs-base-container div:nth-child(2) div:nth-child(4) div.radioGroup-base-container.dialogs-base-ListSellerContainer div.listSeller-base-radioButtonSeller:nth-child(2) > svg.listSeller-base-radioIcon.listSeller-base-uncheckedRadio"
        },
        selectSellerDoneButton:
        {
          pwa:"",
          desktop:"//div[contains(@class,'button-base-button sizeSelector-base-desktopButton')]"
        },
        qtySelectDone:
                {
                pwa:".button-base-button  ",
                desktop:".button-base-button  "
                },
         qtyText:
         {
           pwa:"//*[@class='itemComponents-base-quantity']/span",
           desktop:"//*[@class='itemComponents-base-quantity']/span"
         },
      clickGiftWrap:{
        pwa:".giftWrap-base-container",
        desktop:".giftWrap-base-add"
      },
      enterRecepientName:{
        pwa:"//*[@id='recipientName']",
        desktop:"//*[@id='recipientName']"
      },
      enterMessage:{
      pwa:"//*[@for='message']",
      desktop:"//*[@for='message']"
      },
      enterSenderName:{
        pwa:"//*[@for='senderName']",
        desktop:"//*[@for='senderName']"
      },
      enterApplyGiftWrap:{
        pwa:"//div[contains(text(),'APPLY GIFT WRAP')]",
        desktop:"//div[contains(text(),'APPLY GIFT WRAP')]"
      },

      removeAppliedGiftwrap:
      {
        pwa:".giftWrap-base-closeIcon",
        desktop:".giftWrap-base-remove"
      },
      removeBulkItem:
      {
        pwa:"//*[@class='bulkActionStrip-mobileButtonContainer']/div[1]",
        desktop:"//*[@class='inlinebuttonV2-base-actionButton bulkActionStrip-desktopBulkRemove']"
      },
      closeGiftwrap:
      {
        pwa:"//div[@class='close-icon']//a[@class='linkClean']//*[local-name()='svg']",
        desktop:".modal-base-cancelIcon "
      },
      selectItemCheckbox:
      {
        pwa:".bulkActionStrip-inActiveIcon",
        desktop:".bulkActionStrip-inActiveIcon"
      },
      removeBulkItemConfirm:
      {
        pwa:"button[class='inlinebuttonV2-base-actionButton ']",
        desktop:"button[class='inlinebuttonV2-base-actionButton ']"
      },
      cartfillerSection:
      {
        pwa:".cartFiller-base-cartFiller",
        desktop:".cartFiller-base-cartFiller"
      },
      cartfillerAddtoBagButton:
      {
        pwa:"//div[contains(@class,'cartFiller-base-productWrapper')]/div[1]/div[2]/div[2]",
        desktop:"//div[contains(@class,'cartFiller-base-cartFiller')]//div[2]//div[2]//div[4]"
      },
      totalCartItem:
      {
        pwa: "#priceBlockHeader",
        desktop:"div[class='priceBlock-base-priceHeader']"
      },
      couponLoginButton:
      {
        pwa:".coupons-base-logIn",
        desktop:".coupons-base-logIn"
      },

      addressOnCartSection:
      {
        pwa:"div[class='addressStrip-base-mobileContainer']",
        desktop:".addressStrip-base-desktopContainer"

      },
      addressOnCartHeader:
      {
        pwa:".addressStrip-base-highlight",
        desktop:".addressStrip-base-highlight"
      },
      enterPincode:
      {
        pwa:"div.layout:nth-child(1) div.web div.page div:nth-child(1) div:nth-child(1) div.mobile-base-mobile div.addressStrip-base-mobileContainer:nth-child(1) > div.addressStrip-base-changeBtn",
        desktop:"div[class='addressStrip-base-changeBtn addressStrip-base-changeBtnDesktop']"
      },
      changeAddressButton:
      {
        pwa:"//div[contains(text(),'CHANGE')]",
        desktop:"//div[contains(text(),'CHANGE ADDRESS')]"
      },
      enterPincodeButton:
      {
        pwa:"//div[text()='ENTER PIN CODE']",
        desktop:"//div[text()='ENTER PIN CODE']"
      },
      addNewAddressButton:
      {
        pwa:"//div[@class='checkDelivery-base-addBlockAnchor']",
        desktop:"//div[contains(text(),'Add New Address')]"
      },
      enterPincodeSection:{
        pwa:".checkDelivery-base-pincodeContainer",
        desktop:".checkDelivery-base-pincodeContainer"

      },
      contactNameFiled:
      {
        pwa:"#name",
        desktop:"#name"
      },
      contactMobileNumberField:
      {
        pwa:"#mobile",
        desktop:"#mobile"
      },
      pincodeField:

      {
        pwa:"#pincode",
        desktop:"#pincode"
      },

      checkButton:
      {
        pwa:"//div[contains(@class,'checkDelivery-base-checkBtn checkDelivery-base-inValid')]",
        desktop:"//div[contains(@class,'checkDelivery-base-checkBtn checkDelivery-base-inValid')]"
      },
      pincodeErrormessage:
      {
        pwa:".inputV2-base-errorMessage ",
        desktop:".inputV2-base-errorMessage "
      },
      addressOnCartCloseIcon:
      {
        pwa:"//div[@class='modal-base-halfCard modal-base-iosHalfCard addressStrip-base-mobileModalContainer ']//*[local-name()='svg']",
        desktop:"//div[@class='modal-base-modal addressStrip-base-desktopModalContainer ']//*[local-name()='svg']"
      },

      streetAddressField:
      {
        pwa:"#streetAddress",
        desktop:"#streetAddress"
      },
      localityField:
      {
        pwa:"#locality",
        desktop:"#locality"
      },
      addressType:
      {
        pwa:"#addressType-office",
        desktop:"#addressType-office"
      },
      makeThisMyDefaultAddressCheckBox:
      {
        pwa:"//span[contains(text(),'Make this my default address')]",
        desktop:"//div[@class='addressFormUI-base-isDefault']"
      },
      addAddressButton:
      {
        pwa:"div.layout:nth-child(1) div.web div.page div:nth-child(1) div:nth-child(1) div.addressFormUI-base-container div.addressFormUI-base-footer > div.button-base-button.addressFormUI-base-saveBtn",
        desktop:"div.layout:nth-child(1) div.page div.desktop-base-cartLayout div.itemBlock-base-leftBlock div.modal-base-container:nth-child(2) div.modal-base-modal.addressStrip-base-desktopModalContainer.addressStrip-base-addressModal div.addressFormUI-base-container div.addressFormUI-base-footer > div.button-base-button.addressFormUI-base-saveBtn"
      },
      verifyName:
      {
        pwa:"//div[@class='addressStrip-base-addressName']/span",
        desktop:"//div[@class='addressStrip-base-addressName']/span"
      },
      verifyPincode:
      {
        pwa:"//div[@class='addressStrip-base-addressName']/div",
        desktop:"//div[@class='addressStrip-base-addressName']/div"

      },
      listOfaddressOnCartPage:
      {
        pwa:'div.addressDetails-base-addressTitle',
        desktop:'div.addressDetails-base-addressTitle'
      },
      pincodeFieldOnChangeAddress:
      {
        pwa:'#pincode',
        desktop:'#pincode'
      },
      isAddressSelected:
      {
        pwa:"//div[@class='radioButton-base-container checkDelivery-base-addressContainer ']",
        desktop:"//div[@class='radioButton-base-container checkDelivery-base-addressContainer ']"
      },
      applyCouponButton:
      {
        pwa:"div.coupons-base-container:nth-child(2) > a:nth-child(1) > div",
        desktop:"button[class='coupons-base-button']"
      },
      applyCoupounHeader:
      {
        pwa:"//div[contains(@class,'page-name')]",
        desktop:"//div[@class='couponsModal-base-couponsModalHeader']"
      },
      enterInvalidCoupon:
      {
        pwa:"//input[@id='coupon-input-field']",
        desktop:"//input[@id='coupon-input-field']"
      },
      clickCheckInCouponWindow:
      {
        pwa:"//div[contains(@class,'couponsForm-base-applyButton couponsForm-base-enabled')]",
        desktop:"//div[contains(@class,'couponsForm-base-applyButton couponsForm-base-enabled')]"
      },
      couponErrorMessage:
      {
        pwa:".couponsForm-base-errorMessage",
        desktop:".couponsForm-base-errorMessage"
      },
      clickApplyCouponInCouponWindow:
      {
        pwa:"#applyCoupon",
        desktop:"#applyCoupon"
      },
      offerSection:
      {
        pwa:"//div[contains(@class,'offersV2-base-container')]",
        desktop:"//div[contains(@class,'offersV2-base-container')]"
      },
      offerSectionHeader:
      {
          pwa:".offersV2-base-title",
          desktop:".offersV2-base-title"
      },
      showMoreButton:
      {
        pwa:".offersV2-base-more",
        desktop:".offersV2-base-more"
      },
      clickPlaceOrder:
      {
        pwa:"//button[normalize-space()='PLACE ORDER']",
        desktop:"//div[contains(text(),'Place Order')]"
      },
      clickMyntraLogo:
      {
        pwa:"//div[@class='back-icon']//a[@class='linkClean']//*[local-name()='svg']",
        desktop:"//a[@class='linkClean']//*[local-name()='svg']"
      },
      closeWindow:
      {
        pwa:"div.layout:nth-child(1) div.web div.page div:nth-child(1) div:nth-child(1) div.modal-base-container div.modal-base-halfCard.modal-base-iosHalfCard.cartModal-base-mobileModal > svg.modal-base-cancelIcon.cartModal-base-cancel:nth-child(1)",
        desktop:"div.layout:nth-child(1) div.page div:nth-child(1) div:nth-child(1) div.modal-base-container div.modal-base-modal.cartModal-base-desktopModal > svg.modal-base-cancelIcon.cartModal-base-cancel:nth-child(1)"
      },
      nonServicableModelError:
      {
        pwa:"span[class='cartModalComponent-base-modalHeaderText']",
        desktop:"span[class='cartModalComponent-base-modalHeaderText']"
      },
      itemNotServicable:
      {
        pwa:".headerComponents-base-nonServiceableHeader",
        desktop:".headerComponents-base-nonServiceableHeader"
      },
      clickPlaceOrderBtn:
      {
        android:"#placeOrderButton",
        ios:"#placeOrderButton"
      },
      insiderpoints:
      {
          android:"//div[@class='components-base-insiderInfoText']/span[@class='components-base-textGreen']",
          ios:"//div[@class='components-base-insiderInfoText']/span[@class='components-base-textGreen']"
     
      }
          
            
    }
}
}