export default class GiftCardPageLocators {

    constructor(page) 
    {

        this.page = page
        this.env = process.env.platform

        this.giftcardDetails = {
            mobileNumber: "9620117709",
            email: "testuser@gmail.com",
            name: "Mahaveer",
            amount:"1000"
          }

        this.GiftCardPageLocators = {

            userProfile :
            {
                pwa:'//button[text()="Remove"]',
                desktop: '//button[text()="Remove"]'
            },
            sendGiftCardButton:
            {   
                pwa:"button.styles-buy",
                desktop:"button.styles-buy"
            },
            nextButton:
            {
                pwa:"button.personalize-next",
                desktop:"button.personalize-next"
            },
            gcAmountFiled:
            {
                pwa:"#gcamount",
                desktop:""
            },
            dataAmount:
            {
                pwa:"div[data-amount='1000']",
                desktop:"div[data-amount='1000']"
            },
            mobileNumberField:
            {
                pwa:"#mobileNumber",
                desktop:"#mobileNumber"
            },
            emailField:
            {
                pwa:"#email",
                desktop:"#email"
            },
            recipientNameField:
            {
                pwa:"#to",
                desktop:"#to"
            },
            recipientNameFieldForNewGiftcardUI:
            {
                pwa:"#name"
            },
            previewLink:
            {
                pwa:"div.textButton-container "
            },
            closePreviewButtonAndPayButton:
            {
                pwa:"div.button-base-button  "
            },
            showPriviewButton:
            {
                pwa:"button.form-next",
                desktop:"button.form-next"
            },
            payButton:
            {
                pwa:"button.preview-proceed",
                desktop:"button.preview-proceed"
            }
            
        }
    }

}
