export default class HomePageLocators {
    constructor(page) {
      this.page = page
      this.env = process.env.platform
      this.searchQuery = "Nike"
        this.invalidSearchQuery = "2"
        
        this.HomePageLocators = {

            searchBoxIcon :{
              pwa:"#header-search-icon > svg",
              desktop: ".desktop-searchBar"
            },
            searchBoxText : {
                pwa: ".form-control.search-box-input",
                desktop: ".desktop-searchBar"
            },
            profileHamburgerIcon: {
                pwa: "#header-menu-icon",
                desktop: ".myntraweb-sprite.desktop-iconUser.sprites-headerUser"
            },

            profileUserEmail:{
                pwa : ".naviLevel.text-ellipsis.user-name",
                desktop : ".desktop-infoEmail"
            },

            autoSuggestSearchResults:
            {
                    pwa: ".search-results.clearfix",
                    desktop: ".desktop-suggestion.null"
            },
            pageNotFound:
            {
                pwa: ".nf-title",
                desktop: ".index-infoBig"
            },
            
            logoutDashboard:
            {
                pwa:'.dashboard-logoutButton',
                desktop:'.dashboard-logoutButton'

            },
            bagIcon:
            {
                pwa:"#header-cart-icon",
                desktop:"span[class='myntraweb-sprite desktop-iconBag sprites-headerBag']"
            }
        }
    }
}