export default class PaymentsPageLocators {
    constructor(page) {

     
      this.page = page
      this.env = process.env.platform
      this.paymentDetails = 
      {
        giftCardNumber:"6001220033525232",
        giftCardPin:"503080",
        creditCardNumber:"5555 5555 5555 4444",
        creditCardName:"test card",
        creditCardExpiry:"1125",
        creditCardcvv:"123",
        invalidUpiID:"test123",
        validUpiID:"vandana13.kgowda"
      }
    
    this.PaymentsPageLocators = 
    {
        offerSection:
        {
          pwa:"//div[contains(@class,'offersV2-base-container')]",
          desktop:"//div[contains(@class,'offersV2-base-container')]"
        },
        offerSectionHeader:
        {
            pwa:".offersV2-base-title",
            desktop:".offersV2-base-title"
        },
        showMoreButton:
        {
          pwa:".offersV2-base-more",
          desktop:".offersV2-base-more"
        },
        totalPrice:
        {
            pwa:"//*[@class='priceDetail-base-total']/span[2]/span[2]",
            desktop:"//*[@class='priceDetail-base-total']/span[2]/span[2]"
        },
        giftCardSection:
        {
            pwa:"//div[@class='giftcard-base-container paymentMobile-base-giftCardBlock']",
            desktop:"//div[@class='giftcard-base-container paymentDesktop-base-giftCardBlock']"
        },
        applyGiftCardButton:
        {
            pwa:"span[class='giftcard-base-apply']",
            desktop:"span[class='giftcard-base-apply']"
        },
        giftCardHeaderText:
        {
            pwa:"div[class='giftCardModal-base-heading']",
            desktop:"div[class='giftCardModal-base-heading']"
        },
        giftCardNumber:
        {
            pwa:"//input[@id='gcNumber']",
            desktop:"//input[@id='gcNumber']"
        },
        giftCardPin:
        {
            pwa:"//input[@id='gcPin']",
            desktop:"//input[@id='gcPin']"
        },
        addToMyntraCreditButton:
        {
            pwa:"//div[contains(@class,'button-base-button giftCardModal-base-addButton')]",
            desktop:"//div[contains(@class,'button-base-button giftCardModal-base-addButton')]"
        },
        giftCardErrorMessage:
        {
            pwa:"div.notifyContent.notifyerror:nth-child(1) > div",
            desktop:"div.notifyContent.notifyerror:nth-child(1) > div"
        },
        emiOption:
        {
            pwa:"#emi",
            desktop:"#emi"
        },
        emiHeader:
        {
            pwa:"div[class='emi-base-heading']",
            desktop:"div[class='emi-base-heading']"
        },
        selectEmiOption:
        {
            pwa:"//div[contains(@class,'emi-base-emiContainer')]/div/div[1]",
            desktop:"//div[contains(@class,'emi-base-emiContainer')]/div/div[1]"
        },
        emiPaynow:
        {
            
            desktop:"div.paymentSubOption-base-rowContainer:nth-child(1) > button"
        },
        placeOrderButtonPWA:
        {
            pwa:"#placeOrderButton"
        },
        emiRedirection:
        {
            pwa:"img[alt='Myntra']",
            desktop:"img[alt='Myntra']"
        },
        creditDebitCardOption:
        {
            pwa:"#card",
            desktop:"#card"
        },
        creditDebitCardHeader:
        {
            pwa:"div[class='card-base-heading']",
            desktop:"div[class='card-base-heading']"
        },
        creditCardNumber:
        {
            pwa:"//input[@id='cardNumber']",
            desktop:"//input[@id='cardNumber']"
        },
        creditCardcvv:
        {
            pwa:"//input[@id='cvv']",
            desktop:"//input[@id='cvv']"
        },
        creditCardName:
        {
            pwa:"//input[@id='cardName']",
            desktop:"//input[@id='cardName']"
        },
        creditCardExpiry:
        {
            pwa:"//input[@id='expiry']",
            desktop:"//input[@id='expiry']"
        },
        uncheckSaveCard:
        {
            pwa:"#saveCard-icon",
            desktop:"#saveCard-icon"
        },
        clickCreditDebitCardPaynow:
        {
            pwa:"//button[@id='placeOrderButton']",
            desktop:"//button[normalize-space()='PAY NOW']"
        },
        cvvInfoIconToolTip:
        {
            pwa:"//div[@class='cardForm-base-row cardForm-base-cvv']//*[local-name()='svg']",
            desktop:"//div[@class='cardForm-base-row cardForm-base-cvv']//*[local-name()='svg']"
        },
        cvvInfoModal:
        {
            pwa:"//*[@class='modal-base-modal cardForm-base-cvvInfoModal']",
            desktop:"//*[@class='modal-base-modal cardForm-base-cvvInfoModal']"
        },
        cvvCloseModal:
        {
            pwa:"//div[contains(@class,'modal-base-modal cardForm-base-cvvInfoModal')]//*[local-name()='svg']",
            desktop:"//div[contains(@class,'modal-base-modal cardForm-base-cvvInfoModal')]//*[local-name()='svg']"
        },
        savedCardInfoIcon:
        {
            pwa:"//*[@class='cardForm-base-savedInfoIcon']",
            desktop:"//*[@class='cardForm-base-savedInfoIcon']"
        },
        savedUPIDetails:
        {
            pwa:"div[class='upiComponents-base-saveInfo']",
            desktop:"div[class='upiComponents-base-saveInfo']"
        },
        savedCardDetails:
        {
            pwa:"div[class='cardForm-base-saveCardSuggestion']",
            desktop:"div[class='cardForm-base-saveCardSuggestion']"
        },
        creditCardLogo:
        {
            pwa:"div[class='sprite-cardv2-mastercard cardForm-base-cardType']",
            desktop:"div[class='sprite-cardv2-mastercard cardForm-base-cardType']"
        },
        walletOption:
        {
            pwa:"#wallet > div:nth-child(1) > span.accordianComponents-base-tabLabel:nth-child(1) > span",
            desktop:"#wallet"
        },
        selectWalletRadioButton:
        {
            pwa:"//*[@class='wallets-base-walletContainer wallets-base-mobileContainer']/div[1]/div[1]//*[@class='radioButton-base-radioIcon paymentSubOption-base-icon ']",
            desktop:"//*[@class='wallets-base-walletContainer']/div[2]/div[1]//*[@class='radioButton-base-radioIcon paymentSubOption-base-icon ']"
        },
        walletHeader:
        {
            pwa:"",
            desktop:"//div[@class='wallets-base-heading']"
        },
        walletName:
        {
            pwa:"div.paymentSubOption-base-row  span",
            desktop:"div.paymentSubOption-base-row  span"
        },
        walletPaynow:
        {
            pwa:"",
            desktop:"//div[contains(@class,'wallets-base-walletContainer')]//div[2]//button[1]"
        },
        upiOption:
        {
            pwa:"#upi",
            desktop:"#upi"
        },
        upiHeader:
        {
            pwa:"",
            desktop:"div[class='upiComponents-base-heading']"
        },
        phonepeUPI:
        {
            pwa:"//span[normalize-space()='PhonePe']",
            desktop:"//span[normalize-space()='PhonePe']"
        },
        phonepeUPIPaynow:
        {
            pwa:"",
            desktop:"//div[contains(@class,'paymentDesktop-base-left')]//div[1]//button[1]"
        },
        otherUPI:
        {
            pwa:"#upiApp-72",
            desktop:"#upiApp-72"
        },
        enterUPIID:
        {
            pwa:"//*[@class='inputWithDropdown-base-input inputWithDropdown-base-inputFull ']",
            desktop:"//*[@class='inputWithDropdown-base-input inputWithDropdown-base-inputFull ']"
        },
        enterGpayUPIID:
        {
            pwa:"//*[@class='inputWithDropdown-base-inputWithDropdown inputWithDropdown-base-input ']",
            desktop:"//*[@class='inputWithDropdown-base-inputWithDropdown inputWithDropdown-base-input ']"
        },
        otherUPIPayNow:
        {
            pwa:"",
            desktop:"div.paymentSubOption-base-rowContainer:nth-child(3) > button"
        },
        upiErrorMessage:
        {
            pwa:"div[class='inputWithDropdown-base-error']",
            desktop:"div.upiComponents-base-inputWithDropdown:nth-child(1) > div"
        },
        savedUPIInfoIcon:
        {
            pwa:"//*[@class='upiComponents-base-saveInfoIcon']",
            desktop:"//*[@class='upiComponents-base-saveInfoIcon']"
        },
        uncheckSaveUPI:
        {
            pwa:"//*[@class='upiComponents-base-checkIcon upiComponents-base-checked']",
            desktop:"//*[@class='upiComponents-base-checkIcon upiComponents-base-checked']"
        },
        selectGpayOption:
        {
            pwa:"//span[normalize-space()='Google Pay']",
            desktop:"//span[normalize-space()='Google Pay']"
        },
        gpayPayNow:
        {
            pwa:"div.paymentSubOption-base-rowContainer:nth-child(2) > button",
            desktop:"div.paymentSubOption-base-rowContainer:nth-child(2) > button"
        },
        codOption:
        {
            pwa:"#cod",
            desktop:"#cod"
        },
        CODHeaderText:
        {
            pwa:"",
            desktop:"div[class='cod-base-heading']"
        },
        captcha:
        {
            pwa:"//img[contains(@class,'captcha-base-captchaImage')]",
            desktop:"//img[contains(@class,'captcha-base-captchaImage')]"
        },
        codErrorMessage:
        {
            pwa:"div.inputV2-base-inputRow.captcha-base-inputRow:nth-child(1) > div",
            desktop:"div.inputV2-base-inputRow.captcha-base-inputRow:nth-child(1) > div"
        },
        placeOrderButton:
        {
            pwa:"#action-cod",
            desktop:"#action-cod"
        },
        codInfoText:
        {
            pwa:"div[class='codCardUI-base-helpText']",
            desktop:"div[class='codCardUI-base-helpText']"
        },
        netBankingOption:
        {
            pwa:"#netbanking",
            desktop:"#netbanking"
        },
        netbankingHeader:
        {
            pwa:"div[class='netBanking-base-heading']",
            desktop:"div[class='netBanking-base-heading']"
        },
        selectDefaultNetbankingOption:
        {
            pwa:"//div[contains(@class,'accordianComponents-base-content')]/form[@id='form-netbanking']/div/div[1]",
            desktop:"//div[contains(@class,'tabBar-base-contentBlock')]//div//div[2]"
        },
        defaultNetbankingName:
        {
            pwa:"div.paymentSubOption-base-row  span",
            desktop:"div.paymentSubOption-base-row  span"
        },
        netbankingPaynow:
        {
            pwa:"div.paymentSubOption-base-rowContainer:nth-child(2) > button",
            desktop:"div.paymentSubOption-base-rowContainer:nth-child(2) > button"
        },
        otherNetbankingOption:
        {
            pwa:"//div[contains(@class,'netBanking-base-otherBankDropDown')]/span[@class='netBanking-base-entryName']",
            desktop:"//div[contains(@class,'netBanking-base-otherBankDropDown')]"
        },
        selctFromDropdown:
        {
            pwa:"//div[contains(@class,'netBanking-base-modalBody')]//div[7]",
            desktop:"//div[contains(@class,'netBanking-base-modalBody')]//div[7]"
        },
        otherPaynow:
        {
            pwa:"",
            desktop:"//div[contains(@class,'tabBar-base-contentBlock')]/form[@id='form-netbanking']/div/button[1]"
        },
        selectedOtherNetbankingName:
        {
            pwa:"span[class='netBanking-base-entryName']",
            desktop:"div.netBanking-base-otherBankDropDown.netBanking-base-selected:nth-child(7) > span"
        },

        losSuccessRateMeassage:
        {
            pwa:"//div[@class='lowSRMessage-base-lowSRMessage netBanking-base-lowSRMessage']",
            desktop:"//div[@class='lowSRMessage-base-lowSRMessage netBanking-base-lowSRMessage']"
        },
        savedUPISection:
        {
            pwa:"//*[@class='savedInstruments-base-savedInstrumentMobileHeading']",
            desktop:"//*[@class='tabBar-base-selected tabBar-base-tab ']"
        },
        savedPaymentHeaderText:
        {
            pwa:"//*[@class='savedInstruments-base-savedInstrumentMobileHeading']",
            desktop:"//*[@class='tabBar-base-selected tabBar-base-tab ']//*[@class='paymentOptions-base-tabDisplayText']"
        },
        savedOptions:
        {
            pwa:".savedInstruments-base-savedInstrumentContainer",
            desktop:".savedInstruments-base-savedInstrumentContainer"
        },
        savedUPIText:
        {
            pwa:"//div[contains(text(),'@')]",
            desktop:"//div[contains(text(),'@')]"
        },
        selectSavedVPA:
        {
            pwa:"//*[@class='radioButton-base-container savedInstrumentCardUI-base-radioContainer ']//*[@class='radioButton-base-radioIcon savedInstrumentCardUI-base-radioIcon ']",
            desktop:"//*[@class='radioButton-base-container savedInstrumentCardUI-base-radioContainer ']//*[@class='radioButton-base-radioIcon savedInstrumentCardUI-base-radioIcon ']"
        },
        savedVPAPaynow:
        {
            pwa:"",
            desktop:"//div[contains(@class,'savedInstrumentCardUI-base-rowContainer savedInstruments-base-fullWidth')]//button[contains(@class,'')][normalize-space()='PAY NOW']"
        },
        recommendedOptions:
        {
            pwa:".recommendedInstrumentsUI-base-recommendedInstrumentContainer",
            desktop:".recommendedInstrumentsUI-base-recommendedInstrumentContainer"
        },
        recommendedUPIPaynow:
        {
            
            desktop:"//div[contains(@class,'savedInstrumentCardUI-base-rowContainer recommendedInstrumentsUI-base-fullWidth')]//button[contains(@class,'')][normalize-space()='PAY NOW']"
        },
        savedCardText:
        {
            pwa:"//div[contains(text(),'Card')]",
            desktop:"//div[contains(text(),'Card')]"
        },
        savedCreditCardPaynow:
        {
            pwa:"",
            desktop:"//div[contains(@class,'savedInstrumentCardUI-base-rowContainer savedInstruments-base-fullWidth')]//button[contains(@class,'')][normalize-space()='PAY NOW']"
        },
        recommendedSavedCardPaynow:
        {
            pwa:"",
            desktop:"//div[contains(@class,'savedInstrumentCardUI-base-rowContainer recommendedInstrumentsUI-base-fullWidth')]//button[contains(@class,'')][normalize-space()='PAY NOW']"
        },
        savedCreditCardCvv:
        {
            pwa:"//*[@class='instrumentCard-base-cvvInput ']",
            desktop:"//*[@class='instrumentCard-base-cvvInput ']"
        },
        creditCardMaskText:
        {
            pwa:"//div[contains(text(),'***')]",
            desktop:"//div[contains(text(),'***')]"

        },
        myntraCreditSelected:
        {
            pwa:"//*[@class='gcCheckbox credits-base-checkbox credits-base-checkboxSelected '] ",
            desktop:"//*[@class='gcCheckbox credits-base-checkbox credits-base-checkboxSelected '] "
        },
        myntraCreditInPriceBreakup:
        {
            pwa:"//span[contains(@class,'priceDetail-base-discount priceDetail-base-value')]/span[2]",
            desktop:"//span[contains(@class,'priceDetail-base-discount priceDetail-base-value')]/span[2]"
        },
        selectMyntraCredit:
        {
            pwa:"//*[@class='gcCheckbox credits-base-checkbox  ']",
            desktop:"//*[@class='gcCheckbox credits-base-checkbox  ']"
        },
        getMcUsedPrice:
        {
            pwa:"//span[contains(@class,'credits-base-appliedAmount')]",
            desktop:"//span[contains(@class,'credits-base-appliedAmount')]"
        },
        otpWindow:
        {
            pwa:"//*[@class='modal-base-halfCard modal-base-iosHalfCard twoFactorAuthComponents-base-modal twoFactorAuthComponents-base-mobileModal']",
            desktop:"//div[contains(@class,'modal-base-modal twoFactorAuthComponents-base-modal twoFactorAuthComponents-base-desktopModal')]"
        },
        clickMobileNumber:
        {
            pwa:"//div[@class='twoFactorAuthComponents-base-mobileNumbers']/div[1]",
            desktop:"//div[@class='twoFactorAuthComponents-base-mobileNumbers']/div[1]"
        },
        clickSendOTP:
        {
            pwa:"//*[@class='button-base-button twoFactorAuthComponents-base-sendButton  ']",
            desktop:"//*[@class='button-base-button twoFactorAuthComponents-base-sendButton  ']"
        },
        otpScreen:
        {
            pwa:"#otpField",
            desktop:"#otpField"
        },
        phoneNumberInOTPScreen:
        {
            pwa:".twoFactorAuthComponents-base-otpScannerDesc",
            desktop:".twoFactorAuthComponents-base-otpScannerDesc"
        },
        contactNumberInOTPScreen:
        {
            pwa:"//*[@class='twoFactorAuthComponents-base-mobileNumbers']/div[1]/span[2]",
            desktop:"//*[@class='twoFactorAuthComponents-base-mobileNumbers']/div[1]/span[2]"
        },
        clickSubmitInOTP:
        {
            pwa:"//*[@class='button-base-button twoFactorAuthComponents-base-sendButton ']",
            desktop:"//*[@class='button-base-button twoFactorAuthComponents-base-sendButton ']"
        },
        getErrorMessageOTP:
        {
            pwa:"//div[contains(@class,'twoFactorAuthComponents-base-error')]",
            desktop:"//div[contains(@class,'twoFactorAuthComponents-base-error')]"
        }

    }
}
}