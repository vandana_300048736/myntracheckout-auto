export default class AddressPageLocators {
    constructor(page) {

     
      this.page = page
      this.env = process.env.platform
      this.firstAaddressDetails = {
        name : "First Address",
        mobileNumber: "9620117709",
        nonServicablePincode: "682556",
        streetAddress: "#301,2nd cross, 1st main ,Mahaveer fair oaks,  Narayanappa garden",
        locality: "Whitefield",
        pincode:"560026",
        nonNCRPincode:"122001",
        nonNCRStreetAddress:"abcd"
      }
      this.secondAaddressDetails = {
        name : "Second Address",
        mobileNumber: "9620117709",
        nonServicablePincode: "682556",
        streetAddress: "#34343,67th cross, 2nd corss,Akrithi aprartment",
        locality: "Channasandra",
        pincode:"560085",
        nonNCRPincode:"122001",
        nonNCRStreetAddress:"abcd"
      }
      this.editAddressDetails = {
        name : "Edited Name",
        mobileNumber: "8147877895",
        streetAddress: "#88,4nd cross, 1st main ,Shantinikethan",
        locality: "Richmond Town",
        pincode: "560025",
      }
      this.AddressPageLocators = {
        tryAndBuySection:
        {
            pwa:".tryAndBuy-base-container",
            desktop:".tryAndBuy-base-container"
        },
        tryAndBuyFreeText:
        {
            pwa:"//span[@class='tryAndBuy-base-freeTag']",
            desktop:"//span[@class='tryAndBuy-base-freeTag']"
        },
        tryAndBuyHowItWorksButton:
        {
            pwa:"//div[contains(@class,'tryAndBuy-base-how')]",
            desktop:"//div[contains(@class,'tryAndBuy-base-how')]"
        },
        tryAndBuyImage:
        {
            pwa:".tryAndBuy-base-modalBody",
            desktop:".tryAndBuy-base-modalBody"
        },
        tryAndBuyClose:
        {
            pwa:"//div[@class='modal-base-modal tryAndBuy-base-modal']//*[local-name()='svg']",
            desktop:"//div[@class='modal-base-modal tryAndBuy-base-modal']//*[local-name()='svg']"
        },
        tryAndBuyCheckBox:
        {
            pwa:"div[class='tryAndBuy-base-container']>svg",
            desktop:"div.layout:nth-child(1) div.page div.addressDesktop-base-addressLayout div.addressDesktop-base-right div:nth-child(1) div:nth-child(1) div:nth-child(1) div.tryAndBuy-base-container > svg.tryAndBuy-base-checkboxIcon"
        },
        tryAndBuyPriceFree:
        {
            pwa:"div.priceDetail-base-row:nth-child(3) > span.priceDetail-base-value:nth-child(2) > span",
            desktop:"div.priceDetail-base-row:nth-child(3) > span.priceDetail-base-value:nth-child(2) > span"
        },
        tryAndBuyNotAllowedHeader:
        {
            pwa:".tryAndBuy-base-title",
            desktop:".tryAndBuy-base-title"
        },
        tryAndBuyNotAllowedForLesserPriceMessage:
        {
            pwa:"//span[normalize-space()='1199 are not eligible.']",
            desktop:"//span[normalize-space()='1199 are not eligible.']"
        },
        deliverySection:
        {
            pwa:"div[class='serviceability-base-list']",
            desktop:"div[class='serviceability-base-list']"
        },
        deliveryDate:
        {
            pwa:".serviceability-base-estimatedDate",
            desktop:".serviceability-base-estimatedDate"

        },
        addNewAddressButton:
        {
            pwa:"//a[text()='Add New Address']",
            desktop:"//div[contains(@class,'addressList-base-addAddressButton')]"
        },
        addEditAddressButton:
        {
            pwa:"a[class='addressBlocks-base-changeOrAddBtn']"
        },
        addAddress:
        {
            pwa:"a[class='addressBlocks-base-addBlockAnchor']"
        },
        addNewAddressHeader:
        {
            pwa:"body:nth-child(2) div.layout:nth-child(1) div.header div.site-nav-container.container > div.page-name",
            desktop:".addressFormUI-base-header"
        },
        contactNameFiled:
        {
            pwa:"#name",
            desktop:"#name"
        },
        contactMobileNumberField:
        {
            pwa:"#mobile",
            desktop:"#mobile"
        },
        pincodeField:
        {
            pwa:"#pincode",
            desktop:"#pincode"
        },
        streetAddressField:
        {
            pwa:"#streetAddress",
            desktop:"#streetAddress"
        },
        localityField:
        {
            pwa:"#locality",
            desktop:"#locality"
        },
        addAddressButton:
        {
            pwa:"div.addressFormUI-base-footer div",
            desktop:"div.addressFormUI-base-footer div"
        },
        addressBlockError:
        {
            pwa:"",
            desktop:".addressBlocks-base-error"
        },
        nonServicableHeader:
        {
            pwa:"",
            desktop:"//div[contains(@class,'notServiceableHeader-base-container addressDesktop-base-notServiceableHeader')]"
        },
        nonServicableHeaderMessage:
        {
            pwa:"div.layout:nth-child(1) div.web div.page div.addressMobile-base-mobileContainer div.addressMobile-base-mobile div.addressMobile-base-headerContainer div.notServiceableHeader-base-container > div:nth-child(1)",
            desktop:"div.layout:nth-child(1) div.page div.addressDesktop-base-addressLayout div.addressDesktop-base-left div:nth-child(1) div.notServiceableHeader-base-container.addressDesktop-base-notServiceableHeader > div:nth-child(1)"
        },
        goToBagButton:
        {
            pwa:".notServiceableHeader-base-button",
            desktop:".notServiceableHeader-base-button"
        },
        nonServicableModelError:
        {
            pwa:".itemComponents-base-serviceabilityErrorMessage",
            desktop:".itemComponents-base-serviceabilityErrorMessage"
        },
        selectNonServicableAddress:
        {
            pwa:"",
            desktop:"//span[normalize-space()='682556']"
        },
        removeAddress:
        {
            pwa:"button[data-action='remove']",
            desktop:".addressBlocks-base-remove"
        },
        confirmButton:
        {
            pwa:"//div[text()='CONFIRM']"
        },
        addressSelected:
        {
            pwa:"div.addressDetails-base-name",
            desktop:"//button[text()='Remove']//parent::div//preceding-sibling::div/preceding-sibling::div/div//div[@class='addressDetails-base-name']"
        },
        badAddressScoringError:
        {
            pwa:"div.notifyContent.notifyerror:nth-child(1) > div",
            desktop:"div.notifyContent.notifyerror:nth-child(1) > div"
        },
        totalPrice:
        {
            pwa:"//*[@class='priceDetail-base-total']/span[2]/span[2]",
            desktop:"//*[@class='priceDetail-base-total']/span[2]/span[2]"
        },
        verifyPincode:
        {
            pwa:"//*[@class='addressDetails-base-address undefined']/div[3]",
            desktop:"//button[text()='Remove']//parent::div//preceding-sibling::div/preceding-sibling::div/div/div/following-sibling::div/following-sibling::span[2]"
        },

        clickContinue:
        {
            pwa:"#placeOrderButton",
            desktop:"#placeOrderButton"
        },
        removeButton:
        {
            pwa:"//button[contains(text(),'Remove')]",
            desktop:"//button[contains(text(),'Remove')]"
        },
        listOfAddress:
        {
            pwa:"div.addressDetails-base-name",
            desktop:"div.addressDetails-base-name"
        },
        noSavedAddress:
        {
            pwa:"//div[contains(text(),'CONTACT DETAILS')]",
            desktop:"//div[contains(text(),'CONTACT DETAILS')]"
        },
        editButton:
        {
            pwa:"//a[text()='Edit']",
            desktop:"//button[text()='Edit']"
        },
        saveAddressButton:
        {
            pwa:"//div[@class='addressFormUI-base-footer']/div",
            desktop:"//div[@class='addressFormUI-base-footer']/div"
        },
        changeAddressBtn:
        {
            pwa:"//a[@class='addressBlocks-base-changeOrAddBtn']"
        },
        optionTextField:
        {
            pwa:"#optionTextField"
        },
        doneButton:
        {
            pwa:"//button[text()='done']"
        },
        proceedToPay:
        {
            pwa:"div.button-base-button  ",
            desktop:"div.button-base-button  "
        }

    }
  }
}