export default class PdpPageLocators {
    constructor(page) {
      this.page = page
      this.env = process.env.platform
      this.validPincode = "560068"
      this.invalidPincode = "000000",
      
      this.PdpPageLocators = {

        addToBagButton :{
          pwa:"div[class='padding-sm pdp-action-buttons fixed visible'] div[class='width-60'] button:nth-child(1)",
          desktop: "div.application-base main.pdp-pdp-container div.pdp-details.common-clearfix div.pdp-description-container div:nth-child(3) div.pdp-action-container.pdp-fixed > div.pdp-add-to-bag.pdp-button.pdp-flex.pdp-center",
          android:"div > div.padding-sm.pdp-action-buttons.fixed.visible > div > div.width-60 > div > button",
          ios:"div > div.padding-sm.pdp-action-buttons.fixed.visible > div > div.width-60 > div > button" 
      
        },

        sizeChartaddToBagButton :{
          desktop: "//div[contains(@class,'sizeChartWeb-sc-addToBag ')]",
          pwa:"//div[contains(@class,'padding-sm pdp-action-buttons fixed visible')]//button[contains(@class,'flex center full-width')]" 
        },

          wishlistButton :{
          pwa: '.btn.default.flat.block',
          desktop: ".pdp-add-to-wishlist.pdp-button.pdp-add-to-wishlist.pdp-button.pdp-flex.pdp-center",
          pwaXpath : "//div[contains(@class,'fixed visible')]//button[contains(text(),'WISHLIST')]",
          desktopXapth: "//span[text()='WISHLIST']"
        },
        sizeChartWishlistButton:{
          pwa : "//div[contains(@class,'fixed visible')]//button[contains(text(),'WISHLIST')]",
          desktop: '//div[contains(@class,"sizeChartWeb-sc-wishlist")]'
        },
        wishlistedButton:{
            pwa: "//button[contains(@class,'wishlist added')]",
            desktop: "//span[text()='WISHLISTED']"
        },

        goToBagButton:{
            pwa:"#header-cart-icon",
            desktop:"span[class='myntraweb-sprite desktop-iconBag sprites-headerBag']",
            android:"div > div.padding-sm.pdp-action-buttons.fixed.visible > div > div.width-60 > div > button",
            ios:"div > div.padding-sm.pdp-action-buttons.fixed.visible > div > div.width-60 > div > button"
        },
        goToBagDisplayed:
        {
          pwa:"div.pdp-action-container.pdp-fixed:nth-child(1) > a",
          desktop:"div.pdp-action-container.pdp-fixed:nth-child(1) > a"
        },
        selectSize:{
            pwa:"(//button[contains(@class,'btn') and contains(@class,'default') and contains(@class,'outline') and contains(@class,'size-btn') and not(contains(@class,'disabled'))])[1]",
            desktop:'//*[contains(@class,"size-buttons-tipAndBtnContainer")]'
        },

        selectSizeConfirm:{
                pwa:".atb-button-icon",
                desktop:".atb-button-icon"
        },
        doneButton:{
          pwa:"//button[normalize-space()='DONE']",
          desktop:"//button[contains(text(),'DONE')]"

        },
        sizeSelect:{
          pwa:"div.layout div.box div.pdp-main-container div.pdp-actions-container:nth-child(4) div.box.undefined:nth-child(1) div.popup-container div.overlay.active div.drawer.bottom.active div.popup-content.bottom div:nth-child(1) div.product-sizes-container div.sizes-list-ctn:nth-child(2) > ul.sizes-list.list-unstyled.list-inline.padding-md-left.padding-md-bottom",
         desktop:"//*[contains(@class,'size-buttons-size-button size-buttons-size-button-default') or @class='size-buttons-size-button-selected size-buttons-size-button size-buttons-size-button-default size-buttons-big-size']"
        
        },

        pincodeText: {
          pwa: ".form-control",
          desktop: ".pincode-code"
        },
        moreSellerSection:
        {
          pwa:"span[class='size-more-sellers']",
          desktop:".SelectedSizeSellerInfo-moreSellerLink"
        },
        pincodeCheckButton:{
          pwa: "div[class='padding-md delivery-options-container '] button:nth-child(1)",
          desktop: ".pincode-check.pincode-button"
        },
        serviceabilityInfo:{
          pwa: '.real-descriptor-title',
          desktop: '.pincode-serviceabilityTitle'
        },

        ratingCount: {
          pwa:'.ratingsCount',
          desktop: '.index-ratingsCount'
        },

        sizeChart:{
          desktop: '.size-buttons-show-size-chart',
          pwa: '.ripple-container.size-chart-button'
        },

       sizeChartsizeSelector:{
        pwa: '//tbody//tr[contains(@class,"sizeChart-row  ")]',
        desktop:'//tbody//tr[contains(@class,"sizeChartWeb-newRow  ")]'
       },

       sizeList:{
        pwa:".size-buttons-tipAndBtnContainer",
        desktop:'.size-buttons-tipAndBtnContainer'
       },
        loginpopup:
       {
         pwa:".popuInfoContainer"
       },
       closeLoginPopup:
       {
        pwa:"div[class='ripple-container popup-close right']"
       },
       loginHalfCardClose:{
         android: '#mainReactContent > div.loginPopupContainer > div > div > div > div > div > div.ripple-container.popup-close.right > svg'
        },
       itemPrice:{
         android:'//*[@id="info"]/div/span[1]',
         ios:'//*[@id="info"]/div/span[1]'

       }
      }
    }
}