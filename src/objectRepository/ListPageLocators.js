export default class ListPageLocators {
    constructor(page) {
      this.page = page
      this.env = process.env.platform
      this.ListPageLocators = {

        firstProduct :{
          pwa:"ul.list>li:first-of-type",
          desktop: "ul.results-base>li:first-of-type",
          android: "#page-1 > li:nth-child(1)",
          ios: "#page-1 > li:nth-child(1)"
        },

        ratingsContainer:{
            pwa:"ratings-container",
            desktop: ".product-ratingsContainer"
        },
        ratingText:{
            pwa: ".rating-text",
            desktop: ".product-ratingsContainer>span"
            
        },
        ratingCount:{
            pwa:".rating-count",
            desktop: ".product-ratingsCount"
        }


}
  

}


}