import HomePageLocators  from '../objectRepository/HomePageLocators'
let hpl = new HomePageLocators()

export default class HomePage{
    constructor(page) {
        this.page = page
        this.env = process.env.platform

    }

    async verifyAutoSuggest(page) {
        await this.page.reload(true)
        
        await this.page.click(hpl.HomePageLocators.searchBoxIcon[this.env])
        console.log(hpl.HomePageLocators.searchBoxText[this.env])
        await this.page.waitAndType(hpl.HomePageLocators.searchBoxText[this.env], hpl.searchQuery), 
        await this.page.waitFor(4000)
        
        const count = await this.page.getCount(hpl.HomePageLocators.autoSuggestSearchResults[this.env])
        console.log("no of results for search queries  ", count)
        return count
      }

    async verifyUserisLoggedIn(page){
        await this.page.click(hpl.HomePageLocators.profileHamburgerIcon[this.env])

        const isVisible =await this.page.isElementVisible(hpl.HomePageLocators.profileUserEmail[this.env])
        return isVisible

    }


    async navigateToHomePage(page){

        await this.page.goto("https://www.myntra.com")
        
    }

    async navigateT0OrdersPage(page){

        await this.page.goto("https://www.myntra.com/my/orders")
        await this.page.waitFor(4000)
    }

    async navigateToDashboardPage(page){

        await this.page.goto("https://www.myntra.com/my/dashboard")
        await this.page.waitFor(4000)
        
    }

    async logoutFromDashboardPage(page){
        
        await this.page.click(hpl.HomePageLocators.logoutDashboard[this.env])
        
    }

    

    async searchFor(page, searchQuery){
        console.log("inside abput to do a search")
       
        await this.page.click(hpl.HomePageLocators.searchBoxIcon[this.env])
        await this.page.waitAndType(hpl.HomePageLocators.searchBoxText[this.env], searchQuery)
        await this.page.keyboard.press('Enter')
        await this.page.waitFor(6000)
    
    }

    async isresultDisplayed(page){
        
        const isPageFound =  await this.page.isElementVisible(hpl.HomePageLocators.pageNotFound[this.env]);
        console.log("page not found 404 ", isPageFound)
        if(isPageFound){
            return false;
        }
        return true
    }
    async clickOnBagIcon(page)
    {
        await this.page.waitAndClick(hpl.HomePageLocators.bagIcon[this.env])

    }

    async navigateToPlp(page){      
        await this.page.goto("https://www.myntra.com/socks")     
    }

    async navigateToCart(page){
        await this.page.waitForTimeout(2000)
        await this.page.goto("https://www.myntra.com/checkout/cart")
        await this.page.waitForTimeout(2000)    
    }
    async navigateToLogin(page){
        await this.page.goto("https://www.myntra.com/login/password")
        
    }
    
    async navigateToPlp(page){      
        await this.page.goto("https://www.myntra.com/socks")     
    }

    async navigateToCart(page){
        await this.page.waitForTimeout(2000)
        await this.page.goto("https://www.myntra.com/checkout/cart")
        await this.page.waitForTimeout(2000)    
    }
    async navigateToLogin(page){
        await this.page.goto("https://www.myntra.com/login/password")
        
    }

}