import { assert } from 'chai'
import GiftCardPageLocators from '../objectRepository/GiftCardPageLocators';
import AddressPageLocators  from '../objectRepository/AddressPageLocators';

let gpl=new GiftCardPageLocators()
let apl = new AddressPageLocators()

export default class GiftCardPage 
{
    constructor(page) 
    {
      this.page = page
      this.env = process.env.platform
    }

    async purchaseGiftCard(page) 
    {
        
        const title=await this.page.title()
        const url=await this.page.url()
        console.log("User is on giftcard page, page title is: "+title+"  url is: "+url)

        const cookies = [{
            "domain": ".myntra.com",
            "name": "_mxab_",
            "value": "checkout.addressOnCart%3Denabled%3Bcheckout.payments.recommended%3Ddisabled%3Bcheckout.selective%3Denabled%3Bconfig.bucket%3Dregular%3Baddress.model.scoring%3Dsegment1%3Bweb.xceleratorTags%3Denabled%3Bpdp.web.savedAddress%3Ddisabled%3Bcheckout.couponUpsell%3Ddisabled%3Bgiftcardui.v2%3Ddisabled%3Bcheckout.cartLoginNudge%3Ddisabled"
          }];
        await this.page.setCookie(...cookies);
        const cookiesSet = await page.cookies(url);
        console.log(JSON.stringify(cookiesSet));

        await this.page.goto("https://www.myntra.com/giftcard")

        await this.page.waitForTimeout(5000)
        await this.page.click(gpl.GiftCardPageLocators.sendGiftCardButton[this.env])
        console.log("Clicked on Send GiftCard button in giftcard page")
        await this.page.waitForTimeout(2000)
        await this.page.scrollPage()
        await this.page.waitForTimeout(1000)
        await this.page.click(gpl.GiftCardPageLocators.nextButton[this.env])
        console.log("Clicked on Next button in personalize page")
        await this.page.waitForTimeout(2000)
        await this.page.click(gpl.GiftCardPageLocators.dataAmount[this.env])
        console.log("Selected 1000 Rs")
        await this.page.waitAndType(gpl.GiftCardPageLocators.mobileNumberField[this.env],gpl.giftcardDetails.mobileNumber)
        await this.page.waitAndType(gpl.GiftCardPageLocators.emailField[this.env],gpl.giftcardDetails.email)
        await this.page.waitAndType(gpl.GiftCardPageLocators.recipientNameField[this.env],gpl.giftcardDetails.name)
        await this.page.scrollPage()
        await this.page.waitForTimeout(10000)
        await this.page.click(gpl.GiftCardPageLocators.showPriviewButton[this.env])
        console.log("Clicked on Privew Button on priview page")
        await this.page.scrollPage()
        await this.page.waitForTimeout(5000)
        await this.page.click(gpl.GiftCardPageLocators.payButton[this.env])
        console.log("Clicked on Pay button")
        await this.page.waitForTimeout(2000)
        if (await page.$(apl.AddressPageLocators.contactNameFiled[this.env]) !== null) 
        {
            await this.addAddress(page)
        }
    }

    async purchaseGiftCardForNewUser(page) 
    {
        
        const title=await this.page.title()
        const url=await this.page.url()
        console.log("User is on giftcard page, page title is: "+title+"  url is: "+url)

        const cookies = [{
            "domain": ".myntra.com",
            "name": "_mxab_",
            "value": "checkout.addressOnCart%3Denabled%3Bcheckout.payments.recommended%3Ddisabled%3Bcheckout.selective%3Denabled%3Bconfig.bucket%3Dregular%3Baddress.model.scoring%3Dsegment1%3Bweb.xceleratorTags%3Denabled%3Bpdp.web.savedAddress%3Ddisabled%3Bcheckout.couponUpsell%3Ddisabled%3Bgiftcardui.v2%3Ddisabled%3Bcheckout.cartLoginNudge%3Ddisabled"
          }];
        await this.page.setCookie(...cookies);
        const cookiesSet = await page.cookies(url);
        console.log(JSON.stringify(cookiesSet));

        await this.page.goto("https://www.myntra.com/giftcard")

        await this.page.waitForTimeout(5000)
        await this.page.click(gpl.GiftCardPageLocators.sendGiftCardButton[this.env])
        console.log("Clicked on Send GiftCard button in giftcard page")
        await this.page.waitForTimeout(2000)
        await this.page.scrollPage()
        await this.page.waitForTimeout(1000)
        await this.page.click(gpl.GiftCardPageLocators.nextButton[this.env])
        console.log("Clicked on Next button in personalize page")
        await this.page.waitForTimeout(2000)
        await this.page.click(gpl.GiftCardPageLocators.dataAmount[this.env])
        console.log("Selected 1000 Rs")
        await this.page.waitAndType(gpl.GiftCardPageLocators.mobileNumberField[this.env],gpl.giftcardDetails.mobileNumber)
        await this.page.waitAndType(gpl.GiftCardPageLocators.emailField[this.env],gpl.giftcardDetails.email)
        await this.page.waitAndType(gpl.GiftCardPageLocators.recipientNameField[this.env],gpl.giftcardDetails.name)
        await this.page.scrollPage()
        await this.page.waitForTimeout(10000)
        await this.page.click(gpl.GiftCardPageLocators.showPriviewButton[this.env])
        console.log("Clicked on Privew Button on priview page")
        await this.page.scrollPage()
        await this.page.waitForTimeout(5000)
        await this.page.click(gpl.GiftCardPageLocators.payButton[this.env])
        console.log("Clicked on Pay button")
        await this.page.waitForTimeout(2000)
        if (await page.$(apl.AddressPageLocators.contactNameFiled[this.env]) !== null) 
        {
            await this.addAddress(page)
        }
        await this.page.waitForTimeout(5000)
    }

    async purchaseGiftCardWithNewGiftcardUiChanges(page) 
    {
        
        const title=await this.page.title()
        const url=await this.page.url()
        console.log("User is on giftcard page, page title is: "+title+"  url is: "+url)

        const cookies = [{
            "domain": ".myntra.com",
            "name": "_mxab_",
            "value": "checkout.addressOnCart%3Denabled%3Bcheckout.payments.recommended%3Ddisabled%3Bcheckout.selective%3Denabled%3Bconfig.bucket%3Dregular%3Baddress.model.scoring%3Dsegment1%3Bweb.xceleratorTags%3Denabled%3Bpdp.web.savedAddress%3Ddisabled%3Bcheckout.couponUpsell%3Ddisabled%3Bgiftcardui.v2%3Denabled%3Bcheckout.cartLoginNudge%3Ddisabled"
          }];
        await this.page.setCookie(...cookies);
        const cookiesSet = await page.cookies(url);
        console.log(JSON.stringify(cookiesSet));

        await this.page.goto("https://www.myntra.com/giftcard")

        await this.page.waitForTimeout(5000)
        await this.page.click(gpl.GiftCardPageLocators.sendGiftCardButton[this.env])
        console.log("Clicked on Send GiftCard button in giftcard page")
        await this.page.waitForTimeout(2000)
        if(this.env==="desktop")
        {
            await this.page.scrollPage()
        }
        await this.page.waitForTimeout(1000)
        if(this.env==="pwa")
        {
            await this.page.waitAndType(gpl.GiftCardPageLocators.gcAmountFiled[this.env],gpl.giftcardDetails.amount)
            console.log("Selected 1000 Rs")
            await this.page.waitForTimeout(1000)
            await this.page.click(gpl.GiftCardPageLocators.closePreviewButtonAndPayButton[this.env])
            console.log("Clicked on Next button in personalize page")
        }
        else
        {
            await this.page.click(gpl.GiftCardPageLocators.nextButton[this.env])
            console.log("Clicked on Next button in personalize page")
        }
        await this.page.waitForTimeout(2000)
        if(this.env==="desktop")
        {
            await this.page.click(gpl.GiftCardPageLocators.dataAmount[this.env])
            console.log("Selected 1000 Rs")
        }
        await this.page.waitAndType(gpl.GiftCardPageLocators.mobileNumberField[this.env],gpl.giftcardDetails.mobileNumber)
        await this.page.waitAndType(gpl.GiftCardPageLocators.emailField[this.env],gpl.giftcardDetails.email)

        if(this.env==="pwa")
        {
            await this.page.waitAndType(gpl.GiftCardPageLocators.recipientNameFieldForNewGiftcardUI[this.env],gpl.giftcardDetails.name)
            await this.page.waitForTimeout(1000)
            await this.page.click(gpl.GiftCardPageLocators.previewLink[this.env])
            console.log("Clicked on Privew link")
            await this.page.waitForTimeout(1000)
            await this.page.click(gpl.GiftCardPageLocators.closePreviewButtonAndPayButton[this.env])
            console.log("Clicked on Close Privew Button")
            await this.page.waitForTimeout(1000)
        }
        else
        {
            await this.page.waitAndType(gpl.GiftCardPageLocators.recipientNameField[this.env],gpl.giftcardDetails.name)
        }
        await this.page.scrollPage()
        await this.page.waitForTimeout(2000)
        if(this.env==="desktop")
        {
            await this.page.click(gpl.GiftCardPageLocators.showPriviewButton[this.env])
            console.log("Clicked on Privew Button on priview page")
        }
        await this.page.scrollPage()
        await this.page.waitForTimeout(5000)
        if(this.env==="desktop")
        {
            await this.page.click(gpl.GiftCardPageLocators.payButton[this.env])
            console.log("Clicked on Pay button")
        }else
        {
            await this.page.click(gpl.GiftCardPageLocators.closePreviewButtonAndPayButton[this.env])
            console.log("Clicked on Pay Button")
        }
        
        await this.page.waitForTimeout(2000)
        if (await page.$(apl.AddressPageLocators.contactNameFiled[this.env]) !== null) 
        {
            await this.addAddress(page)
        }
        await this.page.waitForTimeout(8000)
    }

    async addAddress(page)
    {
        await this.page.waitAndType(apl.AddressPageLocators.contactMobileNumberField[this.env],apl.firstAaddressDetails.mobileNumber)
        await this.page.waitAndType(apl.AddressPageLocators.pincodeField[this.env],apl.firstAaddressDetails.pincode)
        await this.page.waitAndType(apl.AddressPageLocators.streetAddressField[this.env],apl.firstAaddressDetails.streetAddress)
        await this.page.scrollPage()

        if(this.env==="pwa")
        {
            await this.page.click(apl.AddressPageLocators.localityField[this.env])
            await this.page.waitAndType(apl.AddressPageLocators.optionTextField[this.env],apl.firstAaddressDetails.locality)
            await this.page.waitForXPathAndClick(apl.AddressPageLocators.doneButton[this.env])
        }
        else
        {
            await this.page.waitAndType(apl.AddressPageLocators.localityField[this.env],apl.firstAaddressDetails.locality)
        }
        await this.page.waitForTimeout(3000)
        await this.page.click(apl.AddressPageLocators.proceedToPay[this.env])
        await this.page.waitForTimeout(5000)
    }

}