import ListPageLocators  from '../objectRepository/ListPageLocators'
let lpl = new ListPageLocators()

export default class ListPage{

    constructor(page) {
        this.page = page
        this.env = process.env.platform
   
}

async getRatingCount(page){
    await this.page.waitFor(2000)
    console.log("getting rating count of first product")

        const element = await this.page.$(lpl.ListPageLocators.ratingCount[this.env]);
        var ratingCount = await (await element.getProperty('textContent')).jsonValue()
        ratingCount = ratingCount.replace( /^\D+/g, '');
        console.log("totol rating  count--->", parseInt(ratingCount))
        return parseInt(ratingCount)
        
    }
    
    async getRating(page){
        await this.page.waitFor(2000)
        console.log("getting rating  of first product")
    
            const element = await this.page.$(lpl.ListPageLocators.ratingText[this.env]);
            var rating = await (await element.getProperty('textContent')).jsonValue()
            console.log("rating --->", parseFloat(rating))
            return parseFloat(rating)
            
        }

    async clickFirstProduct(page){
        await this.page.waitForTimeout(2000)
        await this.page.click(lpl.ListPageLocators.firstProduct[this.env])
        await this.page.waitForTimeout(8000)
   }
      
}