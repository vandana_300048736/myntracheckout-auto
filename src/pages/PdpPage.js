import PdpPageLocators  from '../objectRepository/PdpPageLocators'
let ppl = new PdpPageLocators()

export default class PdpPage {
    constructor(page) {
      this.page = page
      this.env = process.env.platform
    }

    async wishlistItem(page) {

         await this.page.waitForSelector(ppl.PdpPageLocators.wishlistButton[this.env])
         await this.page.click(ppl.PdpPageLocators.wishlistButton[this.env])
         await this.page.waitFor(4000)
         
    }

    async isItemWishlisted(page){
         
         const isItemWishlisted = await this.page.isXPathVisible(ppl.PdpPageLocators.wishlistedButton[this.env])
        
         console.log("is wishlist button state changed  ", isItemWishlisted)
         return isItemWishlisted
    }

    async addProductFromPDPToCart(page) {

        //await this.page.waitFor(2000)
        if(this.env==="pwa"){
            console.log("inside pwa button")
            await this.page.waitForXPathAndClick(ppl.PdpPageLocators.addToBagButton[this.env])
            await this.page.waitFor(3000)
            await this.page.waitForXPathAndClick(ppl.PdpPageLocators.selectSize[this.env])
            await this.page.click(ppl.PdpPageLocators.selectSizeConfirm[this.env])
        }
        else if(this.env==="desktop"){
            await this.page.click(ppl.PdpPageLocators.selectSize[this.env])
            await this.page.click(ppl.PdpPageLocators.addToBagButton[this.env])
        }
        else if(this.env==="android"){
          console.log("Entered loginHalfCardVisible++++++++++++++++++++")
          await this.page.click(ppl.PdpPageLocators.loginHalfCardClose[this.env])
          await this.page.waitForTimeout(5000)
          await this.page.click(ppl.PdpPageLocators.addToBagButton[this.env])      
       }
       
        await this.page.waitFor(2000)

   }



   async isItemAddedToCart(page){
    const isItemAddedToCart = await this.page.isElementVisible(ppl.PdpPageLocators.goToBagDisplayed[this.env])
    console.log("is ADD TO BAG button state changed  ", isItemAddedToCart)
    return isItemAddedToCart
   }

   async getRatingCountPdp(page){
    console.log("getting rating count from pdp")
    await this.page.waitFor(40000)
    console.log("getting rating count from pdp")
    const element1 = await this.page.$(ppl.PdpPageLocators.ratingCount[this.env]);
    var ratingCount1 = await (await element1.getProperty('textContent')).jsonValue()
    ratingCount1 = ratingCount1.replace( /^\D+/g, '');
    console.log("total rating  pdp count--->", parseInt(ratingCount1))
    return parseInt(ratingCount1)

   }
   async enterPincode(page, pincode){
    await this.page.scrollPage()
    await this.page.waitAndType(ppl.PdpPageLocators.pincodeText[this.env], pincode)
    // await this.page.waitFor(10000)
   }


   async selectSize(page)
   {
    await this.page.waitFor(3000)
  
 
    if(this.env==="pwa")
    {
      //await this.page.scrollPage()
    //await this.page.click(ppl.PdpPageLocators.selectSizeConfirm[this.env])
   // await this.page.click(ppl.PdpPageLocators.addToBagButton[this.env])

    //console.log("Clicked Add to bag")
    await this.page.waitForTimeout(3000)
    if(await this.page.isElementVisible(ppl.PdpPageLocators.loginpopup[this.env]))
    {
      console.log("Login pop up displayed")
      await this.page.click(ppl.PdpPageLocators.closeLoginPopup[this.env])
      await this.page.waitForTimeout(3000)

    }
    else{
      console.log("Login pop up is not displayed")
    }
    await this.page.click(ppl.PdpPageLocators.addToBagButton[this.env])
    console.log("Clicked Add to bag")
    if(await this.page.isElementVisible(ppl.PdpPageLocators.sizeChart[this.env]))
    {
      console.log("***** Select the size******")
      await this.page.waitForTimeout(2000)
      await this.page.waitForXPathAndClick(ppl.PdpPageLocators.selectSize[this.env])
      console.log("***** clicked on size*****")
      await this.page.click(ppl.PdpPageLocators.addToBagButton[this.env])
      await this.page.waitForTimeout(2000)
    }
   
    //if( await this.page.isElementVisible(ppl.PdpPageLocators.doneButton[this.env]))
    //{
      //await this.page.waitFor(2000)

      //const result = await page.evaluate(() => {
        //let data = []; // Create an empty array


        //let elements = document.querySelector('div.layout div.box div.pdp-main-container div.pdp-actions-container:nth-child(4) div.box.undefined:nth-child(1) div.popup-container div.overlay.active div.drawer.bottom.active div.popup-content.bottom div:nth-child(1) div.product-sizes-container div.sizes-list-ctn:nth-child(2) > ul.sizes-list.list-unstyled.list-inline.padding-md-left.padding-md-bottom li'); // Select all 

        //console.log("***** el"+elements)
        // Loop through each proudct
          // Select the title
          // Select the price
         // for(var element of elements){
            //let tt=element.querySelector('div.layout div.box div.pdp-main-container div.pdp-actions-container:nth-child(4) div.box.undefined:nth-child(1) div.popup-container div.overlay.active div.drawer.bottom.active div.popup-content.bottom div.product-sizes-container div.sizes-list-ctn:nth-child(2) ul.sizes-list.list-unstyled.list-inline.padding-md-left.padding-md-bottom li:nth-child(1) button.btn.default.outline.size-btn.big:nth-child(1) > span:nth-child(1)')
              
              
         // }
          //data.push({s}); // Push an object with the data onto our array
          //console.log("***** data is "+data)
          //return data;
          
        //});

         // Return our data array
   
 //console.log("****   element is "+elements)
 
    //const a= await this.page.getCount(ppl.PdpPageLocators.sizeSelect[this.env])
     // console.log ("***** a"+a)
      //console.log("*****Size list"+ppl.PdpPageLocators.sizeSelect[this.env])
     // await this.page.click(ppl.PdpPageLocators.sizeSelect[this.env])
      //await this.page.waitFor(2000)
      //await this.page.click(ppl.PdpPageLocators.doneButton)
      //await this.page.waitFor(2000)
    //}
   //}
    }
   else{
     console.log("**** desktop run")

    
     await this.page.click(ppl.PdpPageLocators.addToBagButton[this.env])
     if(await this.page.isElementVisible(ppl.PdpPageLocators.sizeChart[this.env]))
     {
       console.log("***** Select the size******")
       await this.page.waitForTimeout(2000)
       await this.page.waitForXPathAndClick(ppl.PdpPageLocators.selectSize[this.env])
       console.log("***** clicked on size*****")
       await this.page.click(ppl.PdpPageLocators.addToBagButton[this.env])
       await this.page.waitForTimeout(2000)
     }
     

   }
   
  }
  async addProductPdpToCart(page)
  {
    if(this.env==="pwa")
    {
      await this.page.waitFor(2000)
      console.log("Wait for click")
      await this.page.click(ppl.PdpPageLocators.goToBagButton[this.env])
      await this.page.waitFor(2000)
    }
    else if(this.env==="desktop"){
      await this.page.waitFor(4000)
      console.log("Wait for click")
     await this.page.click(ppl.PdpPageLocators.goToBagButton[this.env])
      
      await this.page.waitFor(5000)

      console.log("Naviagte from pdp to cart")
    }
    else if(this.env==="android")
    {
      await this.page.waitFor(2000)
      console.log("Wait for click")
      await this.page.click(ppl.PdpPageLocators.goToBagButton[this.env])
      await this.page.waitFor(2000)
      console.log("Naviagte from pdp to cart")
    }
    else if(this.env==="ios")
    {
      await this.page.waitFor(2000)
      console.log("Wait for click")
      await this.page.click(ppl.PdpPageLocators.goToBagButton[this.env])
      await this.page.waitFor(2000)
      console.log("Naviagte from pdp to cart")
    }
  }

   async checkPincodeServiceability(page){
    
      await this.enterPincode(page, ppl.validPincode)
      await this.page.click(ppl.PdpPageLocators.pincodeCheckButton[this.env])
      await this.page.waitFor(2000)

   }

   async clickSizeChartLink(){
       await this.page.waitForTimeout(3000)
       await this.page.click(ppl.PdpPageLocators.sizeChart[this.env])
       await this.page.waitForTimeout(3000)
      
   }

   async wishlistFromSizeChart(){
    await this.page.waitForXPathAndClick(ppl.PdpPageLocators.sizeChartWishlistButton[this.env])

   }

   async addToBagFromSizeChart(){

    await this.page.waitForXPathAndClick(ppl.PdpPageLocators.sizeChartsizeSelector[this.env])
     await this.page.waitForXPathAndClick(ppl.PdpPageLocators.sizeChartaddToBagButton[this.env])
     await this.page.waitForTimeout(5000)
    
     
   }
   async isMoreSellerDisplayed(page)
   {
    const isMoreSellerdisplayed= await this.page.isElementVisible(ppl.PdpPageLocators.moreSellerSection[this.env])
    return isMoreSellerdisplayed

   }
   

   async isPincodeServiceable(page){
    const isServicable = false;

    const totalCount = await this.page.getCount(ppl.PdpPageLocators.serviceabilityInfo[this.env])
    console.log("service pincode ", totalCount)
    if(totalCount>1){
      const pincodeInfo = await page.$$(ppl.PdpPageLocators.serviceabilityInfo[this.env]);
      for(const info of pincodeInfo){
      const serviceabilityInfo = await page.evaluate(el => el.innerText, info)
      console.log(serviceabilityInfo) 
      if(serviceabilityInfo.includes('Get it by')){
        return true;
      }
        
     }

    }

    else{
      return isServicable;
    }
    
      
   }

   async addSingleSizeItemFromPDPToCart(page) {
    await this.page.waitFor(2000)
    if(this.env==="pwa"){
        console.log("inside pwa button")
        await this.page.waitForXPathAndClick(ppl.PdpPageLocators.addToBagButton[this.env])
        await this.page.waitFor(3000)
      }
    else if(this.env==="desktop"){
              await this.page.click(ppl.PdpPageLocators.addToBagButton[this.env])
    }
    else if(this.env==="android"){
      await this.page.waitForTimeout(2000)
      //console.log("Entered loginHalfCardVisible++++++++++++++++++++")
      //await this.page.click(ppl.PdpPageLocators.loginHalfCardClose[this.env])
      await this.page.click(ppl.PdpPageLocators.addToBagButton[this.env])      
   }
   else if(this.env==="ios"){
    await this.page.waitForTimeout(2000)
    await this.page.click(ppl.PdpPageLocators.addToBagButton[this.env])      
 }
   
    //await this.page.waitFor(2000)

}

  async getTextPicePDP(page){
    const itemPriceInPDP = await this.page.getTextXpath(ppl.PdpPageLocators.itemPrice[this.env])
    return itemPriceInPDP
    }

  }

