import WishlistPageLocators  from '../objectRepository/WishlistPageLocators'
let wpl = new WishlistPageLocators()

import { expect, assert } from 'chai'

export default class WishlistPage {
    constructor(page) {
      this.page = page
      this.env = process.env.platform
       
    }

    async isUserLoggedOut(page){

        return this.page.isElementVisible(wpl.wishlistLocators.loginButtonWishlist[this.env])

    }

    async clearWishlist(page){
    
        await this.page.goto("https://www.myntra.com/wishlist")
        await this.page.waitFor(2000)
        console.log("checking if wishlist is empty")
        const isEmpty = await this.isWishlistEmpty(page)
        console.log(!isEmpty)
        if(!isEmpty){
            console.log("clearing wishlist items")
            const elements = await this.page.$x(wpl.wishlistLocators.removeIconLength[this.env])
            let i=0;
            
            while(i<elements.length){
                await this.page.click(wpl.wishlistLocators.removeIcon[this.env])
                await this.page.waitFor(2000)
                i++;
            }  
        }
        

    }

    async isWishlistEmpty(page){
         console.log("calling wishlst empty funnction")
        if(await this.page.isXPathVisible(wpl.wishlistLocators.emptyWishlist[this.env])){
            return true
        }

            
        else {
            console.log('wishlist is not empty found');
            return false
        
           
    }
}
async navigateToCartPage(page)
{
    await this.page.click(wpl.wishlistLocators.bagIcon[this.env])
    await this.page.waitForTimeout(2000)
}

async navigateToWishlist(page){
    await this.page.waitFor(2000)
    await this.page.goto("https://www.myntra.com/wishlist")
  
}

async moveItemfromWishlist(page){
   
    await this.page.waitForSelector(wpl.wishlistLocators.moveToBag[this.env])
    await this.page.click(wpl.wishlistLocators.moveToBag[this.env])
    await this.page.waitForSelector(wpl.wishlistLocators.selectSize[this.env])
    await this.page.waitForTimeout(2000)
    await this.page.click(wpl.wishlistLocators.selectSize[this.env])
    await this.page.waitForSelector(wpl.wishlistLocators.SelectSizeDone[this.env])
    await this.page.click(wpl.wishlistLocators.SelectSizeDone[this.env])
    
}

async getCartCount(page){
    console.log("getCartCount getting called")
    await this.page.waitForTimeout(3000)
    const isPresnn =await this.page.isXPathVisible(wpl.wishlistLocators.cartCount[this.env])
    if(isPresnn){
        await this.page.waitForTimeout(3000)
        const countText = await this.page.getText(wpl.wishlistLocators.bagCount[this.env])
        
        return parseInt(countText)
    }
    else{
          return 0;
    }
    
    
    
}

async getWishlistCount(page){
    let wishCounts = "";
    console.log("getWishlistCount getting called")
    const isItemPresent =await this.page.isElementVisible(wpl.wishlistLocators.wishlistCount[this.env])
    console.log(isItemPresent)
    if(isItemPresent){
        await this.page.waitForTimeout(5000)
        const element = await this.page.$(wpl.wishlistLocators.wishlistCount[this.env]);
       
        wishCounts = await (await element.getProperty('textContent')).jsonValue()
        console.log("wishlist count is ", wishCounts)
        console.log("wishist items--->", parseInt(wishCounts))
        return parseInt(wishCounts)
    }
    else
    {
         return 0;
    }
    
    
    
}

}