import { assert } from 'chai'
import CartPageLocators  from '../objectRepository/CartPageLocators'
import ppl from '../objectRepository/PdpPageLocators'
import PdpPage from './PdpPage'

let cpl = new CartPageLocators()

export default class CartPage {
    constructor(page) {
      this.page = page
      this.env = process.env.platform
    }

    //using this method to set cookie on cart page for selective checkout AB, will remove this once its 100 perecnt live
    async setCookies(page){
        
      const cookies = [{
        'name': '_mxab_',
        'value': 'checkout.selective%3Denabled%3Bcheckout.cartmerge%3Denabled'
      }]
      await this.page.setCookie(...cookies);
    }
    
    async clearCart(page) {
      
        //await this.page.goto("https://www.myntra.com/checkout/cart")
        await this.page.waitFor(2000)
        const isEmpty = await this.isCartEmpty(page)
        console.log(!isEmpty)
        if(!isEmpty){
            console.log("clearing items from cart inside if loop")
            const elements = await this.page.$x(cpl.CartPageLocators.removeItems[this.env])
        
            console.log("***** the length is "+elements.length)
            let i=0;
            
            while(elements.length>0){
                console.log("inside while loop")
                await this.page.click(cpl.CartPageLocators.removeButton[this.env])
                await this.page.waitFor(3000)
                await this.page.waitForXPathAndClick(cpl.CartPageLocators.removeFromDialog[this.env])
                await this.page.waitFor(3000)
                elements.length= elements.length-1
                console.log("***** the length is after clear "+elements.length)
                console.log("****** Iteration******")
                i++;
            }
        }
        // else{
        //     console.log("cart is cleared")
        // }
    
    }

    async isCartEmpty(page){
        console.log("empty cart checking.....................")
        if (await page.$(cpl.CartPageLocators.emptyCart[this.env]) !== null) {
            console.log("cart is empty")
            return true
        }
            
        else {
            console.log('cart is not empty');
            return false
    }
  }
  async editQty(Page)
  {
  
    await this.page.click(cpl.CartPageLocators.edityQty[this.env])
    await this.page.waitFor(5000)
    await this.page.waitForXPathAndClick(cpl.CartPageLocators.qtySelect[this.env])
    await this.page.waitFor(5000)
    if(this.env==="pwa")
    {
      await this.page.click(cpl.CartPageLocators.qtySelectDone[this.env])
      await this.page.waitFor(5000)
      console.log("**** Clicked edit qty ***")
    }
   else{
     console.log("**** Clicked edit qty ***")
   }

  }
async addGiftWrap(page,giftwrapMessage)
{
  if (this.env==="pwa")
  {
    await this.page.scrollPage()
  }
  await this.page.click(cpl.CartPageLocators.clickGiftWrap[this.env])
  await this.page.waitFor(5000)
  await this.page.waitForXpathAndType(cpl.CartPageLocators.enterRecepientName[this.env],cpl.giftwrapMessage)
 await this.page.waitForXpathAndType(cpl.CartPageLocators.enterMessage[this.env],cpl.giftwrapMessage)
  await this.page.waitForXpathAndType(cpl.CartPageLocators.enterSenderName[this.env],cpl.giftwrapMessage)
  await this.page.waitForXPathAndClick(cpl.CartPageLocators.enterApplyGiftWrap[this.env])
  //await this.page.waitForXPathAndClick(cpl.CartPageLocators.closeGiftwrap[this.env])
console.log("**** Clicked on Giftwrap ****")
await this.page.waitFor(5000)

}
async enterInvalidCouponCode(page,invalidCouponCode)
{
  await this.page.waitForXpathAndType(cpl.CartPageLocators.enterInvalidCoupon[this.env],cpl.invalidCouponCode)

}
async clickOnCheckInCouponWindow(page)
{
  await this.page.waitForTimeout(2000)
 await this.page.waitForXPathAndClick(cpl.CartPageLocators.clickCheckInCouponWindow[this.env])
  

}
async verifyCouponErrorMessage(page)
{
  var couponError= await this.page.getText(cpl.CartPageLocators.couponErrorMessage[this.env])
  await this.page.waitForTimeout(2000)
  console.log("Error message is :"+couponError)
  var isDisplayed=couponError.includes("not")
  return isDisplayed

}
async clickOnApplyCouponInCouponwindow(page)
{
  await this.page.waitAndClick(cpl.CartPageLocators.clickApplyCouponInCouponWindow[this.env])
}
async removeAppliedGiftwrap(page)
{
  await this.page.waitFor(5000)
 await this.page.waitAndClick(cpl.CartPageLocators.removeAppliedGiftwrap[this.env])
}
async removeAllItemsFromCart(page)
{
  
  //await this.page.isElementVisible(cpl.CartPageLocators.selectItemCheckbox[this.env])
  //await this.page.click(cpl.CartPageLocators.selectItemCheckbox[this.env])
  await this.page.waitForXPathAndClick(cpl.CartPageLocators.removeBulkItem[this.env])
  await this.page.waitFor(3000)
  await this.page.click(cpl.CartPageLocators.removeBulkItemConfirm[this.env])
  await this.page.waitFor(3000)
}
async clickOnMyntraLogo(page)
{
  await this.page.waitFor(2000)
  await this.page.waitForXPathAndClick(cpl.CartPageLocators.clickMyntraLogo[this.env])
  await this.page.waitFor(2000)
}
async verifyCartfillerSection(page)
{
  const isCartFillerVisible=await this.page.isElementVisible(cpl.CartPageLocators.cartfillerSection[this.env])
  console.log("***** Cart filler Section is displayed  ", isCartFillerVisible)
  return isCartFillerVisible
}
async addItemFromCartfiller(page)
{
 
    await this.page.waitForXPathAndClick(cpl.CartPageLocators.cartfillerAddtoBagButton[this.env])
    await this.page.waitForTimeout(4000)
 
}
async verifyFreeDeliveryHeader(page)
{
  const freeDeliveryHeader=await this.page.getTextXpath(cpl.CartPageLocators.freeDeliveryText[this.env])
  var isDisplayed=freeDeliveryHeader.includes("No")
  return isDisplayed

}
async verifyFreeDeliveryInPriceBlockSection(page)
{
  const freeDelivery=await this.page.getText(cpl.CartPageLocators.freeDeliveryInPriceBlock[this.env])
  var freeIsDisplayed=freeDelivery.includes("FREE")
  return freeIsDisplayed
}
async clickOnSize(page)
{
  await this.page.waitForTimeout(3000)
  await this.page.click(cpl.CartPageLocators.editSizeButton[this.env])
}
async getSellerName(page)
{
 
  const sellerName =await this.page.getText(cpl.CartPageLocators.sellerName[this.env])
  var trimName=sellerName.split(":")
  var seller=trimName[1]
  console.log("**** Seller name is *****"+seller)
  return seller
}
async getSellerNameinEditSizeWindow(page)
{
  await this.page.waitForTimeout(3000)
  const seller=await this.page.getText(cpl.CartPageLocators.seller[this.env])
  console.log("**** Seller name displayed in  edit size window : *****"+seller)
  return seller
}
async getItemPriceInEditSizeWindow(page)
{
  const itemPriceInEditSizeWindow=await this.page.getTextXpath(cpl.CartPageLocators.itemPriceInEditSizeWindow[this.env])
  console.log("*** Item price in edit size window ****"+itemPriceInEditSizeWindow)
  return  itemPriceInEditSizeWindow
}
async getItemPrice(page)
{
  const itemPrice=await this.page.getTextXpath(cpl.CartPageLocators.itemPrice[this.env])
  console.log("**** Item price is :"+itemPrice)
  return itemPrice
}
async clickSwitchSeller(page)
{
  await this.page.click(cpl.CartPageLocators.switchSeller[this.env])
  console.log("clicked switch seller")
  await this.page.waitFor(3000)
}
async isSellerSelected(page)
{
  
 const isDisplayed= await this.page.isElementVisible(cpl.CartPageLocators.sellerSelectedRadioButton[this.env])
  return isDisplayed
}
async clickOnSizeInPwa(page)
{
  if(this.env==="pwa")
  {
    await this.page.waitForTimeout(2000)
    await this.page.click(cpl.CartPageLocators.editSizeButton[this.env])
    console.log("Clicked edit size")

  }
  else
  {
    console.log("No change for desktop")
  }
}
async switchSeller(page)
{
  await this.page.waitForTimeout(2000)
  await this.page.click(cpl.CartPageLocators.sellerUnselectedRadioButton[this.env])
  

}
async verfiyOfferSectionDisplayed(page)
{
 const displayed= await this.page.isXPathVisible(cpl.CartPageLocators.offerSection[this.env])
 return displayed
}
async verifyOfferSectionHeader(page)
{
  var offer=await this.page.getText(cpl.CartPageLocators.offerSectionHeader[this.env])
  console.log("Offer Section header is:"+offer)
  var isDisplayed=offer.includes("Offers")
  return isDisplayed
}
async clickOnShowMoreInOfferSection(page)
{
  await this.page.click(cpl.CartPageLocators.showMoreButton[this.env])
}
async clickDoneOnSelectSellerWindow(page)
{
  if(this.env==="pwa")
  {
    await this.page.waitForTimeout(2000)
    
  }
  else{
    await this.page.waitForXPathAndClick(cpl.CartPageLocators.selectSellerDoneButton[this.env])
    await this.page.waitForTimeout(5000)
  }
  
}
async clickDoneInPwa(page)
{
  if(this.env==="pwa")
  {
    await this.page.waitAndClick(cpl.CartPageLocators.doneInEditsize[this.env])
    await this.page.waitForTimeout(2000)
  }
  else{
    console.log("not for desktop")
  }
 

}
async  getTotalCartItem(page)
{
  await this.page.waitForTimeout(6000)
  if(this.env==="pwa")
  {
    await this.page.scrollPage()
  }
 const price= await this.page.getText(cpl.CartPageLocators.totalCartItem[this.env]) 

 console.log("****** price details****"+pricedetails)
  var pricedetails=price.split("(")
  var pricesplit=pricedetails[1]
 //String comboDiscountAmount = utils.getText(getLocator("comboDiscountAmt")).split("Rs. ")[1].replace(",", "");
  console.log("******** Total Cart Count **********"+parseInt(pricesplit))
  return parseInt(pricesplit)
}
async verifyLoginButtonforNonLoggedInUserOnCouponSection(page)
{
  await this.page.waitForTimeout(3000)
  const login_Coupon=await this.page.isElementVisible(cpl.CartPageLocators.couponLoginButton[this.env])
  return login_Coupon

}
async clickCouponLogin(page)
{

  await this.page.click(cpl.CartPageLocators.couponLoginButton[this.env])
  await this.page.waitForTimeout(3000)

}
async verifyAddressOnCartSection(page)
{

  const addressOnCart=await this.page.isElementVisible(cpl.CartPageLocators.addressOnCartSection[this.env])
  await this.page.waitForTimeout(3000)
  return addressOnCart
}

async verifyAddressOnCartHeaderForNonLoggedInUser(page)
{

  var header=await this.page.getText(cpl.CartPageLocators.addressOnCartHeader[this.env])
  var headerDisplayed=header.includes("Check")
  return headerDisplayed
 

}


async clickOnAddressonCartHeader(page)
{
  await this.page.click(cpl.CartPageLocators.addressOnCartHeader[this.env])
}
async verifyEnterPincodeForNonLoggedInUser(page)
{
  var pincode=await this.page.getText(cpl.CartPageLocators.enterPincode[this.env])
  var pincodeTextDisplayed=pincode.includes("Enter")
  console.log("****** Enter Pincode text is displayed")
  return pincodeTextDisplayed
 
}
async verifyEnterPincodeSectionForNonLoggedInUser(page)
{
  const isEnterPincodeSectionDisplayed=await this.page.isElementVisible(cpl.CartPageLocators.enterPincodeSection[this.env])
  return isEnterPincodeSectionDisplayed

}
async clickOnCheckInEnterPincodeSection(page)
{
  await this.page.waitForXPathAndClick(cpl.CartPageLocators.checkButton[this.env])
}
async verifyErrormessageOnClickOfCheck(page)
{
  var errorMessage=await this.page.getText(cpl.CartPageLocators.pincodeErrormessage[this.env])
  var errorMessageDisplayed=errorMessage.includes("Please")
  console.log("****** Error message  displayed is :"+errorMessage)
  return errorMessageDisplayed

}
async closeAddressOnCartWindow(page)
{
  await this.page.waitForXPathAndClick(cpl.CartPageLocators.addressOnCartCloseIcon[this.env])
}
async getQty(page)
{
  const qty=await this.page.getTextXpath(cpl.CartPageLocators.qtyText[this.env])
  var trimQty=qty.split(":")
  var qtySplit=trimQty[1]
  return qtySplit

}
async editSizeInWindow(page)
{
  await this.page.waitForXPathAndClick(cpl.CartPageLocators.editSize[this.env])

}
async getSize(page)
{
  const size=await this.page.getTextXpath(cpl.CartPageLocators.getSize[this.env])
  var trimSize=size.split(":")
  var sizeSplit=trimSize[1]
  return sizeSplit

}
async clickOnPlaceOrder(page)
{
  await this.page.waitForTimeout(3000)
  await this.page.waitForXPathAndClick(cpl.CartPageLocators.clickPlaceOrder[this.env])
  await this.page.waitForTimeout(4000)
}
async verifyServicabilityErrorOnCartPage(page)
{
  await this.page.waitForTimeout(5000)
  var errorMessage= await this.page.getText(cpl.CartPageLocators.nonServicableError[this.env])
  console.log("The error message displayed  in cart is :"+errorMessage)
  var isDisplayed=errorMessage.includes("Not")
  return isDisplayed
}
async clickOnCoupon(page)
{
  await this.page.waitAndClick(cpl.CartPageLocators.applyCouponButton[this.env])
}
async verifyApplyCouponHeader(page)
{
  await this.page.waitForTimeout(3000)
  var header=await this.page.getTextXpath(cpl.CartPageLocators.applyCoupounHeader[this.env])
  if(this.env==="pwa")
  {
    var coupounHeaderDisplayed=header.includes("COUPONS")
    console.log("****** Coupoun Header is :"+header)
  return coupounHeaderDisplayed
  }
  else
  {
    var coupounHeaderDisplayed=header.includes("COUPON")
    console.log("****** Coupoun Header is :"+header)
  return coupounHeaderDisplayed
  }
}
async clickOnCloseWindow(page)
{
  await this.page.waitAndClick(cpl.CartPageLocators.closeWindow[this.env])
}
async verifyItemsNotServicableMessage(page)
{
  await this.page.waitForTimeout(2000)
  var errorMessage=await this.page.getText(cpl.CartPageLocators.itemNotServicable[this.env])
  console.log("the error message for non servicable is :"+errorMessage)
  var isDisplayed=errorMessage.includes("not")
  return isDisplayed

}
async verifyNonServicableErrorWindowOnClickOfPlaceOrder(page)
{
  var nonServicableErrorHeader=await this.page.getText(cpl.CartPageLocators.nonServicableModelError[this.env])
  var isDisplayed=nonServicableErrorHeader.includes("not")
  return isDisplayed
}

async addAddressOnCartPage(page)
{
  if(await this.page.isXPathVisible(cpl.CartPageLocators.enterPincodeButton[this.env]))
  {
    await this.page.waitForXPathAndClick(cpl.CartPageLocators.enterPincodeButton[this.env])
  }
  else
  {
    await this.page.waitForXPathAndClick(cpl.CartPageLocators.changeAddressButton[this.env])
  }
  await this.page.waitForTimeout(5000)
  await this.page.waitForXPathAndClick(cpl.CartPageLocators.addNewAddressButton[this.env])
  await this.page.waitAndType(cpl.CartPageLocators.contactNameFiled[this.env],cpl.addressDetails.name)
  await this.page.waitAndType(cpl.CartPageLocators.contactMobileNumberField[this.env],cpl.addressDetails.mobileNumber)
  await this.page.waitAndType(cpl.CartPageLocators.pincodeField[this.env],cpl.addressDetails.pincode)
  await this.page.waitAndType(cpl.CartPageLocators.streetAddressField[this.env],cpl.addressDetails.streetAddress)
  await this.page.waitAndType(cpl.CartPageLocators.localityField[this.env],cpl.addressDetails.locality)
  //await this.page.waitAndClick(cpl.CartPageLocators.addressType[this.env])
  //await this.page.waitForXPathAndClick(cpl.CartPageLocators.makeThisMyDefaultAddressCheckBox,{clickCount:1})
  await this.page.scrollPage()
  await this.page.waitForTimeout(2000)
  await this.page.click(cpl.CartPageLocators.addAddressButton[this.env])
  await this.page.waitForTimeout(5000)
  const actualName=await this.page.getTextXpath(cpl.CartPageLocators.verifyName[this.env])
  const actualPincode=await this.page.getTextXpath(cpl.CartPageLocators.verifyPincode[this.env])
  if(actualName.toUpperCase()===cpl.addressDetails.name.toUpperCase() && actualPincode.replace(/[, ]+/g, " ").trim()===cpl.addressDetails.pincode)
  {
    console.log("Address added sucessfuly")
    return true
  }
  else
  {
    console.log("Address not added sucessfully")
    return false
  }
}

async changeAddressOnCartPage(page)
{
  await this.page.waitForXPathAndClick(cpl.CartPageLocators.changeAddressButton[this.env])
  await this.page.waitForTimeout(10000)
  await this.page.waitFor(cpl.CartPageLocators.pincodeFieldOnChangeAddress[this.env])
  const listOfAddress=await this.page.getCount(cpl.CartPageLocators.listOfaddressOnCartPage[this.env])
  console.log("Available list of address: "+listOfAddress)
  let i=0;
  while(i<=listOfAddress)
  {
    if(await this.page.isXPathVisible(cpl.CartPageLocators.isAddressSelected[this.env]))
    {
      await this.page.waitForXPathAndClick(cpl.CartPageLocators.isAddressSelected[this.env])
      break
    }
    else
    {
      continue
    }
    i++
  }
  await this.page.waitForTimeout(2000)

}
async clickPlaceOrder(page)
{
  await this.page.waitFor(3000)
  await this.page.click(cpl.CartPageLocators.clickPlaceOrderBtn[this.env])
  //await this.page.waitForXPathAndClick(cpl.CartPageLocators.clickPlaceOrderBtn[this.env])
  await this.page.waitForTimeout(3000)
}

async getItemPriceInPDP(page)
{
  //const qty=await this.page.getTextXpath(cpl.CartPageLocators.qtyText[this.env])
  const itemPriceInPDP = await PdpPage.getTextPicePDP()
  console.log("*** Item price in  ****"+itemPriceInPDP)
  return  itemPriceInPDP
}

async getExpectedInsider(page,itemPriceInPDP)
{
  await this.page.waitForTimeout(2000)
  console.log("Price of the item is passed:********************"+itemPriceInPDP)
  const expectedinsiderpoints = Math.round(((itemPriceInPDP/10)*10)/10)
  console.log("Calculated Expected Insider points is in:____________"+expectedinsiderpoints)
  return expectedinsiderpoints
}

async verifyInsiderPoints(page,expectedInsiderPoints)
{
  await this.page.waitForTimeout(3000)
  console.log("Expected Insider points Passed:____________"+expectedInsiderPoints)
  const actualInsiderPoints =  Math.round(await this.page.getTextXpath(cpl.CartPageLocators.insiderpoints[this.env]))
  console.log("Actual Insider points :____________"+actualInsiderPoints)
  assert.strictEqual(expectedInsiderPoints, actualInsiderPoints)
  
}
async getTotalPrice(page)
    {
    const totalPrice=await this.page.getTextXpath(cpl.CartPageLocators.totalPrice[this.env])
    console.log("**** Total price is :"+totalPrice)
    return totalPrice
    }

}