import AddressPageLocators  from '../objectRepository/AddressPageLocators'
let apl = new AddressPageLocators()

export default class AddressPage {
    constructor(page) {
      this.page = page
      this.env = process.env.platform
    }
    async verifyTryAndBuySection(page)
    {

        console.log("In Address page")
        const isDisplayed=await this.page.isElementVisible(apl.AddressPageLocators.tryAndBuySection[this.env])
        return isDisplayed;
    }
    async verifyTryandBuyFree(page)
    {
       var tryAndBuyText= await this.page.getTextXpath(apl.AddressPageLocators.tryAndBuyFreeText[this.env])
       var tryAndBuyFreeIsDisplayed=tryAndBuyText.includes("Free")
       return tryAndBuyFreeIsDisplayed
    }
    async clickOnHowItWorks(page)
    {
       
        await this.page.waitForXPathAndClick(apl.AddressPageLocators.tryAndBuyHowItWorksButton[this.env])
    }
    async verifyTryAndBlockImage(page)
    {
        await this.page.waitForTimeout(3000)
       const isDisplayed= await this.page.isElementVisible(apl.AddressPageLocators.tryAndBuyImage[this.env])
        return isDisplayed

    }
    async closeTryAndBuyBlockImage(page)
    {
        await this.page.waitForTimeout(3000)
        await this.page.waitForXPathAndClick(apl.AddressPageLocators.tryAndBuyClose[this.env])
    }
    async clickOnTryAndBuyCheckoBox(page)
    {
        await this.page.waitAndClick(apl.AddressPageLocators.tryAndBuyCheckBox[this.env])
       
        await this.page.waitForTimeout(5000)
    }
    async verifyTryAndBuyFreeInPriceBreakUp(page)
    {
       // await this.page.waitForTimeout(2000)
       const tryAndBuyFree= await this.page.getText(apl.AddressPageLocators.tryAndBuyPriceFree[this.env])
       console.log("***Try and buy charge is***"+tryAndBuyFree)
       var isDisplayed=tryAndBuyFree.includes("FREE")
       return isDisplayed
    }
    async verifyTryAndBuyIsNotAllowedForLesserCartValue(page)
    {
        var tryAndBuyNotApplicableforLesserCart= await this.page.getTextXpath(apl.AddressPageLocators.tryAndBuyNotAllowedForLesserPriceMessage[this.env])
        console.log("Error message is :"+tryAndBuyNotApplicableforLesserCart)
        var isDisplayed=tryAndBuyNotApplicableforLesserCart.includes("not")
        return isDisplayed
    }
    async verifyTryAndBuyHeaderForLesserCartValue(page)
    {
        var tryAndBuyNotApplicable= await this.page.getText(apl.AddressPageLocators.tryAndBuyNotAllowedHeader[this.env])
        console.log("Error message is :"+tryAndBuyNotApplicable)
        var isDisplayed=tryAndBuyNotApplicable.includes("not")
        return isDisplayed

    }
    async verifyDeliveryEstimatedSection(page)
    {
        const isDeliveryEstimateDisplayed=await this.page.isElementVisible(apl.AddressPageLocators.deliverySection[this.env])
        return isDeliveryEstimateDisplayed
    }
    async verifyDeliveryDate(page)
        {
            const isDeliveryEstimateDateDisplayed=await this.page.isElementVisible(apl.AddressPageLocators.deliveryDate[this.env])
            var deliveryDate=await this.page.getText(apl.AddressPageLocators.deliveryDate[this.env])
            console.log("Estimated delivery date is :"+deliveryDate)
            return isDeliveryEstimateDateDisplayed 
        }
        async clickAddNewAddressButton(page)
        {
            await this.page.waitForTimeout(3000)
            if(this.env==="pwa")
            {
                await this.page.waitAndClick(apl.AddressPageLocators.addEditAddressButton[this.env])
                await this.page.waitForTimeout(2000)
                await this.page.waitAndClick(apl.AddressPageLocators.addAddress[this.env])
                await this.page.waitForTimeout(2000)
            }
            else
            {
                await this.page.waitForXPathAndClick(apl.AddressPageLocators.addNewAddressButton[this.env])
            }
            
        }
        async clickAddEditAddressButton(page)
        {
            if(this.env==="pwa")
            {
                await this.page.waitAndClick(apl.AddressPageLocators.addEditAddressButton[this.env])
                await this.page.waitForTimeout(2000)
            }
            else
            {
                await this.page.waitForTimeout(2000)
                console.log("Skip this step for desktop")
            }
        }
        async verifyAddNewAddressHeader(page)
        {
            
            var addNewAddressHeader=await this.page.getText(apl.AddressPageLocators.addNewAddressHeader[this.env])
            var isDispalyed=addNewAddressHeader.includes("NEW")
            
            return isDispalyed
        }
        async addNonServicableAddress(page)
        {
            await this.page.waitAndType(apl.AddressPageLocators.contactNameFiled[this.env],apl.firstAaddressDetails.name)
            await this.page.waitAndType(apl.AddressPageLocators.contactMobileNumberField[this.env],apl.firstAaddressDetails.mobileNumber)
            await this.page.waitAndType(apl.AddressPageLocators.pincodeField[this.env],apl.firstAaddressDetails.nonServicablePincode)
            await this.page.waitAndType(apl.AddressPageLocators.streetAddressField[this.env],apl.firstAaddressDetails.streetAddress)
            await this.page.waitAndType(apl.AddressPageLocators.localityField[this.env],apl.firstAaddressDetails.locality)
            await this.page.scrollPage()
            await this.page.waitForTimeout(3000)
            await this.page.click(apl.AddressPageLocators.addAddressButton[this.env])
            await this.page.waitForTimeout(5000)
        }

        async addAddress(page)
        {
            await this.page.waitForTimeout(5000)
            const isEmpty = await this.verifySavedAddress(page)
            console.log(!isEmpty)
            if(!isEmpty)
            {
                console.log("Inside if loop")
                if(this.env==='pwa')
                {
                    await this.page.waitForXPathAndClick(apl.AddressPageLocators.changeAddressBtn[this.env])
                }
                await this.page.waitForXPathAndClick(apl.AddressPageLocators.addNewAddressButton[this.env])
                await this.page.waitAndType(apl.AddressPageLocators.contactNameFiled[this.env],apl.firstAaddressDetails.name)
                await this.page.waitAndType(apl.AddressPageLocators.contactMobileNumberField[this.env],apl.firstAaddressDetails.mobileNumber)
                await this.page.waitAndType(apl.AddressPageLocators.pincodeField[this.env],apl.firstAaddressDetails.pincode)
                await this.page.waitAndType(apl.AddressPageLocators.streetAddressField[this.env],apl.firstAaddressDetails.streetAddress)
                await this.page.waitAndType(apl.AddressPageLocators.localityField[this.env],apl.firstAaddressDetails.locality)
                await this.page.scrollPage()
                await this.page.waitForTimeout(3000)
                await this.page.click(apl.AddressPageLocators.addAddressButton[this.env])
                await this.page.waitForTimeout(5000)

                if(this.env==='desktop')
                {
                    await this.page.evaluate(() => new Promise((resolve) => {
                        var scrollTop = -1;
                        const interval = setInterval(() => {
                          window.scrollBy(0, 100);
                          if(document.documentElement.scrollTop !== scrollTop) {
                            scrollTop = document.documentElement.scrollTop;
                            return;
                          }
                          clearInterval(interval);
                          resolve();
                        }, 10);
                      }));
                }

                const actualPincode=await this.page.getTextXpath(apl.AddressPageLocators.verifyPincode[this.env])
                console.log("the actual pincode is :"+actualPincode.replace(/[, ]+/g, " ").trim())
                if(this.env==="pwa")
                {
                    var addressExpected=apl.firstAaddressDetails.pincode
                    console.log("address Expected is :"+addressExpected)
                    var isAddressAdded=actualPincode.includes(addressExpected)
                    console.log("Address added sucessfuly")
                    return isAddressAdded;
                }
                else
                {
                    if( actualPincode.replace(/[, ]+/g, " ").trim()===apl.firstAaddressDetails.pincode)
                    {
                        console.log("Address added sucessfuly")
                        return true
                    }
                    else
                    {
                        console.log("Address not added sucessfully")
                        return false
                    }
                }
            }
            else
            {
                console.log("Inside else loop")
                await this.page.waitForTimeout(3000)
                await this.page.waitAndType(apl.AddressPageLocators.contactNameFiled[this.env],apl.firstAaddressDetails.name)
                await this.page.waitAndType(apl.AddressPageLocators.contactMobileNumberField[this.env],apl.firstAaddressDetails.mobileNumber)
                await this.page.waitAndType(apl.AddressPageLocators.pincodeField[this.env],apl.firstAaddressDetails.pincode)
                await this.page.waitAndType(apl.AddressPageLocators.streetAddressField[this.env],apl.firstAaddressDetails.streetAddress)
                await this.page.waitAndType(apl.AddressPageLocators.localityField[this.env],apl.firstAaddressDetails.locality)
                await this.page.scrollPage()
                await this.page.waitForTimeout(3000)
                await this.page.click(apl.AddressPageLocators.addAddressButton[this.env])
                await this.page.waitForTimeout(5000)
                const actualPincode=await this.page.getTextXpath(apl.AddressPageLocators.verifyPincode[this.env])
                console.log("the actual pincode is :"+actualPincode.replace(/[, ]+/g, " ").trim())
                if(this.env==="pwa")
                {
                    var addressExpected=apl.firstAaddressDetails.pincode
                    console.log("address Expected is :"+addressExpected)
                    var isAddressAdded=actualPincode.includes(addressExpected)
                    console.log("Address added sucessfuly")
                    return isAddressAdded;
                }
                else
                {
                    if( actualPincode.replace(/[, ]+/g, " ").trim()===apl.firstAaddressDetails.pincode)
                    {
                        console.log("Address added sucessfuly")
                        return true
                    }
                    else
                    {
                        console.log("Address not added sucessfully")
                        return false
                    }
                }
            }  
        }

        async addSecondAddress(page)
        {
            await this.page.waitForTimeout(5000)
            const isEmpty = await this.verifySavedAddress(page)
            console.log(!isEmpty)
            if(!isEmpty)
            {
                console.log("Inside if loop")
                if(this.env==='pwa')
                {
                    await this.page.waitForXPathAndClick(apl.AddressPageLocators.changeAddressBtn[this.env])
                }
                await this.page.waitForXPathAndClick(apl.AddressPageLocators.addNewAddressButton[this.env])
                await this.page.waitAndType(apl.AddressPageLocators.contactNameFiled[this.env],apl.secondAaddressDetails.name)
                await this.page.waitAndType(apl.AddressPageLocators.contactMobileNumberField[this.env],apl.secondAaddressDetails.mobileNumber)
                await this.page.waitAndType(apl.AddressPageLocators.pincodeField[this.env],apl.secondAaddressDetails.pincode)
                await this.page.waitAndType(apl.AddressPageLocators.streetAddressField[this.env],apl.secondAaddressDetails.streetAddress)
                await this.page.waitAndType(apl.AddressPageLocators.localityField[this.env],apl.secondAaddressDetails.locality)
                await this.page.scrollPage()
                await this.page.waitForTimeout(3000)
                await this.page.click(apl.AddressPageLocators.addAddressButton[this.env])
                await this.page.waitForTimeout(5000)

                if(this.env==='desktop')
                {
                    await this.page.evaluate(() => new Promise((resolve) => {
                        var scrollTop = -1;
                        const interval = setInterval(() => {
                          window.scrollBy(0, 100);
                          if(document.documentElement.scrollTop !== scrollTop) {
                            scrollTop = document.documentElement.scrollTop;
                            return;
                          }
                          clearInterval(interval);
                          resolve();
                        }, 10);
                      }));
                }

                const actualPincode=await this.page.getTextXpath(apl.AddressPageLocators.verifyPincode[this.env])
                console.log("the actual pincode is :"+actualPincode.replace(/[, ]+/g, " ").trim())
                if(this.env==="pwa")
                {
                    var addressExpected=apl.secondAaddressDetails.pincode
                    console.log("address Expected is :"+addressExpected)
                    var isAddressAdded=actualPincode.includes(addressExpected)
                    console.log("Address added sucessfuly")
                    return isAddressAdded;
                }
                else
                {
                    if( actualPincode.replace(/[, ]+/g, " ").trim()===apl.secondAaddressDetails.pincode)
                    {
                        console.log("Address added sucessfuly")
                        return true
                    }
                    else
                    {
                        console.log("Address not added sucessfully")
                        return false
                    }
                }
            }
            else
            {
                console.log("Inside else loop")
                await this.page.waitForTimeout(3000)
                await this.page.waitAndType(apl.AddressPageLocators.contactNameFiled[this.env],apl.secondAaddressDetails.name)
                await this.page.waitAndType(apl.AddressPageLocators.contactMobileNumberField[this.env],apl.secondAaddressDetails.mobileNumber)
                await this.page.waitAndType(apl.AddressPageLocators.pincodeField[this.env],apl.secondAaddressDetails.pincode)
                await this.page.waitAndType(apl.AddressPageLocators.streetAddressField[this.env],apl.secondAaddressDetails.streetAddress)
                await this.page.waitAndType(apl.AddressPageLocators.localityField[this.env],apl.secondAaddressDetails.locality)
                await this.page.scrollPage()
                await this.page.waitForTimeout(3000)
                await this.page.click(apl.AddressPageLocators.addAddressButton[this.env])
                await this.page.waitForTimeout(5000)
                const actualPincode=await this.page.getTextXpath(apl.AddressPageLocators.verifyPincode[this.env])
                console.log("the actual pincode is :"+actualPincode.replace(/[, ]+/g, " ").trim())
                if(this.env==="pwa")
                {
                    var addressExpected=apl.secondAaddressDetails.pincode
                    console.log("address Expected is :"+addressExpected)
                    var isAddressAdded=actualPincode.includes(addressExpected)
                    console.log("Address added sucessfuly")
                    return isAddressAdded;
                }
                else
                {
                    if( actualPincode.replace(/[, ]+/g, " ").trim()===apl.secondAaddressDetails.pincode)
                    {
                        console.log("Address added sucessfuly")
                        return true
                    }
                    else
                    {
                        console.log("Address not added sucessfully")
                        return false
                    }
                }
            }  
        }

        async editAddress(page)
        {
            await this.page.waitForTimeout(2000)
            if(this.env==='pwa')
            {
                await this.page.waitForXPathAndClick(apl.AddressPageLocators.changeAddressBtn[this.env])
            }

            await this.page.evaluate(() => new Promise((resolve) => {
                var scrollTop = -1;
                const interval = setInterval(() => {
                  window.scrollBy(0, 100);
                  if(document.documentElement.scrollTop !== scrollTop) {
                    scrollTop = document.documentElement.scrollTop;
                    return;
                  }
                  clearInterval(interval);
                  resolve();
                }, 10);
              }));

            await this.page.waitForXPathAndClick(apl.AddressPageLocators.editButton[this.env])
            let name=await this.page.$(apl.AddressPageLocators.contactNameFiled[this.env])
            name.click({clickCount: 3})
            name.press('Backspace')
            await this.page.waitForTimeout(1000)
            await this.page.waitAndType(apl.AddressPageLocators.contactNameFiled[this.env],apl.editAddressDetails.name)
            let mobile=await this.page.$(apl.AddressPageLocators.contactMobileNumberField[this.env])
            mobile.click({clickCount: 3})
            mobile.press('Backspace')
            await this.page.waitForTimeout(1000)
            await this.page.waitAndType(apl.AddressPageLocators.contactMobileNumberField[this.env],apl.editAddressDetails.mobileNumber)
            let pincodeF=await this.page.$(apl.AddressPageLocators.pincodeField[this.env])
            pincodeF.click({clickCount: 3})
            pincodeF.press('Backspace')
            await this.page.waitForTimeout(1000)
            await this.page.waitAndType(apl.AddressPageLocators.pincodeField[this.env],apl.editAddressDetails.pincode)
            let street=await this.page.$(apl.AddressPageLocators.streetAddressField[this.env])
            street.click({clickCount: 3})
            street.press('Backspace')
            await this.page.waitForTimeout(1000)
            await this.page.waitAndType(apl.AddressPageLocators.streetAddressField[this.env],apl.editAddressDetails.streetAddress)
            let locality=await this.page.$(apl.AddressPageLocators.localityField[this.env])
            locality.click({clickCount: 3})
            locality.press('Backspace')
            await this.page.waitForTimeout(3000)
            if(this.env==="pwa")
            {
                await this.page.waitAndType(apl.AddressPageLocators.optionTextField[this.env],apl.editAddressDetails.locality)
                await this.page.waitForXPathAndClick(apl.AddressPageLocators.doneButton[this.env])
            }
            else
            {
                await this.page.waitAndType(apl.AddressPageLocators.localityField[this.env],apl.editAddressDetails.locality)
            }
            await this.page.waitForTimeout(1000)
            await this.page.waitForXPathAndClick(apl.AddressPageLocators.saveAddressButton[this.env])
            await this.page.waitForTimeout(5000)

            if(this.env==='desktop')
                {
                    await this.page.evaluate(() => new Promise((resolve) => {
                        var scrollTop = -1;
                        const interval = setInterval(() => {
                          window.scrollBy(0, 100);
                          if(document.documentElement.scrollTop !== scrollTop) {
                            scrollTop = document.documentElement.scrollTop;
                            return;
                          }
                          clearInterval(interval);
                          resolve();
                        }, 10);
                      }));
                }

            const actualPincode=await this.page.getTextXpath(apl.AddressPageLocators.verifyPincode[this.env])
            console.log("the actual pincode is :"+actualPincode.replace(/[, ]+/g, " ").trim())
            if(this.env==="pwa")
            {
                var addressExpected=apl.editAddressDetails.pincode
                console.log("address Expected is :"+addressExpected)
                var isAddressAdded=actualPincode.includes(addressExpected)
                console.log("Address Edited sucessfuly")
                return isAddressAdded;
            }
            else
            {
                if( actualPincode.replace(/[, ]+/g, " ").trim()===apl.editAddressDetails.pincode)
                {
                    console.log("Address edited sucessfuly")
                    return true
                }
                else
                {
                    console.log("Address not edited sucessfully")
                    return false
                }
            }

        }

        async changeAddressOnAddressPage(page)
        {
            await this.page.waitForTimeout(10000)
            if(this.env==='pwa')
            {
                var currentAddress=await this.page.getText(apl.AddressPageLocators.addressSelected[this.env])
                console.log("Current address is: "+currentAddress)
                await this.page.waitForXPathAndClick(apl.AddressPageLocators.changeAddressBtn[this.env])
            }
            else
            {
                var currentAddressOnDesktop=await this.page.getTextXpath(apl.AddressPageLocators.addressSelected[this.env])
                console.log("Current address is: "+currentAddressOnDesktop)
            }
            const elements = await this.page.$$(apl.AddressPageLocators.listOfAddress[this.env])
        
            console.log("***** the length is "+elements.length)
            if(elements.length==0 || elements.length==1)
            {
                console.log("Minimum two addrsses required for change address ***********")
                return false
            }
            else
            {
                var names = await page.$$eval(apl.AddressPageLocators.listOfAddress[this.env],elements=> elements.map(item=>item.textContent))
                await console.log("User names: "+names)

                console.log("User name: "+names[0])
                await elements[0].click()
                await this.page.waitForTimeout(3000)

                if(this.env==='pwa')

                {
                    await this.page.waitForXPathAndClick(apl.AddressPageLocators.confirmButton[this.env])
                    await this.page.waitForTimeout(2000)
                }
            }
            if(this.env==='pwa')
            {
                var addressAfterChanging=await this.page.getText(apl.AddressPageLocators.addressSelected[this.env])
                console.log("Address after changing is: "+addressAfterChanging)
                if(addressAfterChanging!=currentAddress)
                {
                    console.log("Address changed succesfully")
                    return true
                }
                else
                {
                    return false
                }
            }else
            {
                var addressAfterChangingOnDesktop=await this.page.getTextXpath(apl.AddressPageLocators.addressSelected[this.env])
                console.log("Address after changing is: "+addressAfterChangingOnDesktop)
                if(addressAfterChangingOnDesktop!=currentAddressOnDesktop)
                {
                    console.log("Address changed succesfully")
                    return true
                }
                else
                {
                    return false
                }
            }
            
        }

       async addAddressforNonNCR(page)
       {
        await this.page.waitAndType(apl.AddressPageLocators.contactNameFiled[this.env],apl.firstAaddressDetails.name)
        await this.page.waitAndType(apl.AddressPageLocators.contactMobileNumberField[this.env],apl.firstAaddressDetails.mobileNumber)
        await this.page.waitAndType(apl.AddressPageLocators.pincodeField[this.env],apl.firstAaddressDetails.nonNCRPincode)
        await this.page.waitAndType(apl.AddressPageLocators.streetAddressField[this.env],apl.firstAaddressDetails.nonNCRStreetAddress)
        await this.page.waitAndType(apl.AddressPageLocators.localityField[this.env],apl.firstAaddressDetails.locality)
        await this.page.scrollPage()
        await this.page.waitForTimeout(3000)
        await this.page.click(apl.AddressPageLocators.addAddressButton[this.env]) 
        await this.page.waitForTimeout(3000)
       }
       async verifyBadAddressScoringError(page)
       {


           var error=await this.page.getText(apl.AddressPageLocators.badAddressScoringError[this.env])
           console.log("the bad address scoring error is :"+error)
           var isDisplayed=error.includes("add")
           return isDisplayed
       }
       async verifyBadAddressScoring(page)
       {
           let i=1;
           let Iterate=3;
           while(i<=Iterate)
           {
            await this.page.click(apl.AddressPageLocators.addAddressButton[this.env]) 
            console.log("****** Iteration******")
            i++;
           }
           await this.page.waitFor(2000)
          
           
       }
    async verifyisAddressBlockErrorDisplayed(page)
    {
        if(this.env==="pwa")
        {
            console.log("skip this step")
            return true
        }
        else{
            const isDisplayed=await this.page.isElementVisible(apl.AddressPageLocators.addressBlockError[this.env])
        return isDisplayed
        }
        
    }
    
   async clickContinue(page)
    {
        await this.page.waitAndClick(apl.AddressPageLocators.clickContinue[this.env])
        await this.page.waitFor(4000)
        
    }
    async getAddressBlockError(page)
    {
        if(this.env==="pwa")
        {
            console.log("skip this step")
            return true
        }
        else{
            const isErrorDisplayed=await this.page.getText(apl.AddressPageLocators.addressBlockError[this.env])
            console.log("*** The Error message is :"+isErrorDisplayed)
            var displayed=isErrorDisplayed.includes("Sorry")
            return displayed
        }
        
    }
   
    async verifyNonServicableHeaderMessage(page)
    {
        const isErrorDisplayed=await this.page.getText(apl.AddressPageLocators.nonServicableHeaderMessage[this.env])
        console.log("*** The Error message is :"+isErrorDisplayed)
        var displayed=isErrorDisplayed.includes("non-deliverable")
        return displayed
    }
    async verifyIsGoToBagDisplayed(page)
    {
        const isDispalyed=await this.page.isElementVisible(apl.AddressPageLocators.goToBagButton[this.env])
        return isDispalyed
    }
    async clickGoToBag(page)
    {
        await this.page.click(apl.AddressPageLocators.goToBagButton[this.env])
        await this.page.waitForTimeout(2000)

    }
    async selectNonServicableAddress(page)
    {
        await this.page.waitForXPathAndClick(apl.AddressPageLocators.selectNonServicableAddress[this.env])
    }
    async removeAddress(page)
    {
        await this.page.waitAndClick(apl.AddressPageLocators.removeAddress[this.env])
    }
    async removeAllAddresses(page)
    {
        await this.page.waitForTimeout(5000)
        const isEmpty = await this.verifySavedAddress(page)
        console.log(!isEmpty)
        if(isEmpty===true)
        {
            return true
        }
        if(!isEmpty){
            if(this.env==='pwa')
            {
                await this.page.waitForXPathAndClick(apl.AddressPageLocators.changeAddressBtn[this.env])
            }
            console.log("Removing the address from address page inside if loop")
            await this.page.waitForTimeout(3000)
            const elements = await this.page.$$(apl.AddressPageLocators.listOfAddress[this.env])
        
            console.log("***** the length is "+elements.length)
            let i=0;
            while(elements.length>0){
                console.log("inside while loop")
                await this.page.waitForXPathAndClick(apl.AddressPageLocators.removeButton[this.env])
                await this.page.waitForTimeout(3000)
                elements.length= elements.length-1
                console.log("***** the length is after clear "+elements.length)
                console.log("****** Iteration******")
                const isEmptyAfterRemove = await this.verifySavedAddress(page)
                 if(!isEmptyAfterRemove)
                 {
                    await this.page.evaluate(() => {
                        location.reload(true)
                     })
                    await this.page.waitForTimeout(3000)
                    if(this.env==='pwa')
                    {
                        await this.page.waitForXPathAndClick(apl.AddressPageLocators.changeAddressBtn[this.env])
                        await this.page.waitForTimeout(2000)
                    }
                 }
                i++;
            }
            await this.page.waitForTimeout(2000)
            const isEmptyAfterRemoving = await this.verifySavedAddress(page)
            if(isEmptyAfterRemoving)
            {
                console.log("Removed all the addresses in address page successfully")
                return true
            }
            else
            {   
                return false
            }
        }

    }
    async verifySavedAddress(page)
    {
        console.log("Verifying the saved address.....................")
        if (await page.$(apl.AddressPageLocators.contactNameFiled[this.env]) !== null) {
            console.log("There is no saved address")
            return true
        }
            
        else
        {
            console.log('Address page has saved address');
            return false
        
    }

    }

    async getTotalPrice(page)
    {
    const totalPrice=await this.page.getTextXpath(apl.AddressPageLocators.totalPrice[this.env])
    console.log("**** Total price is :"+totalPrice)
    return totalPrice
    }
    
}