import LoginPageLocators  from '../objectRepository/LoginPageLocators'
let lpl = new LoginPageLocators()

export default class LoginPage {
    constructor(page) {
      this.page = page
      this.env = process.env.platform
    }

    async login(page) {
      
      await page.goto('https://www.myntra.com/login/password')   
      await this.page.waitAndType(lpl.loginLocators.user_id[this.env], lpl.loginDetails.emailId)
      await this.page.waitAndType(lpl.loginLocators.user_password[this.env], lpl.loginDetails.password)
      await this.page.click(lpl.loginLocators.login_button[this.env])
      await this.page.waitForTimeout(4000)
    }
    async myntraCreditlogin(page) {
      
      await page.goto('https://www.myntra.com/login/password')   
      await this.page.waitAndType(lpl.loginLocators.user_id[this.env], lpl.loginDetails.myntraCreditEmailId)
      await this.page.waitAndType(lpl.loginLocators.user_password[this.env], lpl.loginDetails.myntraCreditPassword)
      await this.page.click(lpl.loginLocators.login_button[this.env])
      await this.page.waitForTimeout(4000)
    }

    async isLoginPageDisplayed(page){
      await page.goto('https://www.myntra.com/login/password')
      return await this.page.isElementVisible(lpl.loginLocators.user_id[this.env]);
    }
    async verifyLoginPageDisplayed(page){
    var url=  await this.page.getCurrentURL([this.env])
  console.log("******* the current page URL is ********"+url)
      return await this.page.isElementVisible(lpl.loginLocators.login_Section[this.env]);
    }

    async verifyMobileHardlogin(page){
      var url=  await this.page.getCurrentURL([this.env])
      console.log("******* the current page URL is ********"+url)
      await this.page.waitForTimeout(3000)
      await this.page.isElementVisible(lpl.loginLocators.user_mobile_login[this.env]);
      console.log("Login Mobile PAGE+++++++++++++++")
      await this.page.waitAndType(lpl.loginLocators.user_mobile_login[this.env], lpl.loginDetails.mobiledummy)
    
      //await this.page.waitAndType(lpl.loginLocators.user_mobile_login[this.env], lpl.loginDetails.emailId)
     // await this.page.waitFor(2000)
      await this.page.click(lpl.loginLocators.mob_continueBtn[this.env])
      await this.page.waitForTimeout(3000)
      await this.page.click(lpl.loginLocators.mob_login_with_password[this.env])
      await this.page.waitForTimeout(2000)

     // await this.page.click(lpl.loginLocators.mob_continueBtn[this.env])
     
      let loginUsernametxt = await page.$(lpl.loginLocators.mob_username[this.env])
      await loginUsernametxt.click({clickCount: 10})
      await loginUsernametxt.press('Backspace')
      console.log("Enter Mobile NUMBER+++++++++++++++")
      //await this.page.click(lpl.loginLocators.mob_username[this.env]) 
      await page.keyboard.sendCharacter('9620117709')
     // await this.page.type(lpl.loginLocators.user_mobile_login[this.env], lpl.loginDetails.emailId)
      console.log("EnterED Mobile NUMBER+++++++++++++++")
    
      await this.page.waitAndType(lpl.loginLocators.mob_password[this.env], lpl.loginDetails.password)
      await this.page.click(lpl.loginLocators.mob_clickLoginBtn[this.env])
      await this.page.waitForTimeout(4000)
      }
    
      async verifyMobileLogin(page){
        var url=  await this.page.getCurrentURL([this.env])
        console.log("******* the current page URL is ********"+url)
        
        await this.page.waitForTimeout(3000)
        await this.page.isElementVisible(lpl.loginLocators.user_mobile_login[this.env]);
        console.log("Login Mobile PAGE+++++++++++++++")
        await this.page.waitAndType(lpl.loginLocators.user_mobile_login[this.env], lpl.loginDetails.mobiledummy)
        await this.page.click(lpl.loginLocators.mob_continueBtn[this.env])
        await this.page.waitForTimeout(3000)
        await this.page.click(lpl.loginLocators.mob_login_with_password[this.env])
        await this.page.waitForTimeout(2000)
        let loginUsernametxt = await page.$(lpl.loginLocators.mob_username[this.env])
        await loginUsernametxt.click({clickCount: 10})
        await loginUsernametxt.press('Backspace')
        
        console.log("Enter Mobile NUMBER+++++++++++++++")
        await this.page.click(lpl.loginLocators.mob_username[this.env]) 
        await this.page.waitAndType(lpl.loginLocators.user_mobile_login[this.env], lpl.loginDetails.emailId)
       
        //await page.keyboard.sendCharacter('9620117709')
        console.log("EnterED Mobile NUMBER+++++++++++++++")
        await this.page.waitAndType(lpl.loginLocators.mob_password[this.env], lpl.loginDetails.password)
        await this.page.click(lpl.loginLocators.mob_clickLoginBtn[this.env])
        await this.page.waitForTimeout(5000)
        console.log("Login DONE+++++++++++++++++++++++++++++")

        }

  }
  