import PaymentsPageLocators  from '../objectRepository/PaymentsPageLocators'
let psl = new PaymentsPageLocators()

export default class PaymentsPage {
    constructor(page) {
      this.page = page
      this.env = process.env.platform
    }
async verfiyOfferSectionDisplayed(page)
{
    await this.page.waitForTimeout(4000)
 const displayed= await this.page.isXPathVisible(psl.PaymentsPageLocators.offerSection[this.env])
 return displayed
}
async verifyOfferSectionHeader(page)
{
    await this.page.waitForTimeout(2000)
  var offer=await this.page.getText(psl.PaymentsPageLocators.offerSectionHeader[this.env])
  console.log("Offer Section header is:"+offer)
  var isDisplayed=offer.includes("Offer")
  return isDisplayed
}
async clickOnShowMoreInOfferSection(page)
{
  await this.page.click(psl.PaymentsPageLocators.showMoreButton[this.env])
}
async getTotalPrice(page)
{
    await this.page.waitForTimeout(2000)
    const totalPrice=await this.page.getTextXpath(psl.PaymentsPageLocators.totalPrice[this.env])
    console.log("**** Total price is :"+totalPrice)
    return totalPrice
}
async verifyGiftCardPaymentOption(page)
{
    await this.page.waitForTimeout(2000)
    const isGiftcardOptionDisplayed=await this.page.isXPathVisible(psl.PaymentsPageLocators.giftCardSection[this.env])
    return isGiftcardOptionDisplayed
}
async clickOnApplyGiftCard(page)
{
    await this.page.click(psl.PaymentsPageLocators.applyGiftCardButton[this.env])
}
async verifyApplyGiftcardHeader(page)
{
    const header=await this.page.getText(psl.PaymentsPageLocators.giftCardHeaderText[this.env])
    const isDisplayed=header.includes("Gift")
    return isDisplayed
}
async enterGiftCardDetails(page)
{
    await this.page.waitForXpathAndType(psl.PaymentsPageLocators.giftCardNumber[this.env],psl.paymentDetails.giftCardNumber)
    await this.page.waitForXpathAndType(psl.PaymentsPageLocators.giftCardPin[this.env],psl.paymentDetails.giftCardPin)
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.addToMyntraCreditButton[this.env])
}
async verifyErrorMessageOnAddingGiftCard(page)
{
   var message= await this.page.getText(psl.PaymentsPageLocators.giftCardErrorMessage[this.env])
   var isDisplayed=message.includes("already")
   return isDisplayed
}
async verifyAndClickEMIPaymentOption(page)
{
    await this.page.waitForTimeout(4000)
    const isEMIOptionDisplayed=await this.page.isElementVisible(psl.PaymentsPageLocators.emiOption[this.env])
    await this.page.click(psl.PaymentsPageLocators.emiOption[this.env])
    return isEMIOptionDisplayed
}
async verifyEMIOptionHeader(page)
{
    if(this.env==="pwa")
    {
        console.log("skip this step")
        await this.page.waitForTimeout(2000)
        return true
    }
    else{
        await this.page.waitForTimeout(2000)
        var header=await this.page.getText(psl.PaymentsPageLocators.emiHeader[this.env])
        var isDisplayed=header.includes("EMI")
        return isDisplayed
    }
   
}
async selectEMIOption(page)
{
    await this.page.waitForTimeout(3000)
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.selectEmiOption[this.env])
    await this.page.waitForTimeout(3000)
    console.log("selected EMI")
}
async clickEMIPaynow(page)
{
    if(this.env==="pwa")
    {
        await this.page.waitForTimeout(3000)
        await this.page.click(psl.PaymentsPageLocators.placeOrderButtonPWA[this.env])
        await this.page.waitForTimeout(3000)
    }
    else
    {
        await this.page.waitForTimeout(3000)
        await this.page.click(psl.PaymentsPageLocators.emiPaynow[this.env])
    }
    
}
async waitForPaymentsPageLoader(page)
{
    await this.page.waitForTimeout(8000)
}
async verifyRedirectionForEMIOption(page)
{
    await this.page.waitForTimeout(8000)
   var displayed= await this.page.isElementVisible(psl.PaymentsPageLocators.emiRedirection[this.env])
   console.log("Myntra logo is displayed:"+displayed)
   var url = await this.page.url([this.env])
   var isEmiDisplayed=url.includes("emi")
   return isEmiDisplayed
}
async verifyAndClickCreditDebitCardPaymentOption(page)
{
    await this.page.waitForTimeout(6000)
    const isCreditDebitOptionDisplayed=await this.page.isElementVisible(psl.PaymentsPageLocators.creditDebitCardOption[this.env])
    await this.page.click(psl.PaymentsPageLocators.creditDebitCardOption[this.env])
    return isCreditDebitOptionDisplayed
}
async verifyCreditDebitCardOptionHeader(page)
{
    if(this.env==="pwa")
    {
        console.log("skip this step")
        await this.page.waitForTimeout(2000)
        return true
    }
    else{
        await this.page.waitForTimeout(1000)
        var header=await this.page.getText(psl.PaymentsPageLocators.creditDebitCardHeader[this.env])
        var isDisplayed=header.includes("CREDIT")
        return isDisplayed
    }
}
async verifyOnClickOfCVVInfoIcon(page)
{
    await this.page.waitForTimeout(2000)
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.cvvInfoIconToolTip[this.env])
    await this.page.waitForTimeout(4000)
    const isDisplyed=await this.page.isXPathVisible(psl.PaymentsPageLocators.cvvInfoModal[this.env])
    await this.page.waitForTimeout(2000)
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.cvvCloseModal[this.env])
    await this.page.waitForTimeout(2000)
    return isDisplyed

}
async verifyOnClickOfSaveCardInfoIcon(page)
{
    await this.page.waitForTimeout(2000)
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.savedCardInfoIcon[this.env])
    const isDisplayed=await this.page.isElementVisible(psl.PaymentsPageLocators.savedCardDetails[this.env])
    return isDisplayed

} 
async enterCreditDebitCardDetails(page)
{
    await this.page.waitForXpathAndType(psl.PaymentsPageLocators.creditCardNumber[this.env],psl.paymentDetails.creditCardNumber)
    await this.page.waitForXpathAndType(psl.PaymentsPageLocators.creditCardName[this.env],psl.paymentDetails.creditCardName)
    await this.page.waitForXpathAndType(psl.PaymentsPageLocators.creditCardExpiry[this.env],psl.paymentDetails.creditCardExpiry)
    await this.page.waitForXpathAndType(psl.PaymentsPageLocators.creditCardcvv[this.env],psl.paymentDetails.creditCardcvv)
    await this.page.waitForTimeout(3000)
    const isDisplayed= await this.page.isElementVisible(psl.PaymentsPageLocators.creditCardLogo[this.env])
     console.log("The logo is displayed "+isDisplayed)
    await this.page.click(psl.PaymentsPageLocators.uncheckSaveCard[this.env])
    await this.page.waitForTimeout(4000)
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.clickCreditDebitCardPaynow[this.env])
    console.log("clicked paynow")
}
async verifyAndClickWalletPaymentOption(page)
{
    await this.page.waitForTimeout(5000)
    const isWalletOptionDisplayed=await this.page.isElementVisible(psl.PaymentsPageLocators.walletOption[this.env])
    await this.page.click(psl.PaymentsPageLocators.walletOption[this.env])
    await this.page.waitForTimeout(3000)
    return isWalletOptionDisplayed
}
async selectWalletOption(page)
{
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.selectWalletRadioButton[this.env])
    await this.page.waitForTimeout(2000)
}
async verifyWalletOptionHeader(page)
{
    if(this.env==="pwa")
    {
        console.log("skip this step")
        await this.page.waitForTimeout(2000)
        return true
    }
    else{
        await this.page.waitForTimeout(1000)
        var header=await this.page.getTextXpath(psl.PaymentsPageLocators.walletHeader[this.env])
        var isDisplayed=header.includes("wallet")
        return isDisplayed
    }
   
}
async getSelectedWalletName(page)
{
    await this.page.waitForTimeout(2000)
 var walletTexts = await page.$$eval(psl.PaymentsPageLocators.walletName[this.env],
                elements=> elements.map(item=>item.textContent))
    console.log("the text displayed is :"+walletTexts)
    var wallet=walletTexts[0].toLowerCase()
    const walletName = wallet.split(/\s/).join('');
    console.log('text = ' +walletName);
    return walletName
}
async clickWalletPaynow(page)
{
    if(this.env==="pwa")
    {
        await this.page.waitForTimeout(3000)
        await this.page.click(psl.PaymentsPageLocators.placeOrderButtonPWA[this.env])
        await this.page.waitForTimeout(3000)
    }
    else
    {
        await this.page.waitForTimeout(3000)
        await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.walletPaynow[this.env])
    }
}
async verifyRedirectionForWalletOption(page)
{
    await this.page.waitForTimeout(8000)
   var url = await this.page.url([this.env])
   console.log("the URL is:"+url)
   return url
}
async verifyAndClickUPIPaymentOption(page)
{
    await this.page.waitForTimeout(4000)
    const isUPIOptionDisplayed=await this.page.isElementVisible(psl.PaymentsPageLocators.upiOption[this.env])
    await this.page.click(psl.PaymentsPageLocators.upiOption[this.env])
    await this.page.waitForTimeout(2000)
    return isUPIOptionDisplayed
}
async verifyUPIOptionHeader(page)
{
    if(this.env==="pwa")
    {
        console.log("skip this step")
        await this.page.waitForTimeout(2000)
        return true
    }
    else{
        await this.page.waitForTimeout(1000)
        var header=await this.page.getText(psl.PaymentsPageLocators.upiHeader[this.env])
        var isDisplayed=header.includes("UPI")
        return isDisplayed
    }
}
async selectPhonePeOption(page)
{
    await this.page.waitForTimeout(3000)
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.phonepeUPI[this.env])
}
async selectOtherUPIOption(page)
{
    await this.page.waitForTimeout(3000)
    await this.page.click(psl.PaymentsPageLocators.otherUPI[this.env])

}
async verifyRedirectionForPhonePe(page)
{
    await this.page.waitForTimeout(4000)
    var url = await this.page.url([this.env])
   var isPhonePeDisplayed=url.includes("phonepe")
   return isPhonePeDisplayed
}
async clickPhonePeUPIPaynow(page)
{
    if(this.env==="pwa")
    {
        await this.page.waitForTimeout(2000)
        await this.page.click(psl.PaymentsPageLocators.placeOrderButtonPWA[this.env])
        await this.page.waitForTimeout(2000)
        
    }
    else
    {
        await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.phonepeUPIPaynow[this.env])
    }
}
async enterValidUPIID(page)
{
    await this.page.waitForTimeout(3000)
    await this.page.waitForXpathAndType(psl.PaymentsPageLocators.enterGpayUPIID[this.env],psl.paymentDetails.validUpiID)
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.uncheckSaveUPI[this.env])
    await this.page.waitForTimeout(3000)
}
async enterInvalidUPIID(page)
{
    await this.page.waitForXpathAndType(psl.PaymentsPageLocators.enterUPIID[this.env],psl.paymentDetails.invalidUpiID)
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.uncheckSaveUPI[this.env])
    await this.page.waitForTimeout(3000)
}
async clickUPIIDPaynow(page)
{
    if(this.env==="pwa")
    {
        await this.page.waitForTimeout(3000)
        await this.page.click(psl.PaymentsPageLocators.placeOrderButtonPWA[this.env])
        await this.page.waitForTimeout(3000)
        
    }
    else
    {
        await this.page.waitForTimeout(2000)
        await this.page.click(psl.PaymentsPageLocators.otherUPIPayNow[this.env])
        await this.page.waitForTimeout(3000)
    }
}
async clickGpayUPIIDPaynow(page)
{
    if(this.env==="pwa")
    {
        await this.page.waitForTimeout(3000)
        await this.page.click(psl.PaymentsPageLocators.placeOrderButtonPWA[this.env])
        await this.page.waitForTimeout(3000)
        
    }
    else
    {
        
        await this.page.click(psl.PaymentsPageLocators.gpayPayNow[this.env])
        await this.page.waitForTimeout(3000)
    }
}
async verifyValidationForUPI(page)
{
    await this.page.waitForTimeout(3000)
   const error= await this.page.getText(psl.PaymentsPageLocators.upiErrorMessage[this.env])
   console.log("the error displayed is :"+error)
   var isDisplayed=error.includes("Invalid")
   return isDisplayed
}
async verifyOnClickOfSavedUPIInfoIcon(page)
{
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.savedUPIInfoIcon[this.env])
    await this.page.waitForTimeout(2000)
    const isDisplayed=await this.page.isElementVisible(psl.PaymentsPageLocators.savedUPIDetails[this.env])
    return isDisplayed
}
async selectGpayUPIOption(page)
{
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.selectGpayOption[this.env])
    await this.page.waitForTimeout(1000)

}
async verifyRedirectionForGpay(page)
{
    var url = await this.page.url([this.env])
    var isGpayDisplayed=url.includes("upi")
    console.log("is UPI header displayed"+isGpayDisplayed)
    return isGpayDisplayed
}
async verifyAndClickCODPaymentOption(page)
{
    await this.page.waitForTimeout(3000)
    const isCODOptionDisplayed=await this.page.isElementVisible(psl.PaymentsPageLocators.codOption[this.env])
    await this.page.click(psl.PaymentsPageLocators.codOption[this.env])
    return isCODOptionDisplayed
}
async verifyCODHeader(page)
{
    if(this.env==="pwa")
    {
        await this.page.waitForTimeout(3000)
        console.log("skip this step")
        return true
    }
    else
    {
        const header=await this.page.getText(psl.PaymentsPageLocators.CODHeaderText[this.env])
        const isDisplayed=header.includes("delivery")
        return isDisplayed
    }
   
}
async verifyCaptchaSectionDisplayed(page)
{
    const captchaisDispalyed=await this.page.isXPathVisible(psl.PaymentsPageLocators.captcha[this.env])
    
    return captchaisDispalyed
}
async verifyErrorMessageForCOD(page)
{    await this.page.waitForTimeout(3000)
    var message= await this.page.getText(psl.PaymentsPageLocators.codErrorMessage[this.env])
    var isDisplayed=message.includes("code")
    return isDisplayed
}
async clickOnPlaceOrder(page)
{
    if(this.env==="pwa")
    {
        await this.page.waitForTimeout(3000)
        await this.page.click(psl.PaymentsPageLocators.placeOrderButtonPWA[this.env])
        await this.page.waitForTimeout(3000)
    }
    else
    {
        await this.page.waitForTimeout(2000)
        await this.page.click(psl.PaymentsPageLocators.placeOrderButton[this.env])  
    }

}
async verifyCODInfoMessage(page)
{
  var codMessage=await this.page.getText(psl.PaymentsPageLocators.codInfoText[this.env])
  const isDisplayed=codMessage.includes("Cash/Card")
  return isDisplayed
}
async verifyAndClickNetbankingPaymentOption(page)
{
    
    await this.page.waitForTimeout(4000)
    const isNetbankingOptionDisplayed=await this.page.isElementVisible(psl.PaymentsPageLocators.netBankingOption[this.env])
    await this.page.click(psl.PaymentsPageLocators.netBankingOption[this.env])
    return isNetbankingOptionDisplayed
}
async verifyNetbankingHeader(page)
{
    if(this.env==="pwa")
    {
        console.log("skip this step")
        await this.page.waitForTimeout(3000)
        return true
    }
    else
    {
        await this.page.waitForTimeout(4000)
        const header=await this.page.getText(psl.PaymentsPageLocators.netbankingHeader[this.env])
        const isDisplayed=header.includes("Net Banking")
        return isDisplayed
    }
    
}
async selectDefaultNetBankingOption(page)
{
    await this.page.waitForTimeout(3000)
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.selectDefaultNetbankingOption[this.env])
}
async getSelectedNetbankingName(page)
{
    await this.page.waitForTimeout(4000)
    var netBankingName = await page.$$eval(psl.PaymentsPageLocators.defaultNetbankingName[this.env],
                   elements=> elements.map(item=>item.textContent))
       console.log("the text displayed is :"+netBankingName)
       var netbankName=netBankingName[0].toLowerCase()
      
    const bankName = netbankName.split(/\s/).join('');
    console.log("new text is :"+bankName)
    return bankName
   
}
async clickNetBankingPaynow(page)
{
    if(this.env==="pwa")
    {
        await this.page.waitForTimeout(3000)
        await this.page.click(psl.PaymentsPageLocators.placeOrderButtonPWA[this.env])
        await this.page.waitForTimeout(3000)
    }
    else
    {
        await this.page.waitForTimeout(3000)
        await this.page.click(psl.PaymentsPageLocators.netbankingPaynow[this.env])
        await this.page.waitForTimeout(3000)
    }
}
async verifyRedirectionForNetbankingOption(page)
{
    await this.page.waitForTimeout(15000)
  
    var url = await this.page.url([this.env])
    console.log("the URL is:"+url)
    return url
}
async selectNetBankingFromDropDown(page)
{
   
    
    await this.page.scrollPage()
    console.log("Scrolled")
    await this.page.waitForTimeout(4000)
   await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.otherNetbankingOption[this.env])
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.selctFromDropdown[this.env])
}
async clickOthertNetbankingPaynow(page)
{
    if(this.env==="pwa")
    {
        await this.page.waitForTimeout(2000)
        await this.page.click(psl.PaymentsPageLocators.placeOrderButtonPWA[this.env])
        await this.page.waitForTimeout(3000)
    }
    else
    {
        await this.page.waitForTimeout(3000)
        await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.otherPaynow[this.env])
        await this.page.waitForTimeout(2000)
    }
   
}
async getSelectedDropdownNetbankingName(page)
{
    await this.page.waitForTimeout(4000)
   var netbankingName= await this.page.getText(psl.PaymentsPageLocators.selectedOtherNetbankingName[this.env])
    const bankName = netbankingName.split(/\s/).join('').toLowerCase();
    console.log("new text is :"+bankName)
    return bankName  
}
async verifySavedUPIOptionDisplayed(page)
{
    await this.page.waitForTimeout(2000)
    if(this.env==="pwa")
    {
        console.log("skip this step")
        await this.page.waitForTimeout(2000)
        return true
    }
    else{
        await this.page.waitForTimeout(2000)
        const isSavedUPIOptionDisplayed=await this.page.isXPathVisible(psl.PaymentsPageLocators.savedUPISection[this.env])
         return isSavedUPIOptionDisplayed
    }
    
}
async verifyLowSuccessRateMessageForNetbanking(page)
{
    await this.page.waitForTimeout(2000)
    if(await this.page.isXPathVisible(psl.PaymentsPageLocators.losSuccessRateMeassage[this.env]))
    {
        const lowSR=await this.page.getTextXpath(psl.PaymentsPageLocators.losSuccessRateMeassage[this.env])
        console.log("Bank is on low success rate : "+lowSR)
    }
    
}
async verifyGiftCartContextOnPaymentPage(page)
{
    const url=await this.page.url()
    if(url.includes("cartContext=egiftcard"))
    {
        console.log("Payment page loaded successfully for giftcard purchase")
        return true
    }
    else
    {
        return false
    }
}
async verifySavedUPIOption(page)
{
    await this.page.waitForTimeout(2000)
    const isVisible=await this.page.isXPathVisible(psl.PaymentsPageLocators.savedPaymentHeaderText[this.env])
    console.log("For pwa Payment personalisation displayed:"+isVisible)
    if(this.env==="pwa"&& isVisible===false)
    {
        await this.page.isElementVisible(psl.PaymentsPageLocators.recommendedOptions[this.env])
        const upiTextUnderRecommendedOption=await this.page.getText(psl.PaymentsPageLocators.savedUPIText[this.env])
        if(upiTextUnderRecommendedOption.includes("@")) 
       {
           console.log("Saved VPA is Displayed :"+upiTextUnderRecommendedOption)
           await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.savedUPIText[this.env])
         
               await this.page.click(psl.PaymentsPageLocators.placeOrderButtonPWA[this.env])
               await this.page.waitForTimeout(3000)
        }
        else
        {
            console.log("Saved VPA is not displayed")
        }
        
    }
    const header=await this.page.getTextXpath(psl.PaymentsPageLocators.savedPaymentHeaderText[this.env])
    if(header.includes("SAVED"))
    {
        await this.page.isElementVisible(psl.PaymentsPageLocators.savedOptions[this.env])
        const upiText=await this.page.getTextXpath(psl.PaymentsPageLocators.savedUPIText[this.env])
       if(upiText.includes("@")) 
       {
           console.log("Saved VPA is Displayed :"+upiText)
           await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.savedUPIText[this.env])
           if(this.env==="pwa")
            {
                await this.page.waitForTimeout(3000)
                await this.page.click(psl.PaymentsPageLocators.placeOrderButtonPWA[this.env])
                await this.page.waitForTimeout(3000)
            }
            else
            {
                await this.page.waitForTimeout(3000)
                await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.savedVPAPaynow[this.env])
             }
          
        }
        else
        {
            console.log("Saved VPA is not displayed")
        }
    }
    else if(header.includes("RECOMMENDED"))
    {
        await this.page.isElementVisible(psl.PaymentsPageLocators.recommendedOptions[this.env])
        const upiTextUnderRecommendedOption=await this.page.getText(psl.PaymentsPageLocators.savedUPIText[this.env])
        if(upiTextUnderRecommendedOption.includes("@")) 
       {
           console.log("Saved VPA is Displayed :"+upiTextUnderRecommendedOption)
           await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.savedUPIText[this.env])
           if(this.env==="pwa")
           {
               await this.page.waitForTimeout(3000)
               await this.page.click(psl.PaymentsPageLocators.placeOrderButtonPWA[this.env])
               await this.page.waitForTimeout(3000)
           }
           else
           {
               await this.page.waitForTimeout(3000)
               await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.recommendedUPIPaynow[this.env])
            }
           
        }
        else
        {
            console.log("Saved VPA is not displayed")
        }

    }
    else
    {
        console.log("Saved Payments is not displayed")
    }
}

async verifySavedCardOption(page)
{
    await this.page.waitForTimeout(2000)
    const isVisible=await this.page.isXPathVisible(psl.PaymentsPageLocators.savedPaymentHeaderText[this.env])
    console.log("For pwa Payment personalisation displayed:"+isVisible)
    if(this.env==="pwa"&& isVisible===false)
    {
        await this.page.isElementVisible(psl.PaymentsPageLocators.recommendedOptions[this.env])
        const savedCardTextUnderRecommendedOption=await this.page.getText(psl.PaymentsPageLocators.savedCardText[this.env])
        if(savedCardTextUnderRecommendedOption.includes("Card")) 
       {
           console.log("Saved Card is Displayed :"+savedCardTextUnderRecommendedOption)
           await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.savedCardText[this.env])
         
               await this.page.click(psl.PaymentsPageLocators.placeOrderButtonPWA[this.env])
               await this.page.waitForTimeout(3000)
        }
        else
        {
            console.log("Saved VPA is not displayed")
        }
        
    }
    const header=await this.page.getTextXpath(psl.PaymentsPageLocators.savedPaymentHeaderText[this.env])
    if(header.includes("SAVED"))
    {
        await this.page.isElementVisible(psl.PaymentsPageLocators.savedOptions[this.env])
        const creditCardText=await this.page.getTextXpath(psl.PaymentsPageLocators.savedCardText[this.env])
       if(creditCardText.includes("Card")) 
       {
           console.log("Saved Card is Displayed :"+creditCardText)
           await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.savedCardText[this.env])
          const masked= await this.page.isXPathVisible(psl.PaymentsPageLocators.creditCardMaskText[this.env])
           console.log("the saved credit card is masked :"+masked)
           if(this.env==="pwa")
            {
                await this.page.waitForTimeout(3000)
                await this.page.waitForXpathAndType(psl.PaymentsPageLocators.savedCreditCardCvv[this.env],psl.paymentDetails.creditCardcvv)
                await this.page.click(psl.PaymentsPageLocators.placeOrderButtonPWA[this.env])
                await this.page.waitForTimeout(3000)
            }
            else
            {
                await this.page.waitForTimeout(3000)
                await this.page.waitForXpathAndType(psl.PaymentsPageLocators.savedCreditCardCvv[this.env],psl.paymentDetails.creditCardcvv)
                await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.savedCreditCardPaynow[this.env])
             }
          
        }
        else
        {
            console.log("Saved Card is not displayed")
        }
    }
    else if(header.includes("RECOMMENDED"))
    {
        await this.page.isElementVisible(psl.PaymentsPageLocators.recommendedOptions[this.env])
        const savedCardTextUnderRecommendedOption=await this.page.getText(psl.PaymentsPageLocators.savedCardText[this.env])
        if(savedCardTextUnderRecommendedOption.includes("Card")) 
       {
           console.log("Saved Card is Displayed :"+savedCardTextUnderRecommendedOption)
           await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.savedCardText[this.env])
           const masked= await this.page.isXPathVisible(psl.PaymentsPageLocators.creditCardMaskText[this.env])
           console.log("the saved credit card is masked :"+masked)
           if(this.env==="pwa")
           {
               await this.page.waitForTimeout(3000)
               await this.page.waitForXpathAndType(psl.PaymentsPageLocators.savedCreditCardCvv[this.env],psl.paymentDetails.creditCardcvv)
               await this.page.click(psl.PaymentsPageLocators.placeOrderButtonPWA[this.env])
               await this.page.waitForTimeout(3000)
           }
           else
           {
               await this.page.waitForTimeout(3000)
               await this.page.waitForXpathAndType(psl.PaymentsPageLocators.savedCreditCardCvv[this.env],psl.paymentDetails.creditCardcvv)
               await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.recommendedSavedCardPaynow[this.env])
            }
           
        }
        else
        {
            console.log("Saved Card is not displayed")
        }

    }
    else
    {
        console.log("Saved Payments is not displayed")
    }
}
async verifyMyntraCreditInPriceBreakUp(page)
{
    await this.page.waitForTimeout(3000)
    var isMCDisplayedInPriceBreakup=await this.page.isElementVisible(psl.PaymentsPageLocators.myntraCreditInPriceBreakup[this.env])
    return isMCDisplayedInPriceBreakup
}
async verifyMyntraCreditPaymentOption(page)
{
    var isMyntraCreditSelected=await this.page.isXPathVisible(psl.PaymentsPageLocators.myntraCreditSelected[this.env])

    if(isMyntraCreditSelected===true)
    {
        console.log("Myntra credit is already selected")
    }
    else
    {
        console.log("Myntra credit is not auto selected")
        await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.selectMyntraCredit[this.env])
        console.log("Myntra Credit is selected")
    }
}
async verifyMyntraCreditPriceInPriceBreakupIsSameAsMcUsedBalance(page)
{
    var priceInMcUsed=await this.page.getTextXpath(psl.PaymentsPageLocators.getMcUsedPrice[this.env])
    console.log("Price in MC used:"+priceInMcUsed)
    var mcPriceInPriceBreakup=await this.page.getTextXpath(psl.PaymentsPageLocators.myntraCreditInPriceBreakup[this.env])
    console.log("Price in Break up:"+mcPriceInPriceBreakup)
    if(priceInMcUsed.includes(mcPriceInPriceBreakup))
    {
        console.log("price are same in price break up and MC used")
    }
    else
    {
        console.log("prices are different")
    }
} 
async verify2FAWindow(page)
{
    var is2FADisplayed=await this.page.isXPathVisible(psl.PaymentsPageLocators.otpWindow[this.env])
    if(is2FADisplayed===true)
    {
        console.log("OTP window is displayed")

    }
    else
    {
        console.log("OTP window is not displayed")
    }
}
async selectMobileNumberToGetOTP(page)
{
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.clickMobileNumber[this.env])
    var contactNumber=await this.page.getTextXpath(psl.PaymentsPageLocators.contactNumberInOTPScreen[this.env])
    return contactNumber
  

}
async verifyOTPScreen(page)
{
    var isOTPscreenDisplayed=await this.page.isElementVisible(psl.PaymentsPageLocators.otpScreen[this.env])
    return isOTPscreenDisplayed
}
async getPhoneNumberInOTPScreen(page)
{
   var phoneNumber= await this.page.getText(psl.PaymentsPageLocators.phoneNumberInOTPScreen[this.env])
   var trimName=phoneNumber.split("-")
   var number=trimName[1]
   console.log("**** phone number  is *****"+number)
    return number
}
async clickPhoneNumberin2FAscreen(page)
{
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.clickSendOTP[this.env])
}
async clickSubmitInOTPScreen(page)
{
    await this.page.waitForTimeout(3000)
    await this.page.waitForXPathAndClick(psl.PaymentsPageLocators.clickSubmitInOTP[this.env])
    await this.page.waitForTimeout(3000)
}
async verifyerrorMessageForInvalidOTP(page)
{
     var errorMessage=await this.page.getTextXpath(psl.PaymentsPageLocators.getErrorMessageOTP[this.env])
     console.log("the error message displayed is :"+errorMessage)
     var isDisplayed=errorMessage.includes("Please")
     return isDisplayed
}

}