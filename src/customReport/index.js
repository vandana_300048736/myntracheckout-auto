"use strict";

var dummyJson = require("../../mochawesome-report/mochawesome.json");
var htmlTemplate = require("./template");
var fs = require('fs');
var data = htmlTemplate(dummyJson);
fs.writeFile("customReport.html", data, function () {
    console.info("new html report is generated");
});